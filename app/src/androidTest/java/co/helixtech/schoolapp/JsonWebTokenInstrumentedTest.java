package co.helixtech.schoolapp;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import co.helixtech.schoolapp.utility.JsonWebToken;
import co.helixtech.schoolapp.utility.UserType;

@RunWith(AndroidJUnit4.class)
public class JsonWebTokenInstrumentedTest {
    private final static String TEST_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9." +
            "eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTQ2MDA4MDQ1LCJqdGkiOiIyOWFjNjQ" +
            "5OGFkNmI0OGYxYmFkM2YyNGFlOWRiNjkyNyIsInVzZXJfaWQiOjIsInVzZXJfdHlwZSI6eyJ" +
            "0ZWFjaGVyIjp0cnVlLCJwYXJlbnQiOmZhbHNlLCJ0ZWFjaGVyX2lkIjoxLCJwYXJlbnRfaWQi" +
            "Oi0xfX0.vCqVKMefrWL-veHTnLq4MBV6W5v6yme2OzmY6Lai1CM";

    @Test
    public void userTypeTest()
    {
        JsonWebToken token = new JsonWebToken(TEST_TOKEN);
        Assert.assertEquals(token.getUserType(),UserType.TEACHER);
    }

    @Test
    public void userIDTest()
    {
        JsonWebToken token = new JsonWebToken(TEST_TOKEN);
        Assert.assertEquals(token.getUserID(),2);
    }

    @Test
    public void parentIDTest()
    {
        JsonWebToken token = new JsonWebToken(TEST_TOKEN);
        Assert.assertEquals(token.getParentID(),-1);
    }

    @Test
    public void teacherIDTest()
    {
        JsonWebToken token = new JsonWebToken(TEST_TOKEN);
        Assert.assertEquals(token.getTeacherID(),1);
    }

    @Test
    public void headerValueTest()
    {
        JsonWebToken token = new JsonWebToken(TEST_TOKEN);
        Assert.assertEquals(token.getAuthorizationHeaderValue(),"Bearer "+TEST_TOKEN);
    }
}
