package co.helixtech.schoolapp.db.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attendance {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("total_days_present")
    @Expose
    private Integer totalDaysPresent;
    @SerializedName("total_days_absent")
    @Expose
    private Integer totalDaysAbsent;
    @SerializedName("teacher_class")
    @Expose
    private Integer teacherClass;
    @SerializedName("student")
    @Expose
    private Integer student;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getTotalDaysPresent() {
        return totalDaysPresent;
    }

    public void setTotalDaysPresent(Integer totalDaysPresent) {
        this.totalDaysPresent = totalDaysPresent;
    }

    public Integer getTotalDaysAbsent() {
        return totalDaysAbsent;
    }

    public void setTotalDaysAbsent(Integer totalDaysAbsent) {
        this.totalDaysAbsent = totalDaysAbsent;
    }

    public Integer getTeacherClass() {
        return teacherClass;
    }

    public void setTeacherClass(Integer teacherClass) {
        this.teacherClass = teacherClass;
    }

    public Integer getStudent() {
        return student;
    }

    public void setStudent(Integer student) {
        this.student = student;
    }
}
