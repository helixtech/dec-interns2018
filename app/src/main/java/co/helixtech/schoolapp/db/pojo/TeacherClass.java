package co.helixtech.schoolapp.db.pojo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class TeacherClass {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private Integer id;

    @Ignore
    @SerializedName("teacher")
    @Expose
    private Teacher teacher;

    private int teacherID;

    @Ignore
    @SerializedName("class_id")
    @Expose
    private Klass classId;

    @ColumnInfo(name = "class_id")
    private int classID;

    @Ignore
    @SerializedName("subject")
    @Expose
    private Subject subject;

    @ColumnInfo(name = "subject")
    private int subject_id;

    public int getTeacherID() {
        return teacherID;
    }

    public void setTeacherID(int teacherID) {
        this.teacherID = teacherID;
    }

    public int getClassID() {
        return classID;
    }

    public void setClassID(int classID) {
        this.classID = classID;
    }

    public int getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(int subject_id) {
        this.subject_id = subject_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;

    }

    public Klass getClassId() {
        return classId;
    }

    public void setClassId(Klass classId) {
        this.classId = classId;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;

    }

    public void setUpRedundantFields() {
        this.teacherID = teacher.getId();
        this.subject_id = subject.getId();
        this.classID = classId.getId();
    };


}
