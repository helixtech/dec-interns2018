package co.helixtech.schoolapp.db.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ParentChild implements Serializable {

    @SerializedName("relation")
    @Expose
    private String relation;
    @SerializedName("parent")
    @Expose
    private Parent parent;

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public Parent getParent() {
        return parent;
    }

    public void setParent(Parent parent) {
        this.parent = parent;
    }

}
