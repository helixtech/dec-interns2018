package co.helixtech.schoolapp.db.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PostReportCard implements Serializable {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("teacher_class")
    @Expose
    private Integer teacherClass;
    @SerializedName("student")
    @Expose
    private Integer student;
    @SerializedName("remarks")
    @Expose
    private String remarks;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTeacherClass() {
        return teacherClass;
    }

    public void setTeacherClass(Integer teacherClass) {
        this.teacherClass = teacherClass;
    }

    public Integer getStudent() {
        return student;
    }

    public void setStudent(Integer student) {
        this.student = student;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
