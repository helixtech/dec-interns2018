package co.helixtech.schoolapp.db.pojo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

import co.helixtech.schoolapp.utility.DataConverter;

@Entity
@TypeConverters(DataConverter.class)
public class Assignment {
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("image")
    @Expose
    private String image;

    @Ignore
    @SerializedName("date")
    @Expose
    private String date;

    @Ignore
    @SerializedName("deadline")
    @Expose
    private String deadline;
    @SerializedName("teacher_class")
    @Expose
    private Integer teacherClass;

    @ColumnInfo(name = "deadline")
    private Date deadlineDate;
    @ColumnInfo(name = "date")
    private Date postDate;

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public Date getDeadlineDate() {
        return deadlineDate;
    }

    public void setDeadlineDate(Date dealineDate) {
        this.deadlineDate = dealineDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public Integer getTeacherClass() {
        return teacherClass;
    }

    public void setTeacherClass(Integer teacherClass) {
        this.teacherClass = teacherClass;
    }
}
