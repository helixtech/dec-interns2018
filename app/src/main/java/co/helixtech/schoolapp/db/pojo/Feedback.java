package co.helixtech.schoolapp.db.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Feedback {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("feedback")
    @Expose
    private String feedback;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("teacher")
    @Expose
    private Integer teacher;
    @SerializedName("student")
    @Expose
    private Integer student;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getTeacher() {
        return teacher;
    }

    public void setTeacher(Integer teacher) {
        this.teacher = teacher;
    }

    public Integer getStudent() {
        return student;
    }

    public void setStudent(Integer student) {
        this.student = student;
    }
}
