package co.helixtech.schoolapp.db.pojo;

import com.google.gson.annotations.Expose;

public class Authentication {
    @Expose
    private String access;
    @Expose
    private String refresh;

    public Authentication(String access, String refresh) {
        this.access = access;
        this.refresh = refresh;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public String getRefresh() {
        return refresh;
    }

    public void setRefresh(String refresh) {
        this.refresh = refresh;
    }


    @Override
    public String toString() {
        return "Refresh Token: "+getRefresh()+"\nAccess Token"+getAccess();
    }
}
