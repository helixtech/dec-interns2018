package co.helixtech.schoolapp.db.pojo;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import co.helixtech.schoolapp.dao.AssignmentDao;
import co.helixtech.schoolapp.dao.ClassActivityDao;
import co.helixtech.schoolapp.dao.KlassDao;
import co.helixtech.schoolapp.dao.NoticeDao;
import co.helixtech.schoolapp.dao.StudentDao;
import co.helixtech.schoolapp.dao.SubjectDao;
import co.helixtech.schoolapp.dao.TeacherClassDao;
import co.helixtech.schoolapp.dao.TeacherDao;

@Database(entities = {Klass.class,Subject.class,Teacher.class,TeacherClass.class
,Student.class, Assignment.class, Notice.class, ClassActivity.class}, version = 1,
        exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static final String DATABASE_NAME= "school-app-database";
    private static final Object LOCK = new Object();
    private static volatile AppDatabase sInstance;

    public abstract KlassDao klassDao();
    public abstract SubjectDao subjectDao();
    public abstract TeacherClassDao teacherClassDao();
    public abstract TeacherDao teacherDao();
    public abstract StudentDao studentDao();
    public abstract AssignmentDao assignmentDao();
    public abstract NoticeDao noticeDao();
    public abstract ClassActivityDao classActivityDao();

    public static AppDatabase getInstance(Context context)
    {
        if(sInstance == null)
        {
            synchronized (LOCK) {
                if(sInstance == null)
                {
                    sInstance = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class,DATABASE_NAME).build();
                }
            }
        }
        return sInstance;
    }
}
