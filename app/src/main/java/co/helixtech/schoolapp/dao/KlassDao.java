package co.helixtech.schoolapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import co.helixtech.schoolapp.db.pojo.Klass;

@Dao
public interface KlassDao {

    @Query("SELECT * FROM klass WHERE id = :classID")
    Klass getClass(int classID);

    @Query("SELECT * FROM klass")
    List<Klass> getAllClasses();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertClasses(Klass ... classes);

    @Delete
    void deleteClass(Klass klass);

}
