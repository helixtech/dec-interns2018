package co.helixtech.schoolapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import co.helixtech.schoolapp.db.pojo.ClassActivity;
import co.helixtech.schoolapp.db.pojo.Notice;

@Dao
public interface ClassActivityDao {

    @Query("SELECT * FROM classactivity WHERE id = :id")
    ClassActivity getClassActivityByID(int id);
    @Query("SELECT * FROM classactivity")
    List<ClassActivity> getAllClassActivities();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(ClassActivity...classActivities);
    @Delete
    void deleteClassActivity(ClassActivity activity);
    @Query("SELECT * FROM classactivity WHERE teacherClass = :teacherClassID")
    List<ClassActivity> getActivitiesByTeacherClass(int teacherClassID);
}
