package co.helixtech.schoolapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import co.helixtech.schoolapp.db.pojo.Student;

@Dao
public interface StudentDao {

    @Query("SELECT * FROM student")
    List<Student> getAllStudents();
    @Query("SELECT * FROM student WHERE classId = :classID")
    List<Student> getStudentsOfClass(int classID);

    @Query("SELECT * FROM student WHERE id = :studentID")
    Student getStudent(int studentID);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertStudents(List<Student> students);

    @Delete
    void deleteStudent(Student student);
}
