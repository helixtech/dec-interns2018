package co.helixtech.schoolapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import co.helixtech.schoolapp.db.pojo.Notice;

@Dao
public interface NoticeDao {
    @Query("SELECT * FROM notice WHERE id = :id")
    Notice getNoticeByID(int id);

    @Query("SELECT * FROM notice")
    List<Notice> getAllNOtices();
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Notice...notices);
    @Delete
    void deleteNotice(Notice notice);
    @Query("SELECT * FROM notice WHERE teacherClass = :teacherClassID")
    List<Notice> getNoticesByTeacherClass(int teacherClassID);
}
