package co.helixtech.schoolapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import co.helixtech.schoolapp.db.pojo.Subject;

@Dao
public interface SubjectDao {

    @Query("SELECT * FROM Subject WHERE id = :subjectID")
    Subject getSubject(int subjectID);

    @Query("SELECT * FROM subject")
    List<Subject> getAllSubjects();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSubjects(Subject...subjects);
    @Delete
    void delete(Subject subject);
}
