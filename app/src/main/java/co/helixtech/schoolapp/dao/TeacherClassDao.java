package co.helixtech.schoolapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;
import co.helixtech.schoolapp.db.pojo.TeacherClass;

@Dao
public interface TeacherClassDao {
    @Query("SELECT * FROM teacherclass")
    List<TeacherClass> getAllTeacherClasses();

    @Query("SELECT * FROM teacherclass WHERE teacherID = :teacherID")
    List<TeacherClass> getRecordsByTeacherID(int teacherID);

    @Query("SELECT * FROM teacherclass WHERE class_id = :classID")
    List<TeacherClass> getRecordsByClassID(int classID);

    @Query("SELECT * FROM teacherclass WHERE subject= :subjectID")
    List<TeacherClass> getRecordsBySubjectID(int subjectID);

    @Query("SELECT * FROM teacherclass WHERE teacherID = :teacherID AND class_id = :classID")
    List<TeacherClass> getRecordsByClassAndTeacher(int teacherID, int classID);

    @Query("SELECT * FROM teacherclass WHERE id = :id")
    TeacherClass getRecordByID(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTeacherClasses(List<TeacherClass> teacherClasses);
    @Delete
    void delete(TeacherClass teacher);
}
