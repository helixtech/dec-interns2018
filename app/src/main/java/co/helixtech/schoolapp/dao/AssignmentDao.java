package co.helixtech.schoolapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import co.helixtech.schoolapp.db.pojo.Assignment;

@Dao
public interface AssignmentDao {


    @Query("SELECT * FROM assignment")
    List<Assignment> getAllAssignments();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAssignments(Assignment... assignments);

    @Delete
    void deleteAssignment(Assignment assignment);

    @Query("SELECT * FROM assignment WHERE id in (:ids)")
    List<Assignment> getAssignmentsByIDs(int...ids);

    @Query("SELECT * FROM assignment WHERE teacherClass = :teacherClassID")
    List<Assignment> getAssignmentsByTeacherClass(int teacherClassID);
}
