package co.helixtech.schoolapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import co.helixtech.schoolapp.db.pojo.Teacher;

@Dao
public interface TeacherDao {
    @Query("SELECT * FROM Teacher WHERE id = :teacherID")
    Teacher getTeacher(int teacherID);

    @Query("SELECT * FROM teacher WHERE id = :teacherID")
    Teacher getTeacherByID(int teacherID);

    @Query("SELECT * from teacher")
    List<Teacher> getAllTeachers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTeachers(Teacher...teachers);
    @Delete
    void deleteTeacher(Teacher teacher);
}
