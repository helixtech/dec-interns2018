package co.helixtech.schoolapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jaeger.library.StatusBarUtil;

import co.helixtech.schoolapp.common.Login;
import co.helixtech.schoolapp.parent.children;
import co.helixtech.schoolapp.teacher.select_class_activity;
import co.helixtech.schoolapp.utility.JsonWebToken;
import co.helixtech.schoolapp.utility.UserType;

public class OnboardingScreens extends AppCompatActivity implements View.OnClickListener{

    ViewPager mpager;
    private LinearLayout dots_layout;
    private ImageView[] dots;
    int[] layouts = {R.layout.slide1, R.layout.slide2, R.layout.slide3};
    private Mpager mpageradapter;
    Button btnnext,btnskip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!isFirstTime()){
            checkLoginState();
        }
        setContentView(R.layout.activity_onboarding_screens);
        mpager = (ViewPager) findViewById(R.id.viewpager);
        mpageradapter = new Mpager(layouts, this);
        mpager.setAdapter((mpageradapter));
        dots_layout = (LinearLayout) findViewById(R.id.dots);
        btnnext=(Button) findViewById(R.id.btnnext);
        btnskip=(Button) findViewById(R.id.btnskip);
        createdots(0);

        getSupportActionBar().hide();

        StatusBarUtil.setColor(this,getResources().getColor(R.color.colorPrimaryLight));



        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
*/
        btnnext.setOnClickListener(this);
        btnskip.setOnClickListener(this);

        mpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {

                createdots(i);
                if(i==layouts.length-1)
                {
                    btnnext.setText("Start");
                    btnskip.setVisibility(View.INVISIBLE);
                }
                else
                {
                    btnnext.setText("Next");
                    btnskip.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    private boolean isFirstTime(){
        SharedPreferences preferences = getSharedPreferences(
                getString(R.string.auth_token_preferences),MODE_PRIVATE);
        return preferences.getBoolean("FIRST_TIME",true);
    }
    private void checkLoginState() {
        SharedPreferences preferences = getSharedPreferences
                (getString(R.string.auth_token_preferences), MODE_PRIVATE);
        String accessToken = preferences.getString(getString(R.string.access_token), null);
        if (accessToken == null)
            openLoginActivity();
        JsonWebToken token = new JsonWebToken(accessToken);
        UserType type = token.getUserType();
        if (type == UserType.PARENT)
            openParentActivity();
        else if (type == UserType.TEACHER)
            openTeacherActivity();
    }
    private void openLoginActivity(){
        Intent intent =new Intent();
        intent.setClass(this,Login.class);
        startActivity(intent);
        finish();
    }
    private void openTeacherActivity() {
        Intent intent = new Intent();
        intent.setClass(this, select_class_activity.class);
        startActivity(intent);
        finish();
    }

    private void openParentActivity() {
        Intent intent = new Intent();
        intent.setClass(this, children.class);
        startActivity(intent);
        finish();
    }
    private void createdots(int current_positon) {
        if (dots_layout != null)
            dots_layout.removeAllViews();

        dots = new ImageView[layouts.length];

        for (int i = 0; i < layouts.length; i++) {
            dots[i] = new ImageView(this);
            if (i == current_positon) {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.activedots));
            } else {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.defaultdots));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(4, 0, 4, 0);
            dots_layout.addView(dots[i], params);

        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.btnnext:
            {
                loadnextslid();
                break;
            }
            case R.id.btnskip:
            {
                startActivity(new Intent(this,Login.class));
                finish();
                break;
            }
        }
    }

    public void loadnextslid()
    {
        int nextslide = mpager.getCurrentItem()+1;

        if(nextslide<layouts.length)
        {
            mpager.setCurrentItem(nextslide);
        }
        else
        {
            startActivity(new Intent(this,Login.class));
            finish();
        }
    }

}
