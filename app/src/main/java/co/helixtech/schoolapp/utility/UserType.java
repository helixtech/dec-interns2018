package co.helixtech.schoolapp.utility;

public enum UserType {
    TEACHER,
    PARENT
}
