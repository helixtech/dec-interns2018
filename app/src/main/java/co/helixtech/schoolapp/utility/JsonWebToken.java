package co.helixtech.schoolapp.utility;

import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;

public class JsonWebToken {

    private String token;

    public JsonWebToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    private String[] splitToken() {
        return token.split("\\.");
    }

    private String getPayloadString() {
        String[] parts = splitToken();
        String payload =
                new String(Base64.decode(parts[1], Base64.DEFAULT), Charset.defaultCharset());
        return payload;
    }

    public UserType getUserType() {
        try {
            JSONObject jsonObject = new JSONObject(getPayloadString());
            Log.d(JsonWebToken.class.getName(), jsonObject.toString());
            boolean isTeacher = jsonObject.getJSONObject("user_type").getBoolean("teacher");
            boolean isParent = jsonObject.getJSONObject("user_type").getBoolean("parent");
            if (isTeacher)
                return UserType.TEACHER;
            else if (isParent)
                return UserType.PARENT;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return UserType.PARENT;
    }


    public int getUserID() {
        try {
            JSONObject jsonObject = new JSONObject(getPayloadString());
            return jsonObject.getInt("user_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public int getParentID() {
        try {
            JSONObject jsonObject = new JSONObject(getPayloadString());
            return jsonObject.getJSONObject("user_type").getInt("parent_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public int getTeacherID() {
        try {
            JSONObject jsonObject = new JSONObject(getPayloadString());
            return jsonObject.getJSONObject("user_type").getInt("teacher_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public String getAuthorizationHeaderValue()
    {
        return "Bearer "+this.token;
    }
}
