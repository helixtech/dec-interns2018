package co.helixtech.schoolapp.utility;

import android.content.Context;
import android.content.SharedPreferences;

import co.helixtech.schoolapp.R;

public class TokenSharedPreference {
    private Context context;
    private SharedPreferences preferences;

    public TokenSharedPreference(Context context) {
        this.context = context;
        this.preferences = context.getSharedPreferences(
                context.getString(R.string.auth_token_preferences),Context.MODE_PRIVATE
        );
    }

    public JsonWebToken getCurrentAccessToken()
    {
        String accessToken = preferences.getString(context.getString(R.string.access_token),
                null);
        return new JsonWebToken(accessToken);
    }

    public JsonWebToken getCurrentRefreshToken()
    {
        String refreshToken = preferences.getString(context.getString(R.string.refresh_token),
                null);
        return new JsonWebToken(refreshToken);
    }

    public void clearCredentials() {
        preferences.edit().clear().apply();
        preferences.edit().putBoolean("FIRST_TIME",false).apply();
    }

}
