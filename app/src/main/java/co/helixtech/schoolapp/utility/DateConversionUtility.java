package co.helixtech.schoolapp.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateConversionUtility {
    public static final SimpleDateFormat FORMAT_DATE = new SimpleDateFormat("yyyy-MM-dd",Locale.US);

    public static final SimpleDateFormat FORMAT = new SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
    public static final SimpleDateFormat FORMAT_NORMAL = new SimpleDateFormat(
            "dd-MM-yyyy hh:mm a", Locale.US
    );
    public static final SimpleDateFormat FORMAT_REDABLE = new SimpleDateFormat(
             "EEE, MMM d, yyyy", Locale.US
    );

    public static String dateToString(Date date) throws ParseException
    {
        return FORMAT.format(date);
    }

    public static String dateToStringNormalFormat(Date date) throws ParseException
    {
        return FORMAT_NORMAL.format(date);
    }
    public static Date stringToDate(String date) throws ParseException
    {
        return FORMAT.parse(date);
    }
    public static String dateToStringReadableFormat(Date date) throws ParseException
    {
        return FORMAT_REDABLE.format(date);
    }
    public static String dateToString2(Date date) throws ParseException{
        return FORMAT_DATE.format(date);
    }

}
