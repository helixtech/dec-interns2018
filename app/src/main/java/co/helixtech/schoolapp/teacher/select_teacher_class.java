package co.helixtech.schoolapp.teacher;

public class select_teacher_class {
    private String classname;
    private String subjectname;

    public select_teacher_class(String classname, String subjectname) {
        this.classname = classname;
        this.subjectname = subjectname;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getSubjectname() {
        return subjectname;
    }

    public void setSubjectname(String subjectname) {
        this.subjectname = subjectname;
    }
}
