package co.helixtech.schoolapp.teacher.view_models;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import co.helixtech.schoolapp.db.pojo.TeacherClass;

public class SelectClassActivityViewModel extends ViewModel {
    private MutableLiveData<List<TeacherClass>> teacherClasses;

    public SelectClassActivityViewModel() {
        teacherClasses = new MutableLiveData<>();
    }

    public MutableLiveData<List<TeacherClass>> getTeacherClasses() {
        return teacherClasses;
    }

    public void setTeacherClasses(List<TeacherClass> teacherClasses) {
        this.teacherClasses.postValue(teacherClasses);
    }
}
