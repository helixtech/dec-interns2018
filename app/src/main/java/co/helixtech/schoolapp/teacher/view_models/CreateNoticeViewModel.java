package co.helixtech.schoolapp.teacher.view_models;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import co.helixtech.schoolapp.db.pojo.Notice;

public class CreateNoticeViewModel extends ViewModel {
    private MutableLiveData<Notice> noticeMutableLiveData;

    public CreateNoticeViewModel() {
        noticeMutableLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<Notice> getNoticeMutableLiveData() {
        return noticeMutableLiveData;
    }

    public void setNoticeMutableLiveData(Notice notice) {
        this.noticeMutableLiveData.postValue(notice);
    }
}
