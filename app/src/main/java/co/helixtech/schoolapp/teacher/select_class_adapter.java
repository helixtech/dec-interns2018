package co.helixtech.schoolapp.teacher;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.db.pojo.TeacherClass;

public class select_class_adapter extends RecyclerView.Adapter<select_class_adapter.MyViewHolder> {


    private Context mContext;
    private List<TeacherClass> nData;

    public select_class_adapter(Context mContext, List<TeacherClass> nData) {
        this.mContext = mContext;
        this.nData = nData;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view;
        LayoutInflater minflator = LayoutInflater.from(mContext);
        view = minflator.inflate(R.layout.select_class_items,viewGroup,false);

        return new MyViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, final int i) {

        myViewHolder.class_name.setText(nData.get(i).getClassId().getClassName());
        myViewHolder.subject_name.setText(nData.get(i).getSubject().getSubjectName());
        final TeacherClass teacherClass = nData.get(i);

        myViewHolder.class_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = mContext.getSharedPreferences(mContext.
                        getString(R.string.teacher_preferences),Context.MODE_PRIVATE);
                preferences.edit().putInt(mContext.getString(R.string.teacher_class_id),
                        teacherClass.getId())
                        .putInt(mContext.getString(R.string.class_id),teacherClass.getClassID())
                        .apply();
                Intent intent = new Intent(mContext,teacher_homescreen.class);
                intent.putExtra(teacher_homescreen.ARG_CLASS_NAME,nData.get(i).
                        getClassId().getClassName());
                intent.putExtra(teacher_homescreen.ARG_CLASS_ID,nData.get(i)
                .getClassId().getId());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return nData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView class_name,subject_name;
        CardView class_select;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            /*this.class_name = class_name;
            this.subjectname = subjectname;*/
            class_name = (TextView) itemView.findViewById(R.id.select_teacher_class_id);
            subject_name = (TextView) itemView.findViewById(R.id.select_teacher_subject_id);
            class_select = (CardView) itemView.findViewById(R.id.class_cardview_id);
        }
    }
}
