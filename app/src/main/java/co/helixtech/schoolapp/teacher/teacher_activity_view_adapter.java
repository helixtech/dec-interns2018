package co.helixtech.schoolapp.teacher;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.util.List;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.db.pojo.ClassActivity;
import co.helixtech.schoolapp.services.DataSyncService;
import co.helixtech.schoolapp.utility.DataConverter;
import co.helixtech.schoolapp.utility.DateConversionUtility;

public class teacher_activity_view_adapter extends RecyclerView.Adapter<teacher_activity_view_adapter.MyViewHolder> {

    private Context mContext;
    private List<ClassActivity> mData;

    private String pos;

    public teacher_activity_view_adapter(Context mContext, List<ClassActivity> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        LayoutInflater mInflator = LayoutInflater.from(mContext);
        view = mInflator.inflate(R.layout.activities_card, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {

        myViewHolder.Title.setText(mData.get(i).getTitle());
        myViewHolder.Name.setText("<Not Needed>");
        Picasso.get().load(mData.get(i).getImage()).into(myViewHolder.activity_image);
        myViewHolder.Description.setText(mData.get(i).getDescription());
        try {
            myViewHolder.Date.setText(DateConversionUtility.dateToStringNormalFormat(
                    mData.get(i).getPostDate()
            ));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        myViewHolder.options_btn.setVisibility(View.VISIBLE);


        myViewHolder.options_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos = mData.get(i).getTitle();

                PopupMenu popup = new PopupMenu(mContext, view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.diary_options_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new MyMenuItemClickListener(mData.get(i).getId()));
                popup.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView Title, Name, Description, Date;
        ImageView activity_image;
        LinearLayout options_btn;

        public MyViewHolder(View itemView) {
            super(itemView);

            Title = (TextView) itemView.findViewById(R.id.title_activities);
            Name = (TextView) itemView.findViewById(R.id.name_activities);
            Description = (TextView) itemView.findViewById(R.id.description_activities);
            Date = (TextView) itemView.findViewById(R.id.date_activities);

            activity_image = (ImageView) itemView.findViewById(R.id.image_activities);

            options_btn = (LinearLayout) itemView.findViewById(R.id.activities_options_btn);
        }
    }

    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
        private int activityID;
        public MyMenuItemClickListener(int activityID) {
            this.activityID = activityID;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {

            switch (menuItem.getItemId()) {
                case R.id.edit_diary_optionbtn:
                    //edit activity
                    Intent intent = new Intent();
                    intent.setClass(mContext,create_activity.class);
                    intent.putExtra(create_activity.ACTIVITY_MODE,create_activity.EDIT_MODE);
                    intent.putExtra(create_activity.PARAM_ACTIVITY_ID,activityID);
                    mContext.startActivity(intent);
                    return true;
                case R.id.delete_diary_optionbtn:
                    //delete activity
                    intent = new Intent();
                    intent.setClass(mContext,DataSyncService.class);
                    intent.putExtra(DataSyncService.SERVICE_NAME,
                            DataSyncService.Constants.DELETE_ACTIVITY);
                    intent.putExtra(DataSyncService.Constants.PARAM_ACTIVTIY_ID,activityID);
                    mContext.startService(intent);
                    return true;
                default:
            }
            return false;
        }
    }
}
