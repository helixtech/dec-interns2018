package co.helixtech.schoolapp.teacher.view_models;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import co.helixtech.schoolapp.db.pojo.Assignment;
import co.helixtech.schoolapp.db.pojo.ClassActivity;
import co.helixtech.schoolapp.db.pojo.Notice;


public class DiaryViewModel extends ViewModel {
    private MutableLiveData<List<Assignment>> assignmentMutableLiveData;
    private MutableLiveData<List<Notice>> noticeMutableLiveData;
    private MutableLiveData<List<ClassActivity>>  actvitityMutableLiveData;


    public DiaryViewModel() {
        assignmentMutableLiveData = new MutableLiveData<>();
        noticeMutableLiveData = new MutableLiveData<>();
        actvitityMutableLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<List<ClassActivity>> getActvitityMutableLiveData() {
        return actvitityMutableLiveData;
    }

    public void setActvitityMutableLiveData(List<ClassActivity> activities) {
        this.actvitityMutableLiveData.postValue(activities);
    }

    public MutableLiveData<List<Notice>> getNoticeMutableLiveData() {
        return noticeMutableLiveData;
    }

    public void setNoticeMutableLiveData(List<Notice> notices) {
        this.noticeMutableLiveData.postValue(notices);
    }

    public MutableLiveData<List<Assignment>> getAssignmentMutableLiveData() {
        return assignmentMutableLiveData;
    }

    public void setAssignmentMutableLiveData(List<Assignment> assignments) {
        this.assignmentMutableLiveData.postValue(assignments);
    }
}
