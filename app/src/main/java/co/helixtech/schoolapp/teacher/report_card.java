package co.helixtech.schoolapp.teacher;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.unstoppable.submitbuttonview.SubmitButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.common.Login;
import co.helixtech.schoolapp.db.pojo.PostReportCard;
import co.helixtech.schoolapp.services.DataSyncService;
import co.helixtech.schoolapp.services.ImageSyncService;
import co.helixtech.schoolapp.utility.AppExecutors;

public class report_card extends AppCompatActivity {

    public static String PARAM_STUDENT_ID = report_card.class.getCanonicalName() + ".param_student_id";
    ImageView imagepreview;
    ImageButton uploadbtn;
    SubmitButton postbtn;
    TextView studentName;
    TextInputEditText reportTitle, descriptionText;
    TextInputLayout reportTitleLayout, reportDescriptionLayout;
    private Receiver receiver;
    private AppExecutors executors;

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                ((ImageView) findViewById(R.id.report_card_preview_imageview)).setImageURI(result.getUri());
                Toast.makeText(
                        this, "Cropping successful, Sample: " + result.getSampleSize(), Toast.LENGTH_LONG)
                        .show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getParentActivityIntent() == null) {
                    onBackPressed();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_card);

        getSupportActionBar().setTitle("Class Report");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        receiver = new Receiver();
        executors = new AppExecutors();
        imagepreview = (ImageView) findViewById(R.id.report_card_preview_imageview);
        // uploadbtn = (ImageButton) findViewById(R.id.upload_report_card_btn);
        studentName = (TextView) findViewById(R.id.report_card_student_name);
        postbtn = (SubmitButton) findViewById(R.id.post_report_card);
        reportTitle = (TextInputEditText) findViewById(R.id.report_title_text);
        descriptionText = (TextInputEditText) findViewById(R.id.report_description_text);
        reportTitleLayout = (TextInputLayout) findViewById(R.id.report_title_layout);
        reportDescriptionLayout = (TextInputLayout) findViewById(R.id.report_description_layout);

        imagepreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), 0);

                //startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), 3);
                CropImage.activity(null).setGuidelines(CropImageView.Guidelines.ON).setAspectRatio(2, 1).start(report_card.this);
            }
        });


        postbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validate()) {
                    int studentID = getIntent().getIntExtra(PARAM_STUDENT_ID, -1);
                    PostReportCard postReportCard = new PostReportCard();
                    postReportCard.setStudent(studentID);
                    postReportCard.setRemarks(descriptionText.getText().toString());
                    SharedPreferences preferences = getSharedPreferences(
                            getString(R.string.teacher_preferences), MODE_PRIVATE);
                    postReportCard.setTeacherClass(
                            preferences.getInt(getString(R.string.teacher_class_id), -1));
                    postReportCard.setTitle(reportTitle.getText().toString());
                    Intent intent = new Intent();
                    intent.setClass(view.getContext(), DataSyncService.class);
                    intent.putExtra(DataSyncService.SERVICE_NAME,
                            DataSyncService.Constants.POST_REPORT_CARD);
                    intent.putExtra(DataSyncService.Constants.PARAM_POST_REPORT_CARD,
                            postReportCard);
                    startService(intent);
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(DataSyncService.Constants.BROADCAST_ACTION);
        filter.addAction(ImageSyncService.Constants.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

    }

    private boolean validate() {
        if (!checkimage() | !checkdescp() | !checktitle()) {
            return false;

        } else return true;
    }

    private boolean checkimage() {
        if (imagepreview.getDrawable() == null) {
            Toast.makeText(this, "Upload image", Toast.LENGTH_SHORT).show();
            return false;
        } else return true;
    }

    private boolean checkdescp() {

        String descp = descriptionText.getText().toString().trim();
        if (descp.isEmpty()) {

            reportDescriptionLayout.setErrorEnabled(true);
            reportDescriptionLayout.setError("Please Write a Description");
            return false;
        } else {
            reportDescriptionLayout.setErrorEnabled(false);
            return true;
        }
    }

    private boolean checktitle() {

        String title = reportTitle.getText().toString().trim();
        if (title.isEmpty()) {

            reportTitleLayout.setErrorEnabled(true);
            reportTitleLayout.setError("Please Enter a Title");
            return false;
        } else {
            reportTitleLayout.setErrorEnabled(false);
            return true;
        }


    }

    public class Receiver extends BroadcastReceiver {

        private void onReportCardPosted(final Context context, Intent intent) {
            final int id = intent.getIntExtra(DataSyncService.Constants.PARAM_REPORT_CARD_ID, -1);
            if (id == -1)
                return;
            Log.d(Receiver.class.getCanonicalName(), "Report Posted");
            if (!(imagepreview.getDrawable() instanceof BitmapDrawable)) {
                postbtn.doResult(true);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 5s = 5000ms
                        onBackPressed();
                    }
                }, 1700);
                return;
            }
            executors.diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    try {

                        BitmapDrawable drawable = (BitmapDrawable)
                                imagepreview.getDrawable();
                        Bitmap bitmap = drawable.getBitmap();
                        File file = File.createTempFile(
                                ImageSyncService.Constants.REPORT_CARD_FILE_NAME,
                                id+ ".jpg", context.getCacheDir());
                        FileOutputStream stream = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG,
                                ImageSyncService.IMAGE_QUALITY,
                                stream);
                        bitmap.recycle();
                        Intent serviceIntent = new Intent();
                        serviceIntent.setClass(context, ImageSyncService.class);
                        serviceIntent.putExtra(ImageSyncService.Constants.PARAM_IMAGE_NAME,
                                file.getName());
                        serviceIntent.putExtra(ImageSyncService.SERVICE_NAME,
                                ImageSyncService.Constants.POST_REPORT_CARD);
                        serviceIntent.putExtra(ImageSyncService.Constants.PARAM_REPORT_CARD_ID,
                                id);
                        context.startService(serviceIntent);
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
        }

        private void onReportCardImagePosted(Context context,Intent intent){
            int postedID = intent.getIntExtra(
                    ImageSyncService.Constants.PARAM_REPORT_CARD_ID,
                    -1);
            Log.d(create_assignment.class.getName(), "Report card image " +
                    "posted" + postedID);
            postbtn.doResult(true);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Do something after 5s = 5000ms
                    onBackPressed();
                }
            }, 1700);
        }
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            assert action != null;
            if (action.equals(DataSyncService.Constants.BROADCAST_ACTION)) {
                int broadcastMessage = intent
                        .getIntExtra(DataSyncService.Constants.BROADCAST_MESSAGE,
                                -1);
                if (broadcastMessage == DataSyncService.Constants.REPORT_CARD_POSTED)
                    onReportCardPosted(context,intent);
            } else if (action.equals(ImageSyncService.Constants.BROADCAST_ACTION)) {
                int broadcastMessage = intent
                        .getIntExtra(ImageSyncService.Constants.BROADCAST_MESSAGE,
                                -1);
                if (broadcastMessage == ImageSyncService.Constants.REPORT_CARD_POSTED)
                    onReportCardImagePosted(context,intent);
            }

        }
    }


}
