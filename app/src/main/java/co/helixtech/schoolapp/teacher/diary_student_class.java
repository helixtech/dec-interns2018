package co.helixtech.schoolapp.teacher;

import android.widget.ImageView;

public class diary_student_class {

    private String studentName;
    private ImageView studentProfilePic;
    private boolean isSelected = false;

    public diary_student_class(String studentName, ImageView studentProfilePic) {
        this.studentName = studentName;
        this.studentProfilePic = studentProfilePic;

    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public ImageView getStudentProfilePic() {
        return studentProfilePic;
    }

    public void setStudentProfilePic(ImageView studentProfilePic) {
        this.studentProfilePic = studentProfilePic;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

}
