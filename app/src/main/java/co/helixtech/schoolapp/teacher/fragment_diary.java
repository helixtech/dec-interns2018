package co.helixtech.schoolapp.teacher;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import java.util.Calendar;

import co.helixtech.schoolapp.R;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link fragment_diary.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link fragment_diary#newInstance} factory method to
 * create an instance of this fragment.
 */
public class fragment_diary extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    //declarations

    CardView btnnew,btnassignment,btnnotice,btnactivity;
    /*FloatingActionButton btnnew,btnassignment,btnnotice,btnactivity;
    TextView txtnew,txtassignment,txtnotice,txtactivity;*/

    Animation fabopen,fabclose,clockwise,anticlockwise;
    boolean isOpen=false;

    public fragment_diary() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment fragment_diary.
     */
    // TODO: Rename and change types and number of parameters
    public static fragment_diary newInstance(String param1, String param2) {
        fragment_diary fragment = new fragment_diary();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Diary");

        //calendar
        /* starts before 1 month from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        /* ends after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        final HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(getActivity(), R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(5)
                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                //do something

                Toast.makeText(getActivity(), date.getTime()+"", Toast.LENGTH_SHORT).show();
            }
        });


       // btnnew = (Button) getView().findViewById(R.id.fab_new);
        btnassignment = (CardView) getView().findViewById(R.id.fab_assignment);
        btnnotice = (CardView) getView().findViewById(R.id.fab_notice);
        btnactivity = (CardView) getView().findViewById(R.id.fab_activity);

        /*txtnew = (TextView) getView().findViewById(R.id.textview_new);
        txtassignment = (TextView) getView().findViewById(R.id.textview_assignmet);
        txtnotice = (TextView) getView().findViewById(R.id.textview_notice);
        txtactivity = (TextView) getView().findViewById(R.id.textview_activity);*/

        fabopen= AnimationUtils.loadAnimation(getActivity().getApplicationContext(),R.anim.fab_open);
        fabclose= AnimationUtils.loadAnimation(getActivity().getApplicationContext(),R.anim.fab_close);
        clockwise= AnimationUtils.loadAnimation(getActivity().getApplicationContext(),R.anim.rotate);
        anticlockwise= AnimationUtils.loadAnimation(getActivity().getApplicationContext(),R.anim.offrotate);





        /*btnnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isOpen)
                {
                    btnassignment.startAnimation(fabclose);
                    btnnotice.startAnimation(fabclose);
                    btnactivity.startAnimation(fabclose);

                    txtassignment.startAnimation(fabclose);
                    txtnotice.startAnimation(fabclose);
                    txtactivity.startAnimation(fabclose);

                    btnnew.startAnimation(anticlockwise);
                    btnassignment.setClickable(false);
                    btnactivity.setClickable(false);
                    btnnotice.setClickable(false);


                }
                else
                {
                    btnassignment.startAnimation(fabopen);
                    btnnotice.startAnimation(fabopen);
                    btnactivity.startAnimation(fabopen);

                    txtassignment.startAnimation(fabopen);
                    txtnotice.startAnimation(fabopen);
                    txtactivity.startAnimation(fabopen);

                    btnnew.startAnimation(clockwise);
                    btnassignment.setClickable(true);
                    btnactivity.setClickable(true);
                    btnnotice.setClickable(true);



                }
                isOpen=!isOpen;


            }
        });*/

        btnnotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),teacher_diaryevent_view.class).
                        putExtra(teacher_diaryevent_view.EVENT_TYPE,teacher_diaryevent_view.NOTICE_TYPE);
                startActivity(intent);
            }
        });

        btnassignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(),teacher_diaryevent_view.class).
                        putExtra(teacher_diaryevent_view.EVENT_TYPE,teacher_diaryevent_view.ASSIGNMENT_TYPE);
                startActivity(intent);            }
        });

        btnactivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),teacher_diaryevent_view.class).
                        putExtra(teacher_diaryevent_view.EVENT_TYPE,teacher_diaryevent_view.ACTIVITY_TYPE);
                startActivity(intent);       }
        });






    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment




        return inflater.inflate(R.layout.fragment_fragment_diary, container, false);





    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
