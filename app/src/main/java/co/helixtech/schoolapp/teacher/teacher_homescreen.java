package co.helixtech.schoolapp.teacher;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import co.helixtech.schoolapp.R;

public class teacher_homescreen extends AppCompatActivity {


    public static final String ARG_CLASS_NAME = "class_name";
    public static final String ARG_CLASS_ID = "class_id";

    private TextView mTextMessage;
    private String className = null;

    private int classID = -1;
    Fragment selectedfragment=null;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case android.R.id.home:
                if (getParentActivityIntent() == null) {
                    onBackPressed();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
            case R.id.profile_option_btn:

                Intent intent = new Intent(this, TeacherProfile.class);
                startActivity(intent);

                return true;
            case R.id.settings_option_btn:

                Toast.makeText(this, "Settings option", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_profile, menu);
        return true;
    }

/*  private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menuitem_students:
                    selectedfragment = new fragment_student();
                    break;


                case R.id.menuitem_diary:
                    selectedfragment = new fragment_diary();
                    break;

                case R.id.menuitem_chat:
                    selectedfragment = new fragment_chat();
                    break;


            }


            getSupportFragmentManager().beginTransaction().replace(R.id.teacher_frame_id, selectedfragment).commit();

            return true;
        }
    };*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_homescreen);
        className = getIntent().getExtras().getString(ARG_CLASS_NAME);
        classID = getIntent().getExtras().getInt(ARG_CLASS_ID);
        SharedPreferences preferences = getSharedPreferences(getString(R.string.teacher_preferences),
                MODE_PRIVATE);
        preferences.edit().putInt(getString(R.string.class_id), classID).apply();

        getSupportActionBar().setTitle(className);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mTextMessage = (TextView) findViewById(R.id.message);
       // BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
      //  navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        getSupportFragmentManager().beginTransaction().add(R.id.teacher_frame_id,
                teacher_main_fragment.newInstance(classID,className)).commit();
    }

}
