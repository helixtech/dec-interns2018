package co.helixtech.schoolapp.teacher;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.common.Login;
import co.helixtech.schoolapp.db.pojo.AppDatabase;
import co.helixtech.schoolapp.db.pojo.Klass;
import co.helixtech.schoolapp.db.pojo.Subject;
import co.helixtech.schoolapp.db.pojo.Teacher;
import co.helixtech.schoolapp.db.pojo.TeacherClass;
import co.helixtech.schoolapp.services.DataSyncService;
import co.helixtech.schoolapp.teacher.view_models.SelectClassActivityViewModel;
import co.helixtech.schoolapp.utility.AppExecutors;
import co.helixtech.schoolapp.utility.TokenSharedPreference;
import me.itangqi.waveloadingview.WaveLoadingView;

public class select_class_activity extends AppCompatActivity {

    private List<TeacherClass> class_list = new ArrayList<>();
    private SelectClassActivityViewModel viewModel;
    private  RecyclerView myrv;
    private select_class_adapter myAdapter;
    private AppExecutors appExecutors;
    private AppDatabase appDatabase = null;
    private Receiver receiver;
    private ProgressBar progressBar;

    private long backPressedTime;
    private Toast backtoast;

    @Override
    public void onBackPressed() {

        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            backtoast.cancel();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            backtoast = Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT);
            backtoast.show();
        }
        backPressedTime = System.currentTimeMillis();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.logout_btn_id) {
            Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show();
            TokenSharedPreference preference = new TokenSharedPreference(this);
            preference.clearCredentials();
            finish();
            Intent i = new Intent(select_class_activity.this, Login.class);
              // set the new task and clear flags
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_class);

        progressBar = (ProgressBar) findViewById(R.id.progressBar_selectclass);

        progressBar.getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);


        appDatabase = AppDatabase.getInstance(this);

       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }*/






        receiver = new Receiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(DataSyncService.Constants.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,filter);

        appExecutors = new AppExecutors();
        myrv = (RecyclerView) findViewById(R.id.selectclass_recycler_id);
        myAdapter = new select_class_adapter(this,class_list);
        myrv.setLayoutManager(new LinearLayoutManager(this));
        myrv.setAdapter(myAdapter);

        viewModel = ViewModelProviders.of(this).get(SelectClassActivityViewModel.class);
        viewModel.getTeacherClasses().observe(this, new Observer<List<TeacherClass>>() {
            @Override
            public void onChanged(@Nullable List<TeacherClass> teacherClasses) {
                if (teacherClasses != null) {
                    class_list.clear();
                    class_list.addAll(teacherClasses);
                    myAdapter.notifyDataSetChanged();
                }
            }
        });

        //start bar
        progressBar.setVisibility(View.VISIBLE);

        receiver.loadData(this);

        TokenSharedPreference preference = new TokenSharedPreference(this);
        Intent intent = new Intent();
        intent.setClass(this,DataSyncService.class);
        intent.putExtra(DataSyncService.SERVICE_NAME,
                DataSyncService.Constants.GET_CLASSES_OF_TEACHER);
        intent.putExtra(DataSyncService.Constants.PARAM_TEACHER_ID,
                preference.getCurrentAccessToken().getTeacherID());

        //start

        startService(intent);

    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(DataSyncService.Constants.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,filter);
    }
    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    public class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getIntExtra(DataSyncService.Constants.BROADCAST_MESSAGE,-1))
            {
                case DataSyncService.Constants.CLASSES_OF_TEACHER_SYNCED:
                    loadData(context);
                    progressBar.setVisibility(View.GONE);
                    //stop
                    break;
                default:
                    //stop
                    progressBar.setVisibility(View.GONE);
                    break;

            }


        }

        public void loadData(final Context context)
        {
            appExecutors.diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    TokenSharedPreference preference = new TokenSharedPreference(context);
                    List<TeacherClass> teacherClasses =
                            appDatabase.teacherClassDao().getRecordsByTeacherID(
                                    preference.getCurrentAccessToken().getTeacherID()
                            );
                    for(TeacherClass teacherClass: teacherClasses)
                    {
                        Teacher teacher = appDatabase.teacherDao().
                                getTeacher(teacherClass.getTeacherID());
                        Subject subject = appDatabase.subjectDao()
                                .getSubject(teacherClass.getSubject_id());
                        Klass klass = appDatabase.klassDao()
                                .getClass(teacherClass.getClassID());

                        teacherClass.setTeacher(teacher);
                        teacherClass.setClassId(klass);
                        teacherClass.setSubject(subject);
                    }

                    viewModel.setTeacherClasses(teacherClasses);
                    //stop progress bar

                }
            });
            Log.d(Receiver.class.getCanonicalName(),"Data Fetched");
        }
    }
}
