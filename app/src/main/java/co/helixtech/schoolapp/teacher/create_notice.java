package co.helixtech.schoolapp.teacher;

import android.app.Activity;
import android.app.IntentService;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.unstoppable.submitbuttonview.SubmitButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.db.pojo.AppDatabase;
import co.helixtech.schoolapp.db.pojo.Notice;
import co.helixtech.schoolapp.db.pojo.PostNotice;
import co.helixtech.schoolapp.services.DataSyncService;
import co.helixtech.schoolapp.teacher.view_models.CreateNoticeViewModel;
import co.helixtech.schoolapp.utility.AppExecutors;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;

public class create_notice extends AppCompatActivity {

    public static final String ACTIVITY_MODE = create_notice.class.getCanonicalName() + ".activity_mode";
    public static final int EDIT_MODE = 100;
    public static final int CREATE_MODE = 101;
    public static final String PARAM_NOTICE_ID = "notice_id";

    private boolean editMode;
    private TextInputLayout title_lays, desc_lays;
    private TextInputEditText title_text, desc_text;
    private SubmitButton post_btn;
    private int noticeID;
    private AppExecutors executors;
    private AppDatabase appDatabase;
    private CreateNoticeViewModel viewModel;
    private Receiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_notice);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        receiver = new Receiver();
        executors = new AppExecutors();
        appDatabase = AppDatabase.getInstance(this);
        viewModel = ViewModelProviders.of(this).get(CreateNoticeViewModel.class);
        title_lays = (TextInputLayout) findViewById(R.id.title_layout);
        desc_lays = (TextInputLayout) findViewById(R.id.desc_layout);
        title_text = (TextInputEditText) findViewById(R.id.title_edittext);
        desc_text = (TextInputEditText) findViewById(R.id.desc_edittext);
        post_btn = findViewById(R.id.post_notice);

        post_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    Intent intent = new Intent();
                    intent.setClass(view.getContext(), post_diary_event.class);
                    startActivityForResult(intent, post_diary_event.REQUEST_CODE);
                } else
                    post_btn.reset();
            }
        });


        noticeID = getIntent().getIntExtra(PARAM_NOTICE_ID, -1);
        int mode = getIntent().getIntExtra(ACTIVITY_MODE, CREATE_MODE);
        switch (mode) {
            case CREATE_MODE:
                initCreateMode();
                break;
            case EDIT_MODE:
                initEditMode();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(DataSyncService.Constants.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    private void initEditMode() {
        getSupportActionBar().setTitle("Edit Notice");
        editMode = true;
        viewModel.getNoticeMutableLiveData().observe(this, new Observer<Notice>() {
            @Override
            public void onChanged(@Nullable Notice notice) {
                title_text.setText(notice.getTitle());
                desc_text.setText(notice.getDescription());
            }
        });
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                Notice notice = appDatabase.noticeDao().getNoticeByID(noticeID);
                viewModel.setNoticeMutableLiveData(notice);
            }
        });
    }

    private void initCreateMode() {
        getSupportActionBar().setTitle("New Notice");
        editMode = false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == post_diary_event.REQUEST_CODE) {
            if (resultCode != Activity.RESULT_OK) {
                post_btn.reset();
                return;
            }
            ArrayList<Integer> selected = data.getIntegerArrayListExtra(
                    post_diary_event.ACTIVITY_RESULT);
            Log.d("Studentds selected", selected.toString());

            SharedPreferences preferences = getSharedPreferences(
                    getString(R.string.teacher_preferences), MODE_PRIVATE);

            PostNotice postNotice = new PostNotice();
            postNotice.setDescription(desc_text.getText().toString());
            postNotice.setTitle(title_text.getText().toString());
            postNotice.setStudents(selected);
            postNotice.setTeacherClass(preferences.getInt(getString(R.string.teacher_class_id),
                    -1));
            Intent intent = new Intent();
            intent.setClass(this, DataSyncService.class);
            if (editMode) {
                intent.putExtra(DataSyncService.SERVICE_NAME, DataSyncService.Constants.EDIT_NOTICE);
                intent.putExtra(DataSyncService.Constants.PARAM_NOTICE_ID,noticeID);
            } else {
                intent.putExtra(DataSyncService.SERVICE_NAME, DataSyncService.Constants.POST_NOTICE);
            }
            intent.putExtra(DataSyncService.Constants.PARAM_POST_NOTICE, postNotice);
            startService(intent);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getParentActivityIntent() == null) {
                    onBackPressed();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean validate() {

        if (!check_descp() | !check_title()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean check_descp() {

        String desc = desc_text.getText().toString().trim();
        if (desc.isEmpty()) {
            desc_lays.setErrorEnabled(true);
            desc_lays.setError("Please Write a Description");
            return false;
        } else {
            desc_lays.setErrorEnabled(false);
            return true;
        }
    }

    private boolean check_title() {

        String title = title_text.getText().toString().trim();
        if (title.isEmpty()) {
            title_lays.setErrorEnabled(true);
            title_lays.setError("Please Enter a Title");

            return false;
        } else {
            title_lays.setErrorEnabled(false);
            return true;
        }
    }


    public class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int actionType =
                    intent.getIntExtra(DataSyncService.Constants.BROADCAST_MESSAGE, -1);
            if (actionType == DataSyncService.Constants.NOTICE_POSTED) {
                int noticeID = intent.getIntExtra(DataSyncService.Constants.PARAM_NOTICE_ID, -1);
                Log.d(Receiver.class.getCanonicalName(), "Notice posted " + noticeID);
                post_btn.doResult(true);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 5s = 5000ms
                        onBackPressed();
                    }
                }, 1700);

            }

            else if (actionType == DataSyncService.Constants.NOTICE_EDITED) {
                int noticeID = intent.getIntExtra(DataSyncService.Constants.PARAM_NOTICE_ID, -1);
                Log.d(Receiver.class.getCanonicalName(), "Notice edited " + noticeID);
                post_btn.doResult(true);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 5s = 5000ms
                        onBackPressed();
                    }
                }, 1700);
            }
        }
    }
}
