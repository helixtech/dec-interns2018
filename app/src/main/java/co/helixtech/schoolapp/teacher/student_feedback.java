package co.helixtech.schoolapp.teacher;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.unstoppable.submitbuttonview.SubmitButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.db.pojo.PostFeedback;
import co.helixtech.schoolapp.services.DataSyncService;
import co.helixtech.schoolapp.services.ImageSyncService;
import co.helixtech.schoolapp.utility.TokenSharedPreference;

public class student_feedback extends AppCompatActivity {

    public static String PARAM_STUDENT_ID = "student_id";
    Spinner subjectSpinner, rewardSpinner;
    TextInputEditText remarktext;
    TextInputLayout remark_layout;
    SubmitButton post_feedback;
    TextView studentName;
    LinearLayout star1_layout, star3_layout, star2_layout;
    ImageView star1_img, star2_img, star3_img;
    int selectedreward = 1;
    private int studentID;
    boolean value = true;
    private Receiver receiver;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getParentActivityIntent() == null) {
                    onBackPressed();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_feedback);

        getSupportActionBar().setTitle("Rewards");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        receiver = new Receiver();
        star1_img = (ImageView) findViewById(R.id.star1_image);
        star2_img = (ImageView) findViewById(R.id.star2_image);
        star3_img = (ImageView) findViewById(R.id.star3_image);

        star1_layout = (LinearLayout) findViewById(R.id.star1_layout);
        star2_layout = (LinearLayout) findViewById(R.id.star2_layout);
        star3_layout = (LinearLayout) findViewById(R.id.star3_layout);

        studentID = getIntent().getIntExtra(PARAM_STUDENT_ID, -1);


        star1_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedreward = 1;
                star1_img.setBackgroundResource(R.drawable.onselect);
                star2_img.setBackgroundResource(R.drawable.ondeselect);
                star3_img.setBackgroundResource(R.drawable.ondeselect);
            }
        });

        star2_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedreward = 2;
                star1_img.setBackgroundResource(R.drawable.ondeselect);
                star2_img.setBackgroundResource(R.drawable.onselect);
                star3_img.setBackgroundResource(R.drawable.ondeselect);
            }
        });

        star3_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedreward = 3;
                star1_img.setBackgroundResource(R.drawable.ondeselect);
                star2_img.setBackgroundResource(R.drawable.ondeselect);
                star3_img.setBackgroundResource(R.drawable.onselect);
            }
        });


        remarktext = (TextInputEditText) findViewById(R.id.feedback_remark_edittext);
        remark_layout = (TextInputLayout) findViewById(R.id.feedback_remark_layout);
        post_feedback = (SubmitButton) findViewById(R.id.post_feedback);
        studentName = (TextView) findViewById(R.id.feedback_student_name);

        post_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    PostFeedback feedback = new PostFeedback();
                    String stars = "";
                    if (selectedreward == 1)
                        stars = "1 Star: ";
                    else if (selectedreward == 2)
                        stars = "2 Star: ";
                    else if (selectedreward == 3)
                        stars = "3 Star: ";
                    feedback.setFeedback(stars + remarktext.getText().toString());
                    feedback.setStudent(studentID);
                    TokenSharedPreference preference = new TokenSharedPreference(view.getContext());
                    feedback.setTeacher(preference.getCurrentAccessToken().getTeacherID());

                    Intent intent = new Intent();
                    intent.setClass(view.getContext(), DataSyncService.class);
                    intent.putExtra(DataSyncService.SERVICE_NAME,
                            DataSyncService.Constants.POST_FEEDBACK);
                    intent.putExtra(DataSyncService.Constants.PARAM_POST_FEEDBACK,
                            feedback);
                    startService(intent);
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(DataSyncService.Constants.BROADCAST_ACTION);
        filter.addAction(ImageSyncService.Constants.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

    }

    private boolean checkdescp() {

        String remark = remarktext.getText().toString().trim();
        if (remark.isEmpty()) {

            remark_layout.setErrorEnabled(true);
            remark_layout.setError("Please Enter a Remark");
            return false;
        } else {
            remark_layout.setErrorEnabled(false);
            return true;
        }
    }

    public boolean validate() {
        if (!checkdescp()) {
            return false;

        } else return true;
    }

    public class Receiver extends BroadcastReceiver {

        private void onFeedbackPosted(final Context context, Intent intent) {
            final int id = intent.getIntExtra(DataSyncService.Constants.PARAM_FEEDBACK_ID, -1);
            if (id == -1)
                return;
            Log.d(report_card.Receiver.class.getCanonicalName(), "Feedback Posted");
            post_feedback.doResult(true);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Do something after 5s = 5000ms
                    onBackPressed();
                }
            }, 1700);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            assert action != null;
            if (action.equals(DataSyncService.Constants.BROADCAST_ACTION)) {
                int broadcastMessage = intent
                        .getIntExtra(DataSyncService.Constants.BROADCAST_MESSAGE,
                                -1);
                if (broadcastMessage == DataSyncService.Constants.FEEDBACK_POSTED)
                    onFeedbackPosted(context, intent);
            }
        }
    }

}
