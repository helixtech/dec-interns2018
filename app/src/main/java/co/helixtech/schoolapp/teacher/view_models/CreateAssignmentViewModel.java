package co.helixtech.schoolapp.teacher.view_models;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import co.helixtech.schoolapp.db.pojo.Assignment;
import co.helixtech.schoolapp.db.pojo.PostAssignment;

public class CreateAssignmentViewModel extends ViewModel {
    private MutableLiveData<Assignment> assignmentMutableLiveData;
    private MutableLiveData<PostAssignment> postAssignmentMutableLiveData;

    public CreateAssignmentViewModel()
    {
        assignmentMutableLiveData = new MutableLiveData<>();
        postAssignmentMutableLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<Assignment> getAssignmentMutableLiveData() {
        return assignmentMutableLiveData;
    }

    public void setAssignmentMutableLiveData(Assignment assignment) {
        this.assignmentMutableLiveData.postValue(assignment);
    }

    public MutableLiveData<PostAssignment> getPostAssignmentMutableLiveData() {
        return postAssignmentMutableLiveData;
    }

    public void setPostAssignmentMutableLiveData(PostAssignment postAssignment) {
        this.postAssignmentMutableLiveData.postValue(postAssignment);
    }
}
