package co.helixtech.schoolapp.teacher;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.unstoppable.submitbuttonview.SubmitButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.db.pojo.AppDatabase;
import co.helixtech.schoolapp.db.pojo.ClassActivity;
import co.helixtech.schoolapp.db.pojo.PostActivity;
import co.helixtech.schoolapp.services.DataSyncService;
import co.helixtech.schoolapp.services.ImageSyncService;
import co.helixtech.schoolapp.teacher.view_models.CreateActivityViewModel;
import co.helixtech.schoolapp.utility.AppExecutors;

public class create_activity extends AppCompatActivity {

    public static final String ACTIVITY_MODE = create_notice.class.getCanonicalName() + ".activity_mode";
    public static final int EDIT_MODE = 100;
    public static final int CREATE_MODE = 101;
    public static final String PARAM_ACTIVITY_ID = "activity_id";

    private int activityID;
    private boolean editMode;
    private CreateActivityViewModel viewModel;
    private AppExecutors executors;
    private AppDatabase appDatabase;
    private Receiver receiver;

    private ImageView imagepreview;
    private ImageButton uploadbtn;
    private TextInputLayout title_layout, descp_layout;
    private TextInputEditText title_text, descp_text;
    private SubmitButton postbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_activity);

        executors = new AppExecutors();
        appDatabase = AppDatabase.getInstance(this);
        receiver = new Receiver();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imagepreview = (ImageView) findViewById(R.id.activ_preview_imageview);
        title_layout = (TextInputLayout) findViewById(R.id.acitv_title_layout);
        descp_layout = (TextInputLayout) findViewById(R.id.acitv_desc_layout);
        title_text = (TextInputEditText) findViewById(R.id.acitv_title_edittext);
        descp_text = (TextInputEditText) findViewById(R.id.acitv_desc_edittext);
        postbtn = findViewById(R.id.post_activity);


        imagepreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), 0);

                //startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), 3);
                CropImage.activity(null).setGuidelines(CropImageView.Guidelines.ON).setAspectRatio(2, 1).start(create_activity.this);
            }
        });

        postbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validate()) {
                    //code here
                    Intent intent = new Intent();
                    intent.setClass(view.getContext(), post_diary_event.class);
                    startActivityForResult(intent, post_diary_event.REQUEST_CODE);
                } else
                    postbtn.reset();
            }
        });

        int mode = getIntent().getIntExtra(ACTIVITY_MODE, CREATE_MODE);
        switch (mode) {
            case CREATE_MODE:
                initCreateMode();
                break;
            case EDIT_MODE:
                initEditMode();
                break;
            default:
                break;

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(DataSyncService.Constants.BROADCAST_ACTION);
        filter.addAction(ImageSyncService.Constants.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    private void initEditMode() {
        getSupportActionBar().setTitle("Edit Activity");
        editMode = true;
        activityID = getIntent().getIntExtra(PARAM_ACTIVITY_ID, -1);
        viewModel = ViewModelProviders.of(this).get(CreateActivityViewModel.class);
        viewModel.getClassActivityMutableLiveData().observe(this, new Observer<ClassActivity>() {
            @Override
            public void onChanged(@Nullable ClassActivity activity) {
                title_text.setText(activity.getTitle());
                Picasso.get().load(activity.getImage()).into(imagepreview);
                descp_text.setText(activity.getDescription());
            }
        });
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                ClassActivity classActivity = appDatabase.classActivityDao().
                        getClassActivityByID(activityID);
                viewModel.setClassActivityMutableLiveData(classActivity);
            }
        });
    }

    private void initCreateMode() {
        getSupportActionBar().setTitle("New Activity");
        editMode = false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                ((ImageView) findViewById(R.id.activ_preview_imageview)).setImageURI(result.getUri());
                Toast.makeText(
                        this, "Cropping successful, Sample: " + result.getSampleSize(), Toast.LENGTH_LONG)
                        .show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == post_diary_event.REQUEST_CODE) {

            if (resultCode != RESULT_OK) {
                postbtn.reset();
                return;
            }
            ArrayList<Integer> selected = data.getIntegerArrayListExtra(
                    post_diary_event.ACTIVITY_RESULT);


            SharedPreferences preferences = getSharedPreferences(
                    getString(R.string.teacher_preferences), MODE_PRIVATE);
            PostActivity postActivity = new PostActivity();
            postActivity.setTitle(title_text.getText().toString());
            postActivity.setDescription(descp_text.getText().toString());
            postActivity.setTeacherClass(preferences.getInt(getString(
                    R.string.teacher_class_id), -1));
            postActivity.setStudents(selected);

            Intent serviceIntent = new Intent();
            serviceIntent.setClass(this, DataSyncService.class);
            if (editMode) {
                serviceIntent.putExtra(DataSyncService.SERVICE_NAME,
                        DataSyncService.Constants.EDIT_ACTIVITY);
                serviceIntent.putExtra(DataSyncService.Constants.PARAM_ACTIVTIY_ID,
                        activityID);
            } else {
                serviceIntent.putExtra(DataSyncService.SERVICE_NAME,
                        DataSyncService.Constants.POST_ACTIVITY);
            }

            serviceIntent.putExtra(DataSyncService.Constants.PARAM_POST_ACTIVITY, postActivity);
            startService(serviceIntent);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getParentActivityIntent() == null) {
                    onBackPressed();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean validate() {
        if (!checktitle() | !checkdescp()) {
            return false;

        } else return true;
    }

    private boolean checkdescp() {

        String descp = descp_text.getText().toString().trim();
        if (descp.isEmpty()) {
            descp_layout.setErrorEnabled(true);
            descp_layout.setError("Please Enter a Description");
            return false;
        } else {
            descp_layout.setErrorEnabled(false);
            return true;
        }
    }

    /*private boolean checkimage() {
        if(imagepreview.getDrawable()==null)
        {
            Toast.makeText(this, "Upload image", Toast.LENGTH_SHORT).show();
            return false;
        }
        else return true;
    }*/

    private boolean checktitle() {

        String title = title_text.getText().toString().trim();
        if (title.isEmpty()) {
            title_layout.setErrorEnabled(true);
            title_layout.setError("Please Enter a Title");
            return false;
        } else {
            title_layout.setErrorEnabled(false);
            return true;
        }
    }

    public class Receiver extends BroadcastReceiver {

        private void onActivityPosted(final Context context, Intent intent) {
            final int activityID = intent.getIntExtra(
                    DataSyncService.Constants.PARAM_ACTIVTIY_ID, -1);
            Log.d(Receiver.class.getCanonicalName(), "Activity posted " + activityID);
            final Drawable drawable = imagepreview.getDrawable();
            if (!(drawable instanceof BitmapDrawable)) {
                postbtn.doResult(true);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 5s = 5000ms
                        onBackPressed();
                    }
                }, 1700);

                return;
            }
            executors.diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    try {

                        BitmapDrawable drawable = (BitmapDrawable)
                                imagepreview.getDrawable();
                        Bitmap bitmap = drawable.getBitmap();
                        File file = File.createTempFile(
                                ImageSyncService.Constants.ACTIVITY_FILE_NAME,
                                activityID + ".jpg", context.getCacheDir());
                        FileOutputStream stream = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG,
                                ImageSyncService.IMAGE_QUALITY,
                                stream);
                        bitmap.recycle();
                        Intent serviceIntent = new Intent();
                        serviceIntent.setClass(context, ImageSyncService.class);
                        serviceIntent.putExtra(ImageSyncService.Constants.PARAM_IMAGE_NAME,
                                file.getName());
                        serviceIntent.putExtra(ImageSyncService.SERVICE_NAME,
                                ImageSyncService.Constants.POST_ACTIVITY);
                        serviceIntent.putExtra(ImageSyncService.Constants.PARAM_ACTIVITY_ID,
                                activityID);
                        context.startService(serviceIntent);
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        private void onActivityEdited(final Context context, Intent intent) {
            final int activityID = intent.getIntExtra(
                    DataSyncService.Constants.PARAM_ACTIVTIY_ID, -1);
            Log.d(Receiver.class.getCanonicalName(), "Activity edited " + activityID);
            final Drawable drawable = imagepreview.getDrawable();
            if (!(drawable instanceof BitmapDrawable)) {
                postbtn.doResult(true);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 5s = 5000ms
                        onBackPressed();
                    }
                }, 1700);

                return;
            }
            executors.diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    try {

                        BitmapDrawable drawable = (BitmapDrawable)
                                imagepreview.getDrawable();
                        Bitmap bitmap = drawable.getBitmap();
                        File file = File.createTempFile(
                                ImageSyncService.Constants.ACTIVITY_FILE_NAME,
                                activityID + ".jpg", context.getCacheDir());
                        FileOutputStream stream = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG,
                                ImageSyncService.IMAGE_QUALITY,
                                stream);
                        bitmap.recycle();
                        Intent serviceIntent = new Intent();
                        serviceIntent.setClass(context, ImageSyncService.class);
                        serviceIntent.putExtra(ImageSyncService.Constants.PARAM_IMAGE_NAME,
                                file.getName());
                        serviceIntent.putExtra(ImageSyncService.SERVICE_NAME,
                                ImageSyncService.Constants.POST_ACTIVITY);
                        serviceIntent.putExtra(ImageSyncService.Constants.PARAM_ACTIVITY_ID,
                                activityID);
                        context.startService(serviceIntent);
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        private void onImagePosted(final Context context, Intent intent){
            int activityID = intent.getIntExtra(
                    ImageSyncService.Constants.PARAM_ACTIVITY_ID, -1);
            Log.d(Receiver.class.getCanonicalName(), "Activity picture posted " +
                    activityID);
            postbtn.doResult(true);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Do something after 5s = 5000ms
                    onBackPressed();
                }
            }, 1700);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(DataSyncService.Constants.BROADCAST_ACTION)) {
                switch (intent.getIntExtra(DataSyncService.Constants.BROADCAST_MESSAGE, -1
                )) {
                    case DataSyncService.Constants.ACTIVITY_POSTED:
                        onActivityPosted(context, intent);
                        break;
                    case DataSyncService.Constants.ACTIVITY_EDITED:
                        onActivityEdited(context, intent);
                        break;
                    default:
                        break;

                }
            } else if (intent.getAction().equals(ImageSyncService.Constants.BROADCAST_ACTION)) {
                switch (intent.getIntExtra(ImageSyncService.Constants.BROADCAST_MESSAGE,
                        -1)) {
                    case ImageSyncService.Constants.ACTIVITY_POSTED:
                        onImagePosted(context,intent);
                        break;
                    default:
                        break;
                }
            }
        }
    }


}
