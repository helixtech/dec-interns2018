package co.helixtech.schoolapp.teacher;

import android.widget.ImageView;

/**
 * Created by Tashya Alberto on 20-12-2018.
 */

public class select_students_class {
    private String studentName;
    private ImageView studentProfilePic;

    public select_students_class(String studentName, ImageView studentProfilePic) {
        this.studentName = studentName;
        this.studentProfilePic = studentProfilePic;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public ImageView getStudentProfilePic() {
        return studentProfilePic;
    }

    public void setStudentProfilePic(ImageView studentProfilePic) {
        this.studentProfilePic = studentProfilePic;
    }
}
