package co.helixtech.schoolapp.teacher.view_models;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import co.helixtech.schoolapp.db.pojo.Student;

public class SelectStudentFragmentViewModel extends ViewModel {
    private MutableLiveData<List<Student>> liveData;

    public SelectStudentFragmentViewModel() {
        super();
        liveData = new MutableLiveData<>();
    }

    public void setStudents(List<Student> students)
    {
        liveData.postValue(students);
    }

    public MutableLiveData<List<Student>> getStudentLiveData()
    {
        return liveData;
    }

}
