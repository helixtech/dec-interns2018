package co.helixtech.schoolapp.teacher.view_models;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import co.helixtech.schoolapp.db.pojo.Klass;
import co.helixtech.schoolapp.db.pojo.Student;
import co.helixtech.schoolapp.db.pojo.TeacherClass;

public class StudentProfileViewModel extends ViewModel {

    private MutableLiveData<Student> studentvm;
    private MutableLiveData<Klass> klassname;

    public MutableLiveData<Klass> getKlassname() {
        return klassname;
    }

    public StudentProfileViewModel() {
        studentvm = new MutableLiveData<>();
        klassname = new MutableLiveData<>();
    }

    public MutableLiveData<Student> getStudentvm() { return studentvm; }

    public void setStudentvm(Student studentvm,Klass klassname) { this.studentvm.postValue(studentvm);
    this.klassname.postValue(klassname);}
}
