package co.helixtech.schoolapp.teacher;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.unstoppable.submitbuttonview.SubmitButton;

import java.util.ArrayList;
import java.util.List;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.db.pojo.AppDatabase;
import co.helixtech.schoolapp.db.pojo.Student;
import co.helixtech.schoolapp.teacher.view_models.SelectStudentFragmentViewModel;
import co.helixtech.schoolapp.utility.AppExecutors;

public class post_diary_event extends AppCompatActivity implements View.OnClickListener {
    public static final String ARG_POST_TYPE = post_diary_event.class.getCanonicalName()+
            ".arg_post_type";
    public static final String ACTIVITY_RESULT = post_diary_event.class.getCanonicalName()+
            ".activity_result";
    public static final int REQUEST_CODE =2001;

    private RecyclerView myrv;
    private int postType = 0;
    private List<Student> student_list;
    private SubmitButton sendpostbtn;
    private post_student_adapter myAdapter;
    private SelectStudentFragmentViewModel model;
    private AppExecutors executors;
    private AppDatabase database;
    private Button selectToggle;
    private boolean selectAll = true;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getParentActivityIntent() == null) {
                    onBackPressed();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_diary_event);
        executors = new AppExecutors();
        database = AppDatabase.getInstance(this);
        model = ViewModelProviders.of(this).get(SelectStudentFragmentViewModel.class);
        student_list = new ArrayList<>();
        model.getStudentLiveData().observe(this, new Observer<List<Student>>() {
            @Override
            public void onChanged(@Nullable List<Student> students) {
                student_list.clear();
                student_list.addAll(students);
                myAdapter.notifyDataSetChanged();
            }
        });
        selectToggle = findViewById(R.id.select_toggle);
        sendpostbtn = findViewById(R.id.diary_send_button);
        getSupportActionBar().setTitle("Select Students");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        postType = getIntent().getIntExtra(ARG_POST_TYPE,0);
        myrv = (RecyclerView) findViewById(R.id.diary_students_recycler_id);
        myAdapter = new post_student_adapter(this,student_list);
        myrv.setLayoutManager(new GridLayoutManager(this,4));
        myrv.setAdapter(myAdapter);
        sendpostbtn.setOnClickListener(this) ;
        selectToggle.setOnClickListener(this);

        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                loadData();
            }
        });
    }
    private void loadData()
    {
        SharedPreferences preferences = getSharedPreferences(getString(R.string.teacher_preferences),
                MODE_PRIVATE);
        int classID = preferences.getInt(getString(R.string.class_id),-1);
        if (classID == -1)
            return;
        List<Student> students = database.studentDao().getStudentsOfClass(classID);
        model.setStudents(students);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.diary_send_button:
                handleOnSend();
                break;
            case R.id.select_toggle:
                handleToggle();
                break;

        }
    }

    private void handleOnSend()
    {
        sendpostbtn.reset();
        ArrayList<Integer> selected = myAdapter.getSelected();
        if(selected.size() == 0)
        {
            Toast.makeText(this, "Please Select Students",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent();
        intent.putExtra(ACTIVITY_RESULT,selected);
        setResult(Activity.RESULT_OK,intent);
        finish();
    }

    private void handleToggle()
    {
        if(selectAll){
            myAdapter.checkAll();
            selectAll = false;
            selectToggle.setText("Deselect All");
        }
        else {
            myAdapter.uncheckAll();
            selectAll = true;
            selectToggle.setText("Select All");
        }
    }
}
