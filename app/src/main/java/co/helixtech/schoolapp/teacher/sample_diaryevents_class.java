package co.helixtech.schoolapp.teacher;

public class sample_diaryevents_class {

    String eventtitle;

    public sample_diaryevents_class(String eventtitle) {

        this.eventtitle = eventtitle;
    }

    public String getEventtitle() {
        return eventtitle;
    }

    public void setEventtitle(String eventtitle) {
        this.eventtitle = eventtitle;
    }
}
