package co.helixtech.schoolapp.teacher;

import android.Manifest;
import android.app.Activity;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.UUID;

import co.helixtech.schoolapp.Fragment.ChatFragment;

import co.helixtech.schoolapp.R;
import dmax.dialog.SpotsDialog;
//import dmax.dialog.SpotsDialog;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link teacher_main_fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link teacher_main_fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class teacher_main_fragment extends Fragment {

    private final static String ARG_CLASS_ID = "class_id";
    private final static String ARG_CLASS_NAME = "class_name";
    private static final int PERMISSION_REQUEST_CODE =1000;

    private CardView studentbtn,diarybtn,chatbtn,timetablebtn;
    private String ClassName;
    private int classID;
    private int teacherID;
    AlertDialog alertDialogView;
    private OnFragmentInteractionListener mListener;

    public teacher_main_fragment() {
        // Required empty public constructor
    }



    public static teacher_main_fragment newInstance(int classID, String className) {
        teacher_main_fragment fragment = new teacher_main_fragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CLASS_ID,classID);
        args.putString(ARG_CLASS_NAME,className);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        classID = getArguments().getInt(ARG_CLASS_ID);
        ClassName = getArguments().getString(ARG_CLASS_NAME);


        if(ActivityCompat.checkSelfPermission(getContext(),Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            },PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_teacher_main_fragment, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case PERMISSION_REQUEST_CODE:
            {
                if(grantResults.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(getContext(), "Permission Granted", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_SHORT).show();

            }
            break;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(ClassName);



        studentbtn = (CardView) getView().findViewById(R.id.students_frag_btn);
        diarybtn = (CardView) getView().findViewById(R.id.diary_frag_btn);
        chatbtn = (CardView) getView().findViewById(R.id.chat_frag_btn);
        timetablebtn = (CardView) getView().findViewById(R.id.timetab_frag_btn);


        studentbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getActivity().getSupportFragmentManager().beginTransaction().
                        replace(R.id.teacher_frame_id,fragment_student.newInstance(classID)).
                        addToBackStack("tag").commit();

            }
        });

        diarybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.teacher_frame_id, new fragment_diary()).addToBackStack("tag").commit();
            }
        });


        chatbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.teacher_frame_id, new ChatFragment()).addToBackStack("tag").commit();

            }
        });

        timetablebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder adb = new AlertDialog.Builder(getContext());
                adb.setTitle("Download the Time Table?");
               // adb.setIcon(android.R.drawable.ic_dialog_alert);
                adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if(ActivityCompat.checkSelfPermission(getContext(),Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED)
                        {
                            Toast.makeText(getContext(),"You should grant permission",Toast.LENGTH_SHORT).show();
                            requestPermissions(new String[]{
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                            },PERMISSION_REQUEST_CODE);
                            return;
                        }
                        else {
                            AlertDialog adialog= new SpotsDialog(getContext());
                            adialog.show();
                            adialog.setMessage("Downloading...");

                            String Filename= UUID.randomUUID().toString()+".jpg";
                            Picasso.get()
                                    .load("https://summerinoxford.com/wp-content/uploads/2017/07/2019-SIO-Timetable.jpg")
                                    .into(new SaveImageHelper(getActivity().getBaseContext(),adialog,getContext().getContentResolver(),Filename,"Image description"));


                        }

                    }
                });
                adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();

                    }
                });
                adb.show();




            }
        });


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
