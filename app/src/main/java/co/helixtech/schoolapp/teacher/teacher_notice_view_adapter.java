package co.helixtech.schoolapp.teacher;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.util.List;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.db.pojo.Notice;
import co.helixtech.schoolapp.services.DataSyncService;
import co.helixtech.schoolapp.utility.DateConversionUtility;

public class teacher_notice_view_adapter extends RecyclerView.Adapter<teacher_notice_view_adapter.MyViewHolder> {

    private Context mContext;
    private List<Notice> mData;

    private String pos;

    public teacher_notice_view_adapter(Context mContext, List<Notice> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        LayoutInflater mInflator = LayoutInflater.from(mContext);
        view = mInflator.inflate(R.layout.notice_card, viewGroup, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {

        myViewHolder.Title.setText(mData.get(i).getTitle());
        myViewHolder.Description.setText(mData.get(i).getDescription());
        myViewHolder.options_btn.setVisibility(View.VISIBLE);
        try {
            myViewHolder.Date.setText(
                    DateConversionUtility.dateToStringNormalFormat(mData.get(i).getPostDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        myViewHolder.options_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos = mData.get(i).getTitle();

                PopupMenu popup = new PopupMenu(mContext, view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.diary_options_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new MyMenuItemClickListener(mData.get(i).getId()));
                popup.show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView Title, Description, Date;
        LinearLayout options_btn;

        public MyViewHolder(View itemView) {
            super(itemView);

            Title = (TextView) itemView.findViewById(R.id.titleNotice);
            Description = (TextView) itemView.findViewById(R.id.description_notice);
            Date = (TextView) itemView.findViewById(R.id.date_notice);

            options_btn = (LinearLayout) itemView.findViewById(R.id.notice_options_btn);
        }
    }

    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
        private int noticeID;
        public MyMenuItemClickListener(int noticeID) {
            this.noticeID = noticeID;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {

            switch (menuItem.getItemId()) {
                case R.id.edit_diary_optionbtn:
                    //edit notice
                    Intent intent = new Intent();
                    intent.setClass(mContext,create_notice.class);
                    intent.putExtra(create_notice.ACTIVITY_MODE,create_notice.EDIT_MODE);
                    intent.putExtra(create_notice.PARAM_NOTICE_ID,noticeID);
                    mContext.startActivity(intent);
                    return true;
                case R.id.delete_diary_optionbtn:
                    //delete notice
                    intent = new Intent();
                    intent.setClass(mContext,DataSyncService.class);
                    intent.putExtra(DataSyncService.SERVICE_NAME,
                            DataSyncService.Constants.DELETE_NOTICE);
                    intent.putExtra(DataSyncService.Constants.PARAM_NOTICE_ID,noticeID);
                    mContext.startService(intent);
                    return true;
                default:
            }
            return false;
        }
    }
}
