package co.helixtech.schoolapp.teacher;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.db.pojo.Student;

/**
 * Created by Tashya Alberto on 20-12-2018.
 */

public class select_students_adapter extends RecyclerView.Adapter<select_students_adapter.MyViewHolder> {
    private Context mContext;
    private List<Student> nData;

    public select_students_adapter(Context mContext, List<Student> nData) {
        this.mContext = mContext;
        this.nData = nData;
    }

    @NonNull
    @Override
    public select_students_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view;
        LayoutInflater minflator = LayoutInflater.from(mContext);
        view = minflator.inflate(R.layout.select_students_item,viewGroup,false);

        return new select_students_adapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final select_students_adapter.MyViewHolder myViewHolder, final int i) {

        myViewHolder.studentProfileName.setText(nData.get(i).getFirstName());
        Picasso.get().load(nData.get(i).getImage()).into(myViewHolder.studentProfilePicture);

        final Student student = nData.get(i);


        myViewHolder.onStudentselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext,student_profile.class);
                intent.putExtra(student_profile.ARG_STUDENT_ID,  student.getId());
                mContext.startActivity(intent);
                Toast.makeText(mContext, student.getId().toString(), Toast.LENGTH_SHORT).show();
            }
        });




    }

    @Override
    public int getItemCount() {
        return nData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView studentProfileName;
        ImageView studentProfilePicture;
        CardView onStudentselect;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            studentProfileName = (TextView) itemView.findViewById(R.id.student_profile_name);
            studentProfilePicture = (ImageView) itemView.findViewById(R.id.student_profile_pic);
            onStudentselect = (CardView) itemView.findViewById(R.id.select_students_card_id);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {




                }
            });
        }
    }
}
