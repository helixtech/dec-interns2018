package co.helixtech.schoolapp.teacher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.helixtech.schoolapp.R;

public class student_profile_contact_adapter extends RecyclerView.Adapter<student_profile_contact_adapter.MyViewHolder>{
    private Context mContext;
    private List<student_profile_contact_class> nData;

    public student_profile_contact_adapter(Context mContext, List<student_profile_contact_class> nData) {
        this.mContext = mContext;
        this.nData = nData;
    }

    @NonNull
    @Override
    public student_profile_contact_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view;
        LayoutInflater minflator = LayoutInflater.from(mContext);
        view = minflator.inflate(R.layout.student_profile_contacts_item,viewGroup,false);

        return new student_profile_contact_adapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull student_profile_contact_adapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.contactNumber.setText(nData.get(i).getContactNumber());
        myViewHolder.contactName.setText(nData.get(i).getContactName());

    }

    @Override
    public int getItemCount() {
        return nData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView contactNumber,contactName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            contactNumber = (TextView) itemView.findViewById(R.id.contact_no);
            contactName = (TextView) itemView.findViewById(R.id.contact_person);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + contactNumber.getText().toString()));
                    view.getContext().startActivity(intent);
                }
            });
        }
    }
}
