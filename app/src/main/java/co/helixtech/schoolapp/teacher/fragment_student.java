package co.helixtech.schoolapp.teacher;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.db.pojo.AppDatabase;
import co.helixtech.schoolapp.db.pojo.Student;
import co.helixtech.schoolapp.services.DataSyncService;
import co.helixtech.schoolapp.teacher.view_models.SelectStudentFragmentViewModel;
import co.helixtech.schoolapp.utility.AppExecutors;

public class fragment_student extends Fragment {

    private static final String ARG_CLASS_ID = "arg_class_id";
    private int classID;

    private List<Student> students_list = new ArrayList<>();
    private Receiver receiver;
    private RecyclerView myrv1;
    private select_students_adapter myAdapter;
    private SelectStudentFragmentViewModel viewModel;
    private ProgressBar progressBar;

    public fragment_student() {
        // Required empty public constructor
    }


    public static fragment_student newInstance(int classID) {
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_CLASS_ID,classID);
        fragment_student fragment = new fragment_student();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        classID = getArguments().getInt(ARG_CLASS_ID);
        IntentFilter filter = new IntentFilter();
        filter.addAction(DataSyncService.Constants.BROADCAST_ACTION);
        receiver = new Receiver();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver,filter);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_fragment_student, container, false);
        progressBar = view.findViewById(R.id.progressBar_loadstudent);
        myrv1 = (RecyclerView) view.findViewById(R.id.student_profile_recycler);
        myAdapter = new select_students_adapter(getContext(),students_list);
        myrv1.setLayoutManager(new GridLayoutManager(getActivity(),4));
        myrv1.setAdapter(myAdapter);
        viewModel = ViewModelProviders.of(this).get(SelectStudentFragmentViewModel.class);
        viewModel.getStudentLiveData().observe(this, new Observer<List<Student>>() {
            @Override
            public void onChanged(@Nullable List<Student> students) {
                students_list.clear();
                students_list.addAll(students);
                myAdapter.notifyDataSetChanged();
            }
        });
        return  view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Students");
        progressBar.setVisibility(View.VISIBLE);
        receiver.loadData();
        Intent intent = new Intent();
        intent.setClass(getContext(), DataSyncService.class);
        intent.putExtra(DataSyncService.SERVICE_NAME,
                DataSyncService.Constants.GET_STUDENTS_OF_CLASS);
        intent.putExtra(DataSyncService.Constants.PARAM_CLASS_ID,classID);
        getActivity().startService(intent);
    }



    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(DataSyncService.Constants.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver,filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
    }


    public class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch(intent.getIntExtra(DataSyncService.Constants.BROADCAST_MESSAGE,0))
            {
                case DataSyncService.Constants.STUDENTS_OF_CLASS_SYNCED:
                   loadData();
                   progressBar.setVisibility(View.GONE);
                    break;
                default:
                    progressBar.setVisibility(View.GONE);
                    break;
            }
        }

        public void loadData(){
            new AppExecutors().diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    List<Student> students =
                            AppDatabase.getInstance(getActivity()).studentDao().
                                    getStudentsOfClass(classID);
                    viewModel.setStudents(students);
                }
            });
        }
    }
}
