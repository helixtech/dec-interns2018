package co.helixtech.schoolapp.teacher;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.helixtech.schoolapp.R;

public class student_profile_email_adapter extends RecyclerView.Adapter<student_profile_email_adapter.MyViewHolder> {

    private Context mContext;
    private List<student_profile_email_class> nData;

    public String TO;
    public String[] CC = {""};

    public student_profile_email_adapter(Context mContext, List<student_profile_email_class> nData) {
        this.mContext = mContext;
        this.nData = nData;
    }

    @NonNull
    @Override
    public student_profile_email_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view;
        LayoutInflater minflator = LayoutInflater.from(mContext);
        view = minflator.inflate(R.layout.student_profile_email_item, viewGroup, false);

        return new student_profile_email_adapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull student_profile_email_adapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.contactEmail.setText(nData.get(i).getContactEmail());


    }

    @Override
    public int getItemCount() {
        return nData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView contactEmail;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            contactEmail = (TextView) itemView.findViewById(R.id.email_id);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    TO=contactEmail.getText().toString();
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);

                    emailIntent.setData(Uri.parse("mailto:"));
                    emailIntent.setType("text/plain");
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{TO});
                    emailIntent.putExtra(Intent.EXTRA_CC, CC);
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                    view.getContext().startActivity(Intent.createChooser(emailIntent, "Send mail..."));

                }
            });
        }
    }
}