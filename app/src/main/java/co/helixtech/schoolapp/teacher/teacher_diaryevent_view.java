package co.helixtech.schoolapp.teacher;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.db.pojo.AppDatabase;
import co.helixtech.schoolapp.db.pojo.Assignment;
import co.helixtech.schoolapp.db.pojo.ClassActivity;
import co.helixtech.schoolapp.db.pojo.Notice;
import co.helixtech.schoolapp.services.DataSyncService;
import co.helixtech.schoolapp.teacher.view_models.DiaryViewModel;
import co.helixtech.schoolapp.utility.AppExecutors;

public class teacher_diaryevent_view extends AppCompatActivity {
    public static final String EVENT_TYPE = teacher_diaryevent_view.class.getCanonicalName() +
            ".event_type";
    public static final int ASSIGNMENT_TYPE = 101;
    public static final int NOTICE_TYPE = 102;
    public static final int ACTIVITY_TYPE = 103;

    private FloatingActionButton create_event;
    private RecyclerView myrv;
    private DiaryViewModel diaryViewModel;
    private List<Assignment> assignmentlist;
    private List<Notice> noticelist;
    private List<ClassActivity> activitylist;
    private teacher_assignment_view_adapter assignmentViewAdapter;
    private teacher_activity_view_adapter activityViewAdapter;
    private teacher_notice_view_adapter noticeViewAdapter;
    private ProgressBar progressBar;

    private int eventType;
    private int classID = -1;
    private int teacherClassID = -1;

    private Receiver receiver = new Receiver();
    private AppDatabase database;
    private AppExecutors executors = new AppExecutors();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_diaryevent_view);
        progressBar = findViewById(R.id.progressBar_loaddiary);
        database = AppDatabase.getInstance(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        assignmentlist = new ArrayList<>();
        noticelist = new ArrayList<>();
        activitylist = new ArrayList<>();


        diaryViewModel = ViewModelProviders.of(this).get(DiaryViewModel.class);
        diaryViewModel.getAssignmentMutableLiveData().observe(this, new Observer<List<Assignment>>() {
            @Override
            public void onChanged(@Nullable List<Assignment> assignments) {
                assignmentlist.clear();
                assignmentlist.addAll(assignments);
                if (assignmentViewAdapter != null)
                    assignmentViewAdapter.notifyDataSetChanged();
            }
        });
        diaryViewModel.getNoticeMutableLiveData().observe(this, new Observer<List<Notice>>() {
            @Override
            public void onChanged(@Nullable List<Notice> notices) {
                noticelist.clear();
                noticelist.addAll(notices);
                if (noticeViewAdapter != null)
                    noticeViewAdapter.notifyDataSetChanged();
            }
        });
        diaryViewModel.getActvitityMutableLiveData().observe(this, new Observer<List<ClassActivity>>() {
            @Override
            public void onChanged(@Nullable List<ClassActivity> activities) {
                activitylist.clear();
                activitylist.addAll(activities);
                if (activityViewAdapter != null)
                    activityViewAdapter.notifyDataSetChanged();
            }
        });

        IntentFilter filter = new IntentFilter();
        filter.addAction(DataSyncService.Constants.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);

        create_event = (FloatingActionButton) findViewById(R.id.create_new_diary_event_fab);
        myrv = (RecyclerView) findViewById(R.id.recycler_teacher_diaryview_id);
        myrv.setLayoutManager(new LinearLayoutManager(this));
        eventType = getIntent().getIntExtra(EVENT_TYPE, 0);
        SharedPreferences preferences = getSharedPreferences(getString(R.string.teacher_preferences)
                , MODE_PRIVATE);
        classID = preferences.getInt(getString(R.string.class_id), 0);
        teacherClassID = preferences.getInt(getString(R.string.teacher_class_id), 0);
        create_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (eventType == ASSIGNMENT_TYPE)
                    startActivity(new Intent(teacher_diaryevent_view.this, create_assignment.class));
                else if (eventType == NOTICE_TYPE)
                    startActivity(new Intent(teacher_diaryevent_view.this, create_notice.class));
                else if (eventType == ACTIVITY_TYPE)
                    startActivity(new Intent(teacher_diaryevent_view.this, create_activity.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(DataSyncService.Constants.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
        progressBar.setVisibility(View.VISIBLE);
        switch (eventType) {
            case ASSIGNMENT_TYPE:
                receiver.loadAssignmentsFromDatabase();
                showAssignments();
                break;
            case ACTIVITY_TYPE:
                receiver.loadActivitiesFromDatabase();
                showActivities();
                break;
            case NOTICE_TYPE:
                receiver.loadNoticesFromDatabase();
                showNotices();
        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case android.R.id.home:
                if (getParentActivityIntent() == null) {
                    onBackPressed();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showActivities() {
        getSupportActionBar().setTitle("Activities");
        activityViewAdapter = new teacher_activity_view_adapter(this, activitylist);
        myrv.setAdapter(activityViewAdapter);
        receiver.loadActivitiesFromDatabase();
        Intent intent = new Intent();
        intent.setClass(this, DataSyncService.class);
        intent.putExtra(DataSyncService.SERVICE_NAME,
                DataSyncService.Constants.GET_ACTIVITIES_OF_CLASS);
        intent.putExtra(DataSyncService.Constants.PARAM_CLASS_ID, classID);
        intent.putExtra(DataSyncService.Constants.PARAM_TEACHER_CLASS_ID, teacherClassID);
        startService(intent);
    }

    private void showNotices() {
        getSupportActionBar().setTitle("Notices");
        noticeViewAdapter = new teacher_notice_view_adapter(this, noticelist);
        myrv.setAdapter(noticeViewAdapter);
        receiver.loadNoticesFromDatabase();
        Intent intent = new Intent();
        intent.setClass(this, DataSyncService.class);
        intent.putExtra(DataSyncService.SERVICE_NAME,
                DataSyncService.Constants.GET_NOTICES_OF_CLASS);
        intent.putExtra(DataSyncService.Constants.PARAM_CLASS_ID, classID);
        intent.putExtra(DataSyncService.Constants.PARAM_TEACHER_CLASS_ID, teacherClassID);
        startService(intent);
    }

    private void showAssignments() {
        getSupportActionBar().setTitle("Assignments");
        assignmentViewAdapter = new teacher_assignment_view_adapter(this, assignmentlist);
        myrv.setAdapter(assignmentViewAdapter);
        receiver.loadAssignmentsFromDatabase();
        Intent intent = new Intent();
        intent.setClass(this, DataSyncService.class);
        intent.putExtra(DataSyncService.SERVICE_NAME,
                DataSyncService.Constants.GET_ASSIGNMENTS_OF_CLASS);
        intent.putExtra(DataSyncService.Constants.PARAM_CLASS_ID, classID);
        intent.putExtra(DataSyncService.Constants.PARAM_TEACHER_CLASS_ID, teacherClassID);
        startService(intent);
    }

    public class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, final Intent intent) {
            int broadcastMessage = intent.getIntExtra(DataSyncService.Constants.BROADCAST_MESSAGE, 0);
            switch (broadcastMessage) {
                case DataSyncService.Constants.ASSIGNMENT_DELETED:
                case DataSyncService.Constants.ASSIGNMENTS_OF_CLASS_FETCHED:
                    loadAssignmentsFromDatabase();
                    progressBar.setVisibility(View.GONE);
                    break;
                case DataSyncService.Constants.NOTICE_DELETED:
                case DataSyncService.Constants.NOTICES_OF_CLASS_FETCHED:
                    loadNoticesFromDatabase();
                    progressBar.setVisibility(View.GONE);
                case DataSyncService.Constants.ACTIVITY_DELETED:
                case DataSyncService.Constants.ACTIVITIES_OF_CLASS_FETCHED:
                    loadActivitiesFromDatabase();
                    progressBar.setVisibility(View.GONE);
                default:
                    progressBar.setVisibility(View.GONE);
                    break;
            }
        }

        public void loadActivitiesFromDatabase() {
            executors.diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    List<ClassActivity> activities = database.classActivityDao()
                            .getActivitiesByTeacherClass(teacherClassID);
                    diaryViewModel.setActvitityMutableLiveData(activities);
                }
            });
        }

        public void loadNoticesFromDatabase() {
            executors.diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    List<Notice> notices = database.noticeDao()
                            .getNoticesByTeacherClass(teacherClassID);
                    diaryViewModel.setNoticeMutableLiveData(notices);
                }
            });
        }

        public void loadAssignmentsFromDatabase() {
            executors.diskIO().execute(new Runnable() {
                @Override
                public void run() {
//                            int[] ids =
//                                    intent.getIntArrayExtra(DataSyncService.Constants.PARAM_ASSIGNMENT_ID);
                    List<Assignment> assignments = database.assignmentDao()
                            .getAssignmentsByTeacherClass(teacherClassID);
                    diaryViewModel.setAssignmentMutableLiveData(assignments);
                }
            });
        }
    }
}
