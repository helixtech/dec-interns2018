package co.helixtech.schoolapp.teacher.view_models;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import co.helixtech.schoolapp.db.pojo.ClassActivity;

public class CreateActivityViewModel extends ViewModel {
    private MutableLiveData<ClassActivity> classActivityMutableLiveData;

    public CreateActivityViewModel() {
        super();
        classActivityMutableLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<ClassActivity> getClassActivityMutableLiveData() {
        return classActivityMutableLiveData;
    }

    public void setClassActivityMutableLiveData(ClassActivity classActivity) {
        this.classActivityMutableLiveData.postValue(classActivity);
    }
}
