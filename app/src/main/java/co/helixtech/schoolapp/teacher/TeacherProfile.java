package co.helixtech.schoolapp.teacher;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.net.URI;
import java.util.HashMap;

import co.helixtech.schoolapp.Model.User;
import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.utility.TokenSharedPreference;
import de.hdodenhof.circleimageview.CircleImageView;

public class TeacherProfile extends AppCompatActivity {

    CircleImageView profilepic;
    TextView username;

    ImageButton camneraButton;
    DatabaseReference reference;
    //FirebaseUser firebaseUser;

    StorageReference storageReference;
    private static final int IMAGE_REQUEST=1;
    private Uri ImageUri;
    private StorageTask uploadTask;
    TokenSharedPreference preference;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_teacher_profile);
        preference = new TokenSharedPreference(this);
        id = preference.getCurrentAccessToken().getUserID();
        profilepic= findViewById(R.id.profilepic);
        username= findViewById(R.id.username);
        camneraButton=findViewById(R.id.cameraButton);

        storageReference=FirebaseStorage.getInstance().getReference("uploads");

        //firebaseUser=FirebaseAuth.getInstance().getCurrentUser();
        reference=FirebaseDatabase.getInstance().getReference("Users").child(id+"");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                username.setText(user.getUsername());
                if (user.getImageURL().equals("default")) {
                    profilepic.setImageResource(R.drawable.profile_pic);

                } else {

                    Glide.with(getApplicationContext()).load(user.getImageURL()).into(profilepic);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        camneraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openImage();
            }
        });
    }

    private void openImage() {

        Intent intent=new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,IMAGE_REQUEST);
    }

    private  String getFileExtension(Uri uri )
    {
        ContentResolver contentResolver= this.getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return  mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    private void uploadImage(){

        preference = new TokenSharedPreference(this);
        id = preference.getCurrentAccessToken().getUserID();
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Uploading");
        pd.show();

        if(ImageUri!=null)
        {
            final StorageReference fileReference= storageReference.child(System.currentTimeMillis()
                    +"."+getFileExtension(ImageUri));

            uploadTask = fileReference.putFile(ImageUri);
            uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot,Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if(!task.isSuccessful()){
                        throw task.getException();
                    }

                    return  fileReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if(task.isSuccessful()){
                        Uri downloadUri = task.getResult();
                        String mUri = downloadUri.toString();


                        reference = FirebaseDatabase.getInstance().getReference("Users").child(id+"");

                        HashMap<String, Object> map =new HashMap<>();
                        map.put("imageUrl", mUri);
                        reference.updateChildren(map);


                        pd.dismiss();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"Failed to upload image", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                    }

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                }
            });
        }else
        {
            Toast.makeText(getApplicationContext(),"No image selected", Toast.LENGTH_SHORT).show();
        }
    }

    public void onActivityResult(int requestCode,int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);

        if(requestCode==IMAGE_REQUEST&&resultCode==RESULT_OK
                &&data!=null && data.getData()!=null)
        {
            ImageUri=data.getData();

            if(uploadTask!=null &&uploadTask.isInProgress()) {
                Toast.makeText(getApplicationContext(), "Upload in progress", Toast.LENGTH_SHORT).show();
            }
            else{
                uploadImage();
            }
        }
    }
}
