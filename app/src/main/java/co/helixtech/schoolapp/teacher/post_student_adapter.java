package co.helixtech.schoolapp.teacher;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.util.ArraySet;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.db.pojo.Student;

public class post_student_adapter extends RecyclerView.Adapter<post_student_adapter.MyViewHolder> {

    private Context mcontext;
    private List<Student> mData;
    private ArraySet<Integer> selectedStudents;
    private boolean setAllChecked = false;
    private boolean setAllUnchecked = false;

    public ArrayList<Integer> getSelected() {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < selectedStudents.size(); i++)
            list.add(selectedStudents.valueAt(i));
        return list;
    }

    public post_student_adapter(Context mcontext, List<Student> data) {
        this.selectedStudents = new ArraySet<>();
        this.mcontext = mcontext;
        this.mData = data;
        setAllChecked = false;
        setAllUnchecked = false;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view;
        LayoutInflater minflator = LayoutInflater.from(mcontext);
        view = minflator.inflate(R.layout.post_student_items, viewGroup, false);
        return new post_student_adapter.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, final int i) {
        final Student position = mData.get(i);
        myViewHolder.studentname.setText(mData.get(i).getFirstName());

        if (setAllChecked) {
            myViewHolder.select_student.setVisibility(View.VISIBLE);
            myViewHolder.select_student.setChecked(true);
            selectedStudents.add(position.getId());
        }
        else if (setAllUnchecked){
            myViewHolder.select_student.setVisibility(View.GONE);
            myViewHolder.select_student.setChecked(false);
            selectedStudents.remove(position.getId());
        }
        else{
            if (myViewHolder.select_student.isChecked())
                myViewHolder.select_student.setVisibility(View.VISIBLE);
            else
                myViewHolder.select_student.setVisibility(View.GONE);
        }

        Picasso.get().load(position.getImage()).into(myViewHolder.studentimage);
        myViewHolder.card_diary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!myViewHolder.select_student.isChecked()) {
                    myViewHolder.select_student.setVisibility(View.VISIBLE);
                    myViewHolder.select_student.setChecked(true);
                    selectedStudents.add(position.getId());
                    //Toast.makeText(mcontext, ""+itemlist+" count: " +itemlist.size(), Toast.LENGTH_SHORT).show();
                } else if (myViewHolder.select_student.isChecked()) {
                    //remove from the item list
                    myViewHolder.select_student.setChecked(false);
                    myViewHolder.select_student.setVisibility(View.GONE);
                    selectedStudents.remove(position.getId());
                    //Toast.makeText(mcontext, ""+itemlist+" count: " +itemlist.size(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void checkAll() {
        setAllChecked = true;
        setAllUnchecked = false;
        notifyDataSetChanged();
    }

    public void uncheckAll() {
        setAllUnchecked = true;
        setAllChecked = false;
        notifyDataSetChanged();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView studentname;
        ImageView studentimage;
        CardView card_diary;
        CheckBox select_student;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);


            studentimage = (ImageView) itemView.findViewById(R.id.circleImageView_post_student);
            studentname = (TextView) itemView.findViewById(R.id.textView_post_student);
            card_diary = (CardView) itemView.findViewById(R.id.cardview_diary);
            select_student = (CheckBox) itemView.findViewById(R.id.select_check);
        }
    }


}
