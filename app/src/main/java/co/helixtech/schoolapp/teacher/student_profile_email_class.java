package co.helixtech.schoolapp.teacher;

public class student_profile_email_class {

    private String contactEmail;

    public student_profile_email_class(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }
}
