package co.helixtech.schoolapp.teacher;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.unstoppable.submitbuttonview.SubmitButton;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.db.pojo.AppDatabase;
import co.helixtech.schoolapp.db.pojo.Assignment;
import co.helixtech.schoolapp.db.pojo.PostAssignment;
import co.helixtech.schoolapp.services.DataSyncService;
import co.helixtech.schoolapp.services.ImageSyncService;
import co.helixtech.schoolapp.teacher.view_models.CreateAssignmentViewModel;
import co.helixtech.schoolapp.utility.AppExecutors;
import co.helixtech.schoolapp.utility.DateConversionUtility;

public class create_assignment extends AppCompatActivity {


    public static final String ACTIVITY_MODE = create_assignment.class.getCanonicalName() +
            ".activity_mode";
    public static final String ARG_ASSIGNMENT_ID = create_assignment.class.getCanonicalName()
            + ".arg_assignment_id";
    public static final int EDIT_MODE = 101;
    public static final int CREATE_MODE = 102;

    private int assignmentID;
    private PostAssignment postAssignment;
    private TextInputEditText date_picked;
    private SubmitButton pushbtn;
    private TextInputEditText descptext, titletext;
    private TextInputLayout descp_layout, titlelayout;
    private boolean editMode = false;
    private ImageView imagepreview;
    private CreateAssignmentViewModel viewModel;
    private AppDatabase appDatabase;
    private AppExecutors executors;
    private SharedPreferences preferences;
    private Calendar myCalendar;
    private Receiver receiver;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                ((ImageView) findViewById(R.id.preview_imageview)).setImageURI(result.getUri());
                Toast.makeText(
                        this, "Cropping successful, Sample: " + result.getSampleSize(), Toast.LENGTH_LONG)
                        .show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == post_diary_event.REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                pushbtn.reset();
                return;
            }
            try {
                ArrayList<Integer> selectedStudents = data.
                        getIntegerArrayListExtra(post_diary_event.ACTIVITY_RESULT);
                Log.d(this.getClass().getName(), selectedStudents.toString());
                //send request to send data here
                postAssignment.setTitle(titletext.getText().toString());
                postAssignment.setDescription(descptext.getText().toString());
                postAssignment.setTeacherClass(preferences.getInt(getString(R.string.teacher_class_id),
                        -1));
                postAssignment.setStudents(selectedStudents);
                Date deadlineDate = myCalendar.getTime();
                postAssignment.setDeadline(DateConversionUtility.dateToString(deadlineDate));
                Log.d(create_assignment.class.getName(), postAssignment.toString());

                Intent startServiceIntent = new Intent();
                startServiceIntent.setClass(this, DataSyncService.class);
                if (editMode) {
                    startServiceIntent.putExtra(DataSyncService.SERVICE_NAME,
                            DataSyncService.Constants.EDIT_ASSIGNMENT);
                    startServiceIntent.putExtra(DataSyncService.Constants.PARAM_ASSIGNMENT_ID,
                            assignmentID);
                } else
                    startServiceIntent.putExtra(DataSyncService.SERVICE_NAME,
                            DataSyncService.Constants.POST_ASSIGNMENT);
                startServiceIntent.putExtra(DataSyncService.Constants.PARAM_POST_ASSIGNMENT,
                        postAssignment);
                startService(startServiceIntent);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getParentActivityIntent() == null) {
                    onBackPressed();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initializeCreateMode() {
        getSupportActionBar().setTitle("New Assignment");
        editMode = false;
        viewModel.getPostAssignmentMutableLiveData().observe(this, new Observer<PostAssignment>() {
            @Override
            public void onChanged(@Nullable PostAssignment postAssignment) {
                //change ui here
            }
        });
    }

    private void initializeEditMode() {
        getSupportActionBar().setTitle("Edit Assignment");
        editMode = true;
        viewModel.getAssignmentMutableLiveData().observe(this, new Observer<Assignment>() {
            @Override
            public void onChanged(@Nullable Assignment assignment) {
                try {
                    Picasso.get().load(assignment.getImage()).into(imagepreview);
                    descptext.setText(assignment.getDescription());
                    titletext.setText(assignment.getTitle());
                    date_picked.setText(DateConversionUtility
                            .dateToStringReadableFormat(assignment.getDeadlineDate()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        assignmentID = getIntent().getIntExtra(ARG_ASSIGNMENT_ID, -1);
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                List<Assignment> assignments =
                        appDatabase.assignmentDao().getAssignmentsByIDs(assignmentID);
                viewModel.setAssignmentMutableLiveData(assignments.get(0));
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_assignment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        postAssignment = new PostAssignment();
        preferences = getSharedPreferences(getString(R.string.teacher_preferences), MODE_PRIVATE);
        appDatabase = AppDatabase.getInstance(this);
        executors = new AppExecutors();
        imagepreview = (ImageView) findViewById(R.id.preview_imageview);
        pushbtn = findViewById(R.id.post_assignment);
        descp_layout = (TextInputLayout) findViewById(R.id.assign_desc_layout);
        titlelayout = (TextInputLayout) findViewById(R.id.assignment_layout_id);
        descptext = (TextInputEditText) findViewById(R.id.assign_desc_edittext);
        titletext = (TextInputEditText) findViewById(R.id.assignment_text_id);
        viewModel = ViewModelProviders.of(this).get(CreateAssignmentViewModel.class);
        myCalendar = Calendar.getInstance();
        receiver = new Receiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(DataSyncService.Constants.BROADCAST_ACTION);
        date_picked = (TextInputEditText) findViewById(R.id.date_text);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {


            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

            private void updateLabel() {

                String myFormat = "EEE, MMM d, yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                date_picked.setText(sdf.format(myCalendar.getTime()));
            }

        };


        date_picked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePickerDialog dp = new DatePickerDialog(create_assignment.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                dp.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis());
                dp.show();
            }
        });


        imagepreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), 0);

                CropImage.activity(null).setGuidelines(CropImageView.Guidelines.ON).setAspectRatio(2, 1).start(create_assignment.this);


            }
        });

        pushbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    Intent intent = new Intent();
                    intent.setClass(view.getContext(), post_diary_event.class);
                    startActivityForResult(intent, post_diary_event.REQUEST_CODE);
                } else
                    pushbtn.reset();
            }
        });

        Intent intent = getIntent();
        int mode = intent.getIntExtra(ACTIVITY_MODE, CREATE_MODE);
        switch (mode) {
            case CREATE_MODE:
                initializeCreateMode();
                break;
            case EDIT_MODE:
                initializeEditMode();
                break;
            default:
                break;

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        receiver = new Receiver();
        filter.addAction(ImageSyncService.Constants.BROADCAST_ACTION);
        filter.addAction(DataSyncService.Constants.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    public boolean validate() {
        if (!checkdate() | !checkdescp() | !checktitle()) {
            pushbtn.reset();
            return false;

        } else return true;
    }

    private boolean checkdate() {

        String date = date_picked.getText().toString().trim();
        if (date.isEmpty()) {

            date_picked.setError("Please Pick a Deadline");
            return false;
        } else {
            date_picked.setError(null);
            return true;
        }
    }

    private boolean checkdescp() {

        String descp = descptext.getText().toString().trim();
        if (descp.isEmpty()) {

            descp_layout.setErrorEnabled(true);
            descp_layout.setError("Please Write a Description");
            return false;
        } else {
            descp_layout.setErrorEnabled(false);
            return true;
        }
    }

    private boolean checktitle() {

        String title = titletext.getText().toString().trim();
        if (title.isEmpty()) {

            titlelayout.setErrorEnabled(true);
            titlelayout.setError("Please Enter a Title");
            return false;
        } else {
            titlelayout.setErrorEnabled(false);
            return true;
        }
    }

    public class Receiver extends BroadcastReceiver {

        private void onAssignmentPosted(final Context context, Intent intent){
            final int postedID = intent.getIntExtra(DataSyncService.Constants.PARAM_ASSIGNMENT_ID, -1);
            Log.d(create_assignment.class.getName(), "Assignment posted" + postedID);
            if (!(imagepreview.getDrawable() instanceof BitmapDrawable)) {
                pushbtn.doResult(true);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 5s = 5000ms
                        onBackPressed();
                    }
                }, 1700);
                return;
            }
            executors.diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    try {

                        BitmapDrawable drawable = (BitmapDrawable)
                                imagepreview.getDrawable();
                        Bitmap bitmap = drawable.getBitmap();
                        File file = File.createTempFile(
                                ImageSyncService.Constants.ASSIGNMENT_FILE_NAME,
                                postedID + ".jpg", context.getCacheDir());
                        FileOutputStream stream = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG,
                                ImageSyncService.IMAGE_QUALITY,
                                stream);
                        bitmap.recycle();
                        Intent serviceIntent = new Intent();
                        serviceIntent.setClass(context, ImageSyncService.class);
                        serviceIntent.putExtra(ImageSyncService.Constants.PARAM_IMAGE_NAME,
                                file.getName());
                        serviceIntent.putExtra(ImageSyncService.SERVICE_NAME,
                                ImageSyncService.Constants.POST_ASSIGNMENT);
                        serviceIntent.putExtra(ImageSyncService.Constants.PARAM_ASSIGNMENT_ID,
                                postedID);
                        context.startService(serviceIntent);
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
        private void onAssignmentEdited(final Context context, Intent intent){
            final int postedID2 = intent.getIntExtra(DataSyncService.Constants.PARAM_ASSIGNMENT_ID, -1);
            Log.d(create_assignment.class.getName(), "Assignment edited" + postedID2);
            if (!(imagepreview.getDrawable() instanceof BitmapDrawable)) {
                pushbtn.doResult(true);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 5s = 5000ms
                        onBackPressed();
                    }
                }, 1700);
                return;
            }
            executors.diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    try {

                        BitmapDrawable drawable = (BitmapDrawable)
                                imagepreview.getDrawable();
                        Bitmap bitmap = drawable.getBitmap();
                        File file = File.createTempFile(
                                ImageSyncService.Constants.ASSIGNMENT_FILE_NAME,
                                postedID2 + ".jpg", context.getCacheDir());
                        FileOutputStream stream = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG,
                                ImageSyncService.IMAGE_QUALITY,
                                stream);
                        bitmap.recycle();
                        Intent serviceIntent = new Intent();
                        serviceIntent.setClass(context, ImageSyncService.class);
                        serviceIntent.putExtra(ImageSyncService.Constants.PARAM_IMAGE_NAME,
                                file.getName());
                        serviceIntent.putExtra(ImageSyncService.SERVICE_NAME,
                                ImageSyncService.Constants.POST_ASSIGNMENT);
                        serviceIntent.putExtra(ImageSyncService.Constants.PARAM_ASSIGNMENT_ID,
                                postedID2);
                        context.startService(serviceIntent);
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
        private void onImagePosted(final Context context, Intent intent){
            int postedID = intent.getIntExtra(
                    ImageSyncService.Constants.PARAM_ASSIGNMENT_ID,
                    -1);
            Log.d(create_assignment.class.getName(), "Assignment image " +
                    "posted" + postedID);
            pushbtn.doResult(true);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Do something after 5s = 5000ms
                    onBackPressed();
                }
            }, 1700);
        }

        @Override
        public void onReceive( Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(DataSyncService.Constants.BROADCAST_ACTION)) {
                switch (intent.getIntExtra(DataSyncService.Constants.BROADCAST_MESSAGE, -1)) {
                    case DataSyncService.Constants.ASSIGNMENT_POSTED:
                        onAssignmentPosted(context,intent);
                        break;
                    case DataSyncService.Constants.ASSIGNMENT_EDITED:
                        onAssignmentEdited(context,intent);
                        break;
                    default:
                        break;
                }
            } else if (action.equals(ImageSyncService.Constants.BROADCAST_ACTION)) {
                int message = intent.getIntExtra(ImageSyncService.Constants.BROADCAST_MESSAGE,
                        -1);
                switch (message) {
                    case ImageSyncService.Constants.ASSIGNMENT_POSTED:
                        onImagePosted(context, intent);
                        break;
                }
            }

        }
    }
}
