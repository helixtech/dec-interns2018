package co.helixtech.schoolapp.teacher;


import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog;
import com.unstoppable.submitbuttonview.SubmitButton;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.db.pojo.PostAttendance;
import co.helixtech.schoolapp.services.DataSyncService;
import co.helixtech.schoolapp.services.ImageSyncService;
import co.helixtech.schoolapp.utility.DateConversionUtility;

public class attendance extends AppCompatActivity {

    public static String PARAM_STUDENT_ID = "student_id";
    TextView studentName;
    TextInputEditText datePicked;
    TextInputEditText daysPresent;
    TextInputEditText daysAbsent;
    TextInputLayout datePickedLayout, daysPresentLayout, daysAbsentLayout;
    Button confirmAttendance;
    SubmitButton postAttendance;
    TextView presentdaysdata, absentdaysdata, attandancedata, yearmonthdata;
    float daysPres, daysAbs, totalAttndance;
    CardView cardSummary;
    boolean value = true;
    int studentID;
    private Calendar myCalendar;
    private Receiver receiver;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getParentActivityIntent() == null) {
                    onBackPressed();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);

        getSupportActionBar().setTitle("Attendance");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        receiver = new Receiver();
        datePicked = (TextInputEditText) findViewById(R.id.date_edittext);
        daysPresent = (TextInputEditText) findViewById(R.id.days_present_edittext);
        daysAbsent = (TextInputEditText) findViewById(R.id.days_absent_edittext);
        datePickedLayout = (TextInputLayout) findViewById(R.id.date_layout);
        daysPresentLayout = (TextInputLayout) findViewById(R.id.days_present_layout);
        daysAbsentLayout = (TextInputLayout) findViewById(R.id.days_absent_layout);
        studentName = (TextView) findViewById(R.id.attendance_student_name);
        postAttendance = (SubmitButton) findViewById(R.id.post_attendance);
        presentdaysdata = (TextView) findViewById(R.id.PresentDaysData);
        absentdaysdata = (TextView) findViewById(R.id.AbsentDaysData);
        attandancedata = (TextView) findViewById(R.id.AttendanceData);
        yearmonthdata = (TextView) findViewById(R.id.MonthandYearData);
        confirmAttendance = (Button) findViewById(R.id.confirm_attendance);
        cardSummary = (CardView) findViewById(R.id.summary_card);

        cardSummary.setVisibility(View.GONE);

        myCalendar = Calendar.getInstance();

        studentID = getIntent().getIntExtra(PARAM_STUDENT_ID,-1);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

            private void updateLabel() {
                String myFormat = " MMM yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                datePicked.setText(sdf.format(myCalendar.getTime()));
            }
        };

        datePicked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog dp = new DatePickerDialog(attendance.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                //dp.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis());
                dp.show();
            }
        });

        confirmAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validate()) {
                    Toast.makeText(attendance.this, "Enter Details", Toast.LENGTH_SHORT).show();
                } else
                    confirm();
            }
        });

        postAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validate()) {
                    try {
                        PostAttendance postAttendance = new PostAttendance();
                        Date date1 = myCalendar.getTime();
                        postAttendance.setDate(DateConversionUtility.dateToString2(date1));
                        postAttendance.setStudent(studentID);
                        SharedPreferences preferences = getSharedPreferences(
                                getString(R.string.teacher_preferences), MODE_PRIVATE);

                        postAttendance.setTeacherClass(preferences.getInt(getString(R.string.teacher_class_id),
                                -1));
                        daysPres = Float.parseFloat(daysPresent.getText().toString());
                        daysAbs = Float.parseFloat(daysAbsent.getText().toString());
                        postAttendance.setTotalDaysAbsent((int) daysAbs);
                        postAttendance.setTotalDaysPresent((int) daysPres);


                        Intent intent = new Intent();
                        intent.setClass(view.getContext(), DataSyncService.class);
                        intent.putExtra(DataSyncService.SERVICE_NAME,
                                DataSyncService.Constants.POST_ATTENDANCE);
                        intent.putExtra(DataSyncService.Constants.PARAM_POST_ATTENDANCE,
                                postAttendance);
                        startService(intent);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(DataSyncService.Constants.BROADCAST_ACTION);
        filter.addAction(ImageSyncService.Constants.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

    }

    private boolean checkdate() {

        String date = datePicked.getText().toString().trim();
        if (date.isEmpty()) {

            datePickedLayout.setErrorEnabled(true);
            datePickedLayout.setError("Enter a Date");
            return false;
        } else {
            datePickedLayout.setErrorEnabled(false);
            return true;
        }
    }

    private boolean checkpresent() {

        String present = daysPresent.getText().toString().trim();
        if (present.isEmpty()) {

            daysPresentLayout.setErrorEnabled(true);
            daysPresentLayout.setError("Enter a Value");
            return false;
        } else {
            daysPresentLayout.setErrorEnabled(false);
            return true;
        }
    }

    private boolean checkabsent() {

        String absent = daysAbsent.getText().toString().trim();
        if (absent.isEmpty()) {

            daysAbsentLayout.setErrorEnabled(true);
            daysAbsentLayout.setError("Enter a Value");
            return false;
        } else {
            daysAbsentLayout.setErrorEnabled(false);
            return true;
        }
    }


    public boolean validate() {
        if (!checkdate() | !checkpresent() | !checkabsent()) {
            return false;

        } else return true;
    }

    public void confirm() {
        cardSummary.setVisibility(View.VISIBLE);
        daysPres = Float.parseFloat(daysPresent.getText().toString());
        daysAbs = Float.parseFloat(daysAbsent.getText().toString());


        yearmonthdata.setText(datePicked.getText().toString());
        totalAttndance = (daysPres / (daysPres + daysAbs)) * 100;
        presentdaysdata.setText(daysPresent.getText().toString());
        absentdaysdata.setText(daysAbsent.getText().toString());
        attandancedata.setText(String.valueOf(totalAttndance));

    }

    public class Receiver extends BroadcastReceiver {

        private void onAttendancePosted(final Context context, Intent intent) {
            final int id = intent.getIntExtra(DataSyncService.Constants.PARAM_ATTENDANCE_ID, -1);
            if (id == -1)
                return;
            Log.d(report_card.Receiver.class.getCanonicalName(), "Attendance Posted");
            postAttendance.doResult(true);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Do something after 5s = 5000ms
                    onBackPressed();
                }
            }, 1700);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            assert action != null;
            if (action.equals(DataSyncService.Constants.BROADCAST_ACTION)) {
                int broadcastMessage = intent
                        .getIntExtra(DataSyncService.Constants.BROADCAST_MESSAGE,
                                -1);
                if (broadcastMessage == DataSyncService.Constants.ATTENDANCE_POSTED)
                    onAttendancePosted(context, intent);
            }
        }
    }
}
