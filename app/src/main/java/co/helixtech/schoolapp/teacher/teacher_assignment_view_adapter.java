package co.helixtech.schoolapp.teacher;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.util.List;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.db.pojo.Assignment;
import co.helixtech.schoolapp.services.DataSyncService;
import co.helixtech.schoolapp.services.ImageSyncService;
import co.helixtech.schoolapp.utility.DateConversionUtility;

public class teacher_assignment_view_adapter extends RecyclerView.Adapter<teacher_assignment_view_adapter.MyViewHolder> {

    private Context mContext;
    private List<Assignment> mData;

    private String pos;

    public teacher_assignment_view_adapter(Context mContext, List<Assignment> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view;
        LayoutInflater mInflator = LayoutInflater.from(mContext);
        view = mInflator.inflate(R.layout.assignment_card,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, final int i) {


        myViewHolder.Title.setText(mData.get(i).getTitle());
        try {
            myViewHolder.Deadline.setText(DateConversionUtility.
                    dateToStringNormalFormat(mData.get(i).getDeadlineDate()));
            myViewHolder.Date.setText(DateConversionUtility.dateToStringNormalFormat
                    (mData.get(i).getPostDate()
            ));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        myViewHolder.Description.setText(mData.get(i).getDescription());
        myViewHolder.options_btn.setVisibility(View.VISIBLE);

        myViewHolder.options_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos = mData.get(i).getTitle();
                PopupMenu popup = new PopupMenu(mContext, view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.diary_options_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new MyMenuItemClickListener(mData.get(i).getId()));
                popup.show();

            }
        });

    }

    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
        private int assignmentID = -1;

        public MyMenuItemClickListener( int assignmentID) {
            this.assignmentID = assignmentID;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {

            switch (menuItem.getItemId()) {
                case R.id.edit_diary_optionbtn:
                    //edit assignment
                    Intent intent = new Intent();
                    intent.setClass(mContext,create_assignment.class);
                    intent.putExtra(create_assignment.ACTIVITY_MODE,create_assignment.EDIT_MODE);
                    intent.putExtra(create_assignment.ARG_ASSIGNMENT_ID,assignmentID);
                    mContext.startActivity(intent);
                    return true;
                case R.id.delete_diary_optionbtn:
                    //delete assignment
                    intent = new Intent();
                    intent.setClass(mContext,DataSyncService.class);
                    intent.putExtra(DataSyncService.SERVICE_NAME,
                            DataSyncService.Constants.DELETE_ASSIGNMENT);
                    intent.putExtra(DataSyncService.Constants.PARAM_ASSIGNMENT_ID,assignmentID);
                    mContext.startService(intent);
                    return true;
                default:
            }
            return false;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView Title,Deadline,Description,Teachername,Date;
        LinearLayout options_btn;

        public MyViewHolder(View itemView){
            super(itemView);

            Title = (TextView)  itemView.findViewById(R.id.title_assignment);
            Description = (TextView)  itemView.findViewById(R.id.description_assignment);
            Deadline = (TextView)  itemView.findViewById(R.id.deadline_assignment);
            Teachername = (TextView)  itemView.findViewById(R.id.teacher_assignment);
            Date = (TextView)  itemView.findViewById(R.id.date_assignment);
            options_btn = (LinearLayout) itemView.findViewById(R.id.assignment_options_btn);
        }
    }
}
