package co.helixtech.schoolapp.teacher;

import android.app.ActionBar;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import co.helixtech.schoolapp.MainActivity;
import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.db.pojo.AppDatabase;
import co.helixtech.schoolapp.db.pojo.Klass;
import co.helixtech.schoolapp.db.pojo.Parent;
import co.helixtech.schoolapp.db.pojo.ParentChild;
import co.helixtech.schoolapp.db.pojo.Student;
import co.helixtech.schoolapp.db.pojo.TeacherClass;
import co.helixtech.schoolapp.services.DataSyncService;
import co.helixtech.schoolapp.teacher.view_models.SelectClassActivityViewModel;
import co.helixtech.schoolapp.teacher.view_models.StudentProfileViewModel;
import co.helixtech.schoolapp.utility.AppExecutors;


public class student_profile extends AppCompatActivity {

    public static final String ARG_STUDENT_ID = "student_id";

    private FloatingActionButton btnnew, btnattendance, btnrewards, btnreports;
    private TextView txtnew, txtattendance, txtrewards, txtreports;
    private AppExecutors appExecutors;
    private StudentProfileViewModel viewModel;
    private ImageView Studentprofilepic;
    private TextView classname, rollno, registrationno;
    private TextView bloodgroup;
    private CollapsingToolbarLayout titlebar;
    private AppDatabase appDatabase = null;
    int student_id;
    private View view1,view2;
    private Student student;
    private Klass teacherclassname;
    private Reciever reciever;
    private RecyclerView myrv;
    private RecyclerView myrv1;
    private student_profile_contact_adapter myAdapter;
    private student_profile_email_adapter myAdapter1;
    private ImageView blurLayout;


    List<student_profile_contact_class> student_contacts_list;
    List<student_profile_email_class> student_email_list;

    Animation fabopen, fabclose, clockwise, anticlockwise;
    boolean isOpen = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_profile);

        StatusBarUtil.setColor(this,getResources().getColor(R.color.colorPrimaryLight));


        view1 = findViewById(R.id.view1);
        view2 = findViewById(R.id.view2);
        reciever = new Reciever();

        blurLayout = findViewById(R.id.blurLayout);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DataSyncService.Constants.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(reciever, intentFilter);

        classname = (TextView) findViewById(R.id.classname);
        rollno = (TextView) findViewById(R.id.roll_no_value);
        registrationno = (TextView) findViewById(R.id.registration_no_value);
        bloodgroup = (TextView) findViewById(R.id.bloodgroup);

        titlebar = (CollapsingToolbarLayout) findViewById(R.id.collapsingtoolbar);
        Studentprofilepic = (ImageView) findViewById(R.id.student_display_picture_id);

        student_contacts_list = new ArrayList<>();           //populating student contact information
        myrv = (RecyclerView) findViewById(R.id.student_profile_contacts);
        myAdapter = new student_profile_contact_adapter(student_profile.this, student_contacts_list);
        myrv.setLayoutManager(new LinearLayoutManager(student_profile.this));
        myrv.setAdapter(myAdapter);


        student_email_list = new ArrayList<>();
        myrv1 = (RecyclerView) findViewById(R.id.student_profile_email);
        myAdapter1 = new student_profile_email_adapter(this, student_email_list);
        myrv1.setLayoutManager(new LinearLayoutManager(this));
        myrv1.setAdapter(myAdapter1);


        appExecutors = new AppExecutors();


        viewModel = ViewModelProviders.of(this).get(StudentProfileViewModel.class);
        viewModel.getStudentvm().observe(this, new Observer<Student>() {
            @Override
            public void onChanged(@Nullable Student student) {
                if (student != null) {
                    titlebar.setTitle(student.getFirstName() + " " + student.getSurname());
                    rollno.setText(student.getRollNo());
                    bloodgroup.setText("Blood group: " + student.getBloodGroup());
                    Picasso.get().load(student.getImage()).into(Studentprofilepic);

                    Intent intent = new Intent();
                    intent.setClass(getApplicationContext(), DataSyncService.class);
                    intent.putExtra(DataSyncService.SERVICE_NAME, DataSyncService.Constants.GET_PARENT_BY_STUDENT_ID);
                    intent.putExtra(DataSyncService.Constants.PARAM_STUDENT_ID, student_id);
                    startService(intent);
                }
            }

        });

        viewModel.getKlassname().observe(this, new Observer<Klass>() {
            @Override
            public void onChanged(@Nullable Klass klass) {
                classname.setText("Class: " + klass.getClassName());
            }
        });


        student_id = getIntent().getExtras().getInt(ARG_STUDENT_ID);
        appExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                appDatabase = AppDatabase.getInstance(student_profile.this);
                student = appDatabase.studentDao().getStudent(student_id);
                teacherclassname = appDatabase.klassDao().getClass(student.getClassId());

                viewModel.setStudentvm(student, teacherclassname);

            }
        });


        //


        btnnew = (FloatingActionButton) findViewById(R.id.fab_new);
        btnattendance = (FloatingActionButton) findViewById(R.id.fab_attendance);
        btnrewards = (FloatingActionButton) findViewById(R.id.fab_rewards);
        btnreports = (FloatingActionButton) findViewById(R.id.fab_reports);

        //txtnew = (TextView) findViewById(R.id.textview_new);
        txtattendance = (TextView) findViewById(R.id.textview_attendance);
        txtrewards = (TextView) findViewById(R.id.textview_rewards);
        txtreports = (TextView) findViewById(R.id.textview_reports);

        fabopen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fabclose = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        clockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
        anticlockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.offrotate);


        //populating student email info


        btnnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NestedScrollView view = (NestedScrollView) findViewById(R.id.student_profile_layout);

                view.setDrawingCacheEnabled(true);

                view.buildDrawingCache();

                Bitmap bm = view.getDrawingCache();

                Bitmap.createScaledBitmap(bm, bm.getWidth(), bm.getHeight(), false);


                if (isOpen) {
                    btnattendance.startAnimation(fabclose);
                    btnrewards.startAnimation(fabclose);
                    btnreports.startAnimation(fabclose);

                    txtattendance.startAnimation(fabclose);
                    txtrewards.startAnimation(fabclose);
                    txtreports.startAnimation(fabclose);

                    btnnew.startAnimation(anticlockwise);
                    btnattendance.setClickable(false);
                    btnrewards.setClickable(false);
                    btnreports.setClickable(false);
                    blurLayout.setVisibility(View.GONE);


                } else {
                    btnattendance.startAnimation(fabopen);
                    btnrewards.startAnimation(fabopen);
                    btnreports.startAnimation(fabopen);

                    txtattendance.startAnimation(fabopen);
                    txtrewards.startAnimation(fabopen);
                    txtreports.startAnimation(fabopen);

                    btnnew.startAnimation(clockwise);
                    btnattendance.setClickable(true);
                    btnrewards.setClickable(true);
                    btnreports.setClickable(true);
                    blurLayout.setVisibility(View.VISIBLE);


                }
                isOpen = !isOpen;
            }
        });


        btnreports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), report_card.class);
                intent.putExtra(report_card.PARAM_STUDENT_ID, student_id);
                startActivity(intent);
            }
        });

        btnrewards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), student_feedback.class);
                intent.putExtra(student_feedback.PARAM_STUDENT_ID, student_id);
                startActivity(intent);
            }
        });

        btnattendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), attendance.class);
                intent.putExtra(attendance.PARAM_STUDENT_ID,student_id);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        reciever = new Reciever();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DataSyncService.Constants.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(reciever, intentFilter);

    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(reciever);
    }

    public class Reciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            int code = intent.getIntExtra(DataSyncService.Constants.BROADCAST_MESSAGE, -1);
            if (code == DataSyncService.Constants.PARENT_FETCHED) {

                ArrayList<ParentChild> parentChildren = (ArrayList<ParentChild>)
                        intent.getBundleExtra(DataSyncService.Constants.PARAM_PARENT_LIST)
                                .getSerializable(DataSyncService.Constants.PARAM_PARENT_LIST);
               // Toast.makeText(context, parentChildren.get(0).getRelation(), Toast.LENGTH_SHORT).show();


                student_contacts_list.clear();
                student_email_list.clear();


                for (int i = 0; i < parentChildren.size(); i++) {

                    if (i == 2)
                        break;

                    Parent parent = parentChildren.get(i).getParent();

                    String parenttype = parentChildren.get(i).getRelation();
                    if (parenttype.equals("M"))
                        parenttype = "Mother";
                    else if (parenttype.equals("F"))
                        parenttype = "Father";
                    else if (parenttype.equals("G"))
                        parenttype = "Guardian";

                    student_email_list.add(new student_profile_email_class(parent.getEmailId()));


                    student_contacts_list.add(
                            new student_profile_contact_class(parent.getPrimaryContact(), parenttype + ": " + parent.getFirstName() + " " + parent.getSurname()));
                }


                view1.setVisibility(View.VISIBLE);
                view2.setVisibility(View.VISIBLE);
                myAdapter.notifyDataSetChanged();
                myAdapter1.notifyDataSetChanged();


               // Toast.makeText(context, parentChildren.get(0).getRelation(), Toast.LENGTH_SHORT).show();
            }


        }
    }
}
