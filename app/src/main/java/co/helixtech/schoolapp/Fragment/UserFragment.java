package co.helixtech.schoolapp.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import co.helixtech.schoolapp.Adapter.UserAdapter;
import co.helixtech.schoolapp.Model.User;
import co.helixtech.schoolapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import co.helixtech.schoolapp.Adapter.UserAdapter;
import co.helixtech.schoolapp.Model.User;
import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.utility.TokenSharedPreference;
import co.helixtech.schoolapp.utility.UserType;


public class UserFragment extends Fragment {

    private RecyclerView recyclerView;

    private UserAdapter userAdapter;
    private List<User> mUsers;

    EditText search_user;

    UserType usertype;
    TokenSharedPreference preference;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =inflater.inflate(R.layout.fragment_user,container,false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Select Contact");


        recyclerView=view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        preference = new TokenSharedPreference(getContext());

         usertype = preference.getCurrentAccessToken().getUserType();

        mUsers=new ArrayList<>();

        readUsers();

        search_user=view.findViewById(R.id.search);
        search_user.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                searchUser(charSequence.toString().toLowerCase());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return view;
    }

    private void searchUser(String s) {

        //final FirebaseUser firebaseUser=FirebaseAuth.getInstance().getCurrentUser();
        Query query=FirebaseDatabase.getInstance().getReference("Users").orderByChild("username")
                .startAt(s)
                .endAt(s+"\uf8ff");

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                    mUsers.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        User user = snapshot.getValue(User.class);

                        assert user != null;
                        //assert firebaseUser != null;
                        if(getContext()==null){
                            return;
                        }else {
                            TokenSharedPreference preference = new TokenSharedPreference(getContext());
                            int id = preference.getCurrentAccessToken().getUserID();
                            if (UserType.TEACHER==usertype && user.getId() != id && user.isParent()==true) {

                                mUsers.add(user);

                            }

                            if (UserType.PARENT==usertype && user.getId() != id && user.isTeacher()==true) {

                                mUsers.add(user);

                            }
                        }
                    }

                    userAdapter = new UserAdapter(getContext(), mUsers, false);
                    recyclerView.setAdapter(userAdapter);


            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void readUsers() {

        //final FirebaseUser firebaseUser=FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference=FirebaseDatabase.getInstance().getReference("Users");




        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (search_user.getText().toString().equals("")) {
                    mUsers.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        User user = snapshot.getValue(User.class);

                        assert user != null;
                       // assert firebaseUser != null;
                        if(getContext()==null){
                            return;
                        }else {
                            TokenSharedPreference preference = new TokenSharedPreference(getContext());
                            int id = preference.getCurrentAccessToken().getUserID();


                            if (UserType.TEACHER==usertype && user.getId() != id && user.isParent()==true) {

                                mUsers.add(user);

                            }

                            if (UserType.PARENT==usertype && user.getId() != id && user.isTeacher()==true) {

                                mUsers.add(user);

                            }
                        }

                    }

                    userAdapter = new UserAdapter(getContext(), mUsers, false);
                    recyclerView.setAdapter(userAdapter);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
