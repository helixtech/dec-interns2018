package co.helixtech.schoolapp.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.helixtech.schoolapp.Adapter.UserAdapter;
import co.helixtech.schoolapp.Model.Chatlist;
import co.helixtech.schoolapp.Model.User;
import co.helixtech.schoolapp.R;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.helixtech.schoolapp.Adapter.UserAdapter;
import co.helixtech.schoolapp.Model.Chatlist;
import co.helixtech.schoolapp.Model.User;
import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.utility.TokenSharedPreference;
import co.helixtech.schoolapp.utility.UserType;


public class ChatFragment extends Fragment {


    private RecyclerView recyclerView;
    private UserAdapter userAdapter;
    private List<User> mUsers;
    FloatingActionButton addUser;



    //FirebaseUser firebaseUser;
    DatabaseReference reference;

    private List<Chatlist> userList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view=inflater.inflate(R.layout.fragment_fragment_chat,container,false);

       recyclerView=view.findViewById(R.id.recycler_view);
       recyclerView.setHasFixedSize(true);
       recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


       //FirebaseDatabase.getInstance().setPersistenceEnabled(true);


        //firebaseUser=FirebaseAuth.getInstance().getCurrentUser();

        TokenSharedPreference preference = new TokenSharedPreference(getContext());
        int id = preference.getCurrentAccessToken().getUserID();
       userList=new ArrayList<>();

       reference=FirebaseDatabase.getInstance().getReference("Chatlist").child(id+"");
       reference.addValueEventListener(new ValueEventListener() {
           @Override
           public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

               userList.clear();
               for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                   Chatlist chatlist=snapshot.getValue(Chatlist.class);
                   userList.add(chatlist);
               }
               chatList();

           }

           @Override
           public void onCancelled(@NonNull DatabaseError databaseError) {

           }
       });

       return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Chat");

        addUser=view.findViewById(R.id.add_user);

        addUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                TokenSharedPreference preference = new TokenSharedPreference(getContext());

                UserType user = preference.getCurrentAccessToken().getUserType();



                if(UserType.TEACHER==user)
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.teacher_frame_id, new UserFragment()).addToBackStack("tag").commit();
                else if (UserType.PARENT==user)
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.relLayoutMiddle, new UserFragment(),"002").addToBackStack("tag").commit();



            }
        });
    }

    private void chatList() {
        mUsers=new ArrayList<>();
        reference=FirebaseDatabase.getInstance().getReference("Users");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mUsers.clear();
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    User user = snapshot.getValue(User.class);
                    for (Chatlist chatlist : userList){
                        if(user.getId()==chatlist.getId() ){
                            mUsers.add(user);
                        }
                    }
                }
                userAdapter=new UserAdapter(getContext(),mUsers,true);
                recyclerView.setAdapter(userAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void status(String status)
    {
        TokenSharedPreference preference = new TokenSharedPreference(getContext());
        int id = preference.getCurrentAccessToken().getUserID();
        reference=FirebaseDatabase.getInstance().getReference("Users").child(id+"");

        HashMap<String,Object> hashMap=new HashMap<>();
        hashMap.put("status",status);

        reference.updateChildren(hashMap);
    }

    @Override
    public void onResume() {
        super.onResume();
        status("online");
    }

    @Override
    public void onPause() {
        super.onPause();
        status("offline");
    }


}
