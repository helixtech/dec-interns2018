package co.helixtech.schoolapp.common;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;

import java.net.HttpURLConnection;
import java.util.HashMap;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.backend_calls.BackendService;
import co.helixtech.schoolapp.backend_calls.LoginApi;
import co.helixtech.schoolapp.db.pojo.Authentication;
import co.helixtech.schoolapp.parent.children;
import co.helixtech.schoolapp.teacher.select_class_activity;
import co.helixtech.schoolapp.utility.JsonWebToken;
import co.helixtech.schoolapp.utility.UserType;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import fr.castorflex.android.smoothprogressbar.SmoothProgressDrawable;
import me.itangqi.waveloadingview.WaveLoadingView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.POST;

public class Login extends AppCompatActivity implements Callback<Authentication>,
        View.OnClickListener {

    Button login_butn;
    TextInputEditText uId_box, password_box;
    TextInputLayout uId_layout, password_layout;
    private LoginApi loginApi = null;
    //ProgressDialog dialog;
    SmoothProgressBar mProgressBar;
    ImageView backimage, loginlogo;
    Animation translate;
    LinearLayout loginlayout;

    private long backPressedTime;
    private Toast backtoast;


    @Override
    public void onBackPressed() {

        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            backtoast.cancel();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            backtoast = Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT);
            backtoast.show();
        }
        backPressedTime = System.currentTimeMillis();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getSupportActionBar().hide();
        checkLoginState();
        setContentView(R.layout.activity_login);


        loginApi = (new BackendService()).getLoginApi();
        login_butn = (Button) findViewById(R.id.login_button);
        uId_box = (TextInputEditText) findViewById(R.id.userId);
        password_box = (TextInputEditText) findViewById(R.id.password);
        uId_layout = (TextInputLayout) findViewById(R.id.UserNameInputLayout);
        password_layout = (TextInputLayout) findViewById(R.id.PasswordInputLayout);
        login_butn.setOnClickListener(this);

        backimage = (ImageView) findViewById(R.id.backimage);
        loginlogo = (ImageView) findViewById(R.id.login_logo);

        mProgressBar = (SmoothProgressBar) findViewById(R.id.loading_bar);

        translate = AnimationUtils.loadAnimation(this, R.anim.login_translate);
        loginlayout = (LinearLayout) findViewById(R.id.loginlayout);

        backimage.animate().translationY(-2050).setDuration(900).setStartDelay(400);
        loginlogo.animate().translationY(-200).setDuration(700).setStartDelay(600);
        loginlayout.startAnimation(translate);
    }

    private void checkLoginState() {
        SharedPreferences preferences = getSharedPreferences
                (getString(R.string.auth_token_preferences), MODE_PRIVATE);
        String accessToken = preferences.getString(getString(R.string.access_token), null);
        if (accessToken == null)
            return;

        JsonWebToken token = new JsonWebToken(accessToken);
        UserType type = token.getUserType();
        if (type == UserType.PARENT)
            openParentActivity();
        else if (type == UserType.TEACHER)
            openTeacherActivity();
    }

    private void openTeacherActivity() {
        Intent intent = new Intent();
        intent.setClass(this, select_class_activity.class);
        startActivity(intent);
        finish();
    }

    private void openParentActivity() {
        Intent intent = new Intent();
        intent.setClass(this, children.class);
        startActivity(intent);
        finish();
    }

    public boolean validate() {
        if (!userid() | !password()) {
            return false;
        } else {
            return true;
        }
    }

    public boolean userid() {
        String User = uId_box.getText().toString().trim();
        if (User.isEmpty()) {
            uId_layout.setErrorEnabled(true);
            uId_layout.setError("Please Enter User Id");
            return false;
        } else {
            uId_layout.setErrorEnabled(false);
            return true;
        }
    }

    public boolean password() {
        String Password = password_box.getText().toString().trim();
        if (Password.isEmpty()) {
            password_layout.setErrorEnabled(true);
            password_layout.setError("Please Enter Password");
            return false;
        } else {
            password_layout.setErrorEnabled(false);
            return true;
        }
    }

    @Override
    public void onClick(View v) {
        if (validate()) {
            login_butn.setEnabled(false);
            password_layout.setErrorEnabled(false);
            mProgressBar.setVisibility(View.VISIBLE);
            HashMap<String, Object> body = new HashMap<>();
            body.put("username", uId_box.getText().toString());
            body.put("password", password_box.getText().toString());

            loginApi.getToken(body).enqueue(this);
        }
    }

    @Override
    public void onResponse(Call<Authentication> call, Response<Authentication> response) {
        switch (response.code()) {
            case HttpURLConnection.HTTP_OK:
                Authentication auth = response.body();
                Log.d(Login.class.getSimpleName() + ("Login Successful"),
                        auth.toString());
                JsonWebToken jsonWebToken = new JsonWebToken(auth.getAccess());

                //Save tokens
                SharedPreferences preferences =
                        getSharedPreferences(getString(R.string.auth_token_preferences),
                                MODE_PRIVATE);
                preferences.edit().putString(getString(R.string.access_token), auth.getAccess())
                        .putString(getString(R.string.refresh_token), auth.getRefresh())
                        .putBoolean("FIRST_TIME",false)
                        .apply();

                mProgressBar.setVisibility(View.INVISIBLE);
                UserType userType = jsonWebToken.getUserType();
                if (userType == UserType.PARENT)
                    openParentActivity();
                else if (userType == UserType.TEACHER)
                    openTeacherActivity();
                break;

            case HttpURLConnection.HTTP_BAD_REQUEST:
                Log.d(Login.class.getName() + "(Bad request)",
                        response.raw().toString());
                mProgressBar.setVisibility(View.INVISIBLE);
                password_layout.setErrorEnabled(true);
                password_layout.setError("Invalid Username or Password");
                break;
            default:
                Log.d(Login.class.getName() ,"(Timeout)");
                mProgressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(this, "Couldn't Reach Server :(", Toast.LENGTH_SHORT).show();
                break;

        }
        login_butn.setEnabled(true);
    }

    @Override
    public void onFailure(Call<Authentication> call, Throwable t) {
        Log.d(Login.class.getName() + "(onFailure)", t.getMessage(), t);mProgressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, "Couldn't Reach Server :(", Toast.LENGTH_SHORT).show();
        login_butn.setEnabled(true);

    }
}
