package co.helixtech.schoolapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import co.helixtech.schoolapp.MessageActivity;
import co.helixtech.schoolapp.Model.Chat;
import co.helixtech.schoolapp.Model.User;
import co.helixtech.schoolapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import co.helixtech.schoolapp.Model.User;
import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.utility.TokenSharedPreference;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private Context mContext;
    private List<User> mUsers;
    private boolean ischat;

    String theLastMessage;
    String lastMessageTime;
    public UserAdapter(Context mContext, List<User> mUsers, boolean ischat)
    {
        this.mUsers=mUsers;
        this.mContext=mContext;
        this.ischat=ischat;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view =LayoutInflater.from(mContext).inflate(R.layout.user_item,viewGroup,false);

        return new UserAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        final User user=mUsers.get(i);
        viewHolder.username.setText(user.getUsername());
        if(user.getImageURL().equals("default")){
            viewHolder.profilepic.setImageResource(R.drawable.profile_pic);

        }else
        {
            Glide.with(mContext).load(user.getImageURL()).into(viewHolder.profilepic);
        }

        if(ischat)
        {
            lastMessage(user.getId(),viewHolder.last_msg,viewHolder.timestamp);
        }else {
            viewHolder.last_msg.setVisibility(View.GONE);
        }

        if(ischat){
           if(user.getStatus().equals("online")){
               viewHolder.img_on.setVisibility(View.VISIBLE);
               viewHolder.img_off.setVisibility(View.GONE);
           }
           else
           {
               viewHolder.img_on.setVisibility(View.GONE);
               viewHolder.img_off.setVisibility(View.VISIBLE);
           }
        }
        else
        {
            viewHolder.img_on.setVisibility(View.GONE);
            viewHolder.img_off.setVisibility(View.GONE);
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(mContext,MessageActivity.class);
                intent.putExtra("userId",user.getId());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView username;
        public ImageView profilepic;
        private ImageView img_on;
        private ImageView img_off;
        private TextView last_msg;
        private TextView timestamp;


       public ViewHolder(@NonNull View itemView) {
           super(itemView);

           username=itemView.findViewById(R.id.username);
           profilepic=itemView.findViewById(R.id.profilepic);
           img_on=itemView.findViewById(R.id.img_on);
           img_off=itemView.findViewById(R.id.img_off);
           last_msg=itemView.findViewById(R.id.last_msg);
           timestamp=itemView.findViewById(R.id.timestamp);

       }
   }

   private void lastMessage(final int userid, final TextView last_msg, final TextView timestamp){
        theLastMessage="default";
        lastMessageTime="00:01";
      // final FirebaseUser firebaseUser=FirebaseAuth.getInstance().getCurrentUser();

       TokenSharedPreference preference = new TokenSharedPreference(mContext);
       final int id = preference.getCurrentAccessToken().getUserID();
       DatabaseReference reference=FirebaseDatabase.getInstance().getReference("Chats");

       reference.addValueEventListener(new ValueEventListener() {
           @Override
           public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

               for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                   Chat chat=snapshot.getValue(Chat.class);
                   if((chat.getReceiver()==id && chat.getSender()==userid) || (chat.getReceiver()==userid && chat.getSender()==id)){

                       theLastMessage=chat.getMessage();
                       lastMessageTime=chat.getTimestamp();
                   }
               }
               switch ((theLastMessage))
               {
                   case "default":
                       last_msg.setText("No Message");

                       break;
                   default:
                       last_msg.setText(theLastMessage);
                       break;
               }
               timestamp.setText(lastMessageTime);

               theLastMessage="default";



           }

           @Override
           public void onCancelled(@NonNull DatabaseError databaseError) {

           }
       });
   }
}
