package co.helixtech.schoolapp.backend_calls;

import java.util.HashMap;

import co.helixtech.schoolapp.db.pojo.Authentication;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface LoginApi {

    @POST("token/")
    Call<Authentication> getToken(@Body HashMap<String,Object> body);
    @POST("token/refresh")
    Call<Authentication> refreshToken();
    @POST("token/verify")
    Call<Authentication> verifyToken();
}
