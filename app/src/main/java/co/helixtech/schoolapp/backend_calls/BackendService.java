package co.helixtech.schoolapp.backend_calls;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import co.helixtech.schoolapp.common.Login;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BackendService {
    private final static String BASE_URL = "https://school-app-helix.herokuapp.com/api/";
    private static BackendService mService = null;
    private Retrofit mRetrofit = null;
    public BackendService(){
        OkHttpClient client = new OkHttpClient.Builder()
                .callTimeout(60,TimeUnit.SECONDS)
                .connectTimeout(60,TimeUnit.SECONDS)
                .readTimeout(60,TimeUnit.SECONDS)
                .writeTimeout(60,TimeUnit.SECONDS)
                .build();
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = builder.create();
        mRetrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }

    public LoginApi getLoginApi()
    {
       return mRetrofit.create(LoginApi.class);
    }

    public DataApi getDataApi(){
        return mRetrofit.create(DataApi.class);
    }

    public ImageApi getImageApi(){
        return mRetrofit.create(ImageApi.class);
    }
}
