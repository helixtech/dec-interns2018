package co.helixtech.schoolapp.backend_calls;

import co.helixtech.schoolapp.db.pojo.Assignment;
import co.helixtech.schoolapp.db.pojo.ClassActivity;
import co.helixtech.schoolapp.db.pojo.Notice;
import co.helixtech.schoolapp.db.pojo.ReportCard;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ImageApi{
    @Multipart
    @PUT("image/assignment/{id}/")
    Call<Assignment> postAssignmentImage(@Header("Authorization") String auth, @Path("id") int
                                         assignmentID, @Part MultipartBody.Part image);

    @Multipart
    @PUT("image/activity/{id}/")
    Call<ClassActivity> postActivityImage(@Header("Authorization") String auth, @Path("id") int
            assignmentID,  @Part MultipartBody.Part image);

    @Multipart
    @PUT("image/notice/{id}/")
    Call<Notice> postNoticeImage(@Header("Authorization") String auth, @Path("id") int
            assignmentID, @Part MultipartBody.Part image);

    @Multipart
    @PUT("image/report_card/{id}/")
    Call<ReportCard> postReportCardImage(@Header("Authorization") String auth, @Path("id") int
            reportID, @Part MultipartBody.Part image);
}
