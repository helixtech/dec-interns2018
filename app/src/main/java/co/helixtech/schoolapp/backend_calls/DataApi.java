package co.helixtech.schoolapp.backend_calls;

import java.util.List;

import co.helixtech.schoolapp.db.pojo.Assignment;
import co.helixtech.schoolapp.db.pojo.Attendance;
import co.helixtech.schoolapp.db.pojo.ClassActivity;
import co.helixtech.schoolapp.db.pojo.Feedback;
import co.helixtech.schoolapp.db.pojo.Notice;
import co.helixtech.schoolapp.db.pojo.ParentChild;
import co.helixtech.schoolapp.db.pojo.PostActivity;
import co.helixtech.schoolapp.db.pojo.PostAssignment;
import co.helixtech.schoolapp.db.pojo.PostAttendance;
import co.helixtech.schoolapp.db.pojo.PostFeedback;
import co.helixtech.schoolapp.db.pojo.PostNotice;
import co.helixtech.schoolapp.db.pojo.PostReportCard;
import co.helixtech.schoolapp.db.pojo.ReportCard;
import co.helixtech.schoolapp.db.pojo.Student;
import co.helixtech.schoolapp.db.pojo.TeacherClass;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HEAD;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface DataApi {
    @GET("classes/{id}")
    Call<List<TeacherClass>> getClassesOfTeacher(@Header("Authorization") String auth
            , @Path("id") int teacherID);

    @GET("students/{id}/")
    Call<List<Student>> getStudentsOfClass(@Header("Authorization") String auth,
                                           @Path("id") int classID);


    @POST("post_assignment/")
    Call<Assignment> postAssignment(@Header("Authorization") String auth, @Body
            PostAssignment assignment);

    @POST("post_notice/")
    Call<Notice> postNotice(@Header("Authorization") String auth, @Body PostNotice notice);


    @POST("post_activity/")
    Call<ClassActivity> postActivity(@Header("Authorization") String auth,
                                     @Body PostActivity activity);


    @GET("get_all_assignments_of_class/{classID}/")
    Call<List<Assignment>> getAssignmentsOfClass(@Header("Authorization") String auth,
                                                 @Path("classID") int classID);

    @GET("get_all_notices_of_class/{classID}")
    Call<List<Notice>> getNoticesOfClass(@Header("Authorization") String auth,
                                         @Path("classID") int classID);

    @GET("get_all_activities_of_class/{classID}")
    Call<List<ClassActivity>> getClassActivities(@Header("Authorization") String auth,
                                                 @Path("classID") int classID);

    @GET("get_parent_details/{studentID}")
    Call<List<ParentChild>> getParentbyStudentID(@Header("Authorization") String auth,
                                                 @Path("studentID") int studentID);

    @PUT("assignment/{id}/")
    Call<ResponseBody> editAssignmentByID(@Header("Authorization") String auth,
                                          @Path("id") int id, @Body PostAssignment assignment);

    @PUT("notice/{id}/")
    Call<ResponseBody> editNoticeByID(@Header("Authorization") String auth,
                                          @Path("id") int id, @Body PostNotice notice);

    @PUT("activity/{id}/")
    Call<ResponseBody> editActivityByID(@Header("Authorization") String auth,
                                          @Path("id") int id, @Body PostActivity assignment);

    @DELETE("assignment/{id}/")
    Call<ResponseBody> deleteAssignmentByID(@Header("Authorization") String auth,
                                          @Path("id") int id);

    @DELETE("notice/{id}/")
    Call<ResponseBody> deleteNoticeByID(@Header("Authorization") String auth,
                                      @Path("id") int id);

    @DELETE("activity/{id}/")
    Call<ResponseBody> deleteActivityByID(@Header("Authorization") String auth,
                                        @Path("id") int id);

    @POST("report_card/")
    Call<ReportCard> postReportCard(@Header("Authorization") String auth,
                                    @Body PostReportCard reportCard);

    @POST("post_feedback/")
    Call<Feedback> postFeedback(@Header("Authorization") String auth,
                                @Body PostFeedback feedback);

    @POST("post_attendance/")
    Call<Attendance> postAttendance(@Header("Authorization") String auth,
                                    @Body PostAttendance attendance);
}
