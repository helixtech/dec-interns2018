package co.helixtech.schoolapp.parent;

import android.util.Log;

import java.util.Date;

public class Assignment {
    String Image;
    String Title;
    String Description;
    int Class_id;
    String deadline;
    String date;
    String teacher_name;

    public Assignment(String title, String description, int class_id, String Deadline, String Date, String image, String teacher_name) {
        Title = title;
        Description = description;
        Class_id = class_id;
        deadline = Deadline;
        date = Date;
        Image = image;
        this.teacher_name = teacher_name;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getClass_id() {
        return Class_id;
    }

    public void setClass_id(int class_id) {
        Class_id = class_id;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public String getDeadline() {
        DateConverter2 dateConverter2 = new DateConverter2(deadline);
        return dateConverter2.convert();
    }

    public void setDeadline(String deadlineX) {
        deadline = deadlineX;
    }

    public String getDate() {
        DateConverter dateConverter = new DateConverter(date);
        return dateConverter.convert();
    }

    public void setDate(String dateX) {
        date = dateX;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
}
