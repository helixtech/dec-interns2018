package co.helixtech.schoolapp.parent;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.Date;

//Class Declaration Of Child
public class Child implements Parcelable {
    public static final Creator<Child> CREATOR = new Creator<Child>() {
        @Override
        public Child createFromParcel(Parcel in) {
            return new Child(in);
        }

        @Override
        public Child[] newArray(int size) {
            return new Child[size];
        }
    };
    String id;
    String enrollment_no;
    String roll_no;
    String firstName;
    String middleName;
    String surName;
    String dob;
    String gender;
    String bloodGroup;
    String height;
    String weight;
    String address_line_1;
    String address_line_2;
    String city;
    String district;
    String state;
    String country;
    String pin_code;
    String image_url;
    String class_id;
    String class_name;

    public Child(String id, String enrollment_no, String roll_no, String firstName, String middleName, String surName, String dob, String gender, String bloodGroup, String height, String weight, String address_line_1, String address_line_2, String city, String district, String state, String country, String pin_code, String image_url, String class_id, String class_name) {
        this.id = id;
        this.enrollment_no = enrollment_no;
        this.roll_no = roll_no;
        this.firstName = firstName;
        this.middleName = middleName;
        this.surName = surName;
        this.dob = dob;
        this.gender = gender;
        this.bloodGroup = bloodGroup;
        this.address_line_1 = address_line_1;
        this.address_line_2 = address_line_2;
        this.city = city;
        this.district = district;
        this.state = state;
        this.country = country;
        this.pin_code = pin_code;
        this.image_url = image_url;
        this.class_id = class_id;
        this.class_name = class_name;
        this.height = height;
        this.weight = weight;
        Log.d("Constructor", id + firstName);
    }

    protected Child(Parcel in) {
        id = in.readString();
        enrollment_no = in.readString();
        roll_no = in.readString();
        firstName = in.readString();
        middleName = in.readString();
        surName = in.readString();
        dob = in.readString();
        gender = in.readString();
        bloodGroup = in.readString();
        height = in.readString();
        weight = in.readString();
        address_line_1 = in.readString();
        address_line_2 = in.readString();
        city = in.readString();
        district = in.readString();
        state = in.readString();
        country = in.readString();
        pin_code = in.readString();
        image_url = in.readString();
        class_id = in.readString();
        class_name = in.readString();
    }

    public String getEnrollment_no() {
        return enrollment_no;
    }

    public void setEnrollment_no(String enrollment_no) {
        this.enrollment_no = enrollment_no;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoll_no() {
        return roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getAddress_line_1() {
        return address_line_1;
    }

    public void setAddress_line_1(String address_line_1) {
        this.address_line_1 = address_line_1;
    }

    public String getAddress_line_2() {
        return address_line_2;
    }

    public void setAddress_line_2(String address_line_2) {
        this.address_line_2 = address_line_2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPin_code() {
        return pin_code;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public void setPin_code(String pin_code) {
        this.pin_code = pin_code;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(enrollment_no);
        dest.writeString(roll_no);
        dest.writeString(firstName);
        dest.writeString(middleName);
        dest.writeString(surName);
        dest.writeString(dob);
        dest.writeString(gender);
        dest.writeString(bloodGroup);
        dest.writeString(height);
        dest.writeString(weight);
        dest.writeString(address_line_1);
        dest.writeString(address_line_2);
        dest.writeString(city);
        dest.writeString(district);
        dest.writeString(state);
        dest.writeString(country);
        dest.writeString(pin_code);
        dest.writeString(image_url);
        dest.writeString(class_id);
        dest.writeString(class_name);
    }
}
