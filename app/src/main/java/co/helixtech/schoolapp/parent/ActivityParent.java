package co.helixtech.schoolapp.parent;

//Class for Activities in Parent Side
public class ActivityParent {
    int id;
    String title;
    String description;
    String image_url;
    String date;
    String postName;

    public ActivityParent(int id, String title, String description, String image_url, String date, String postName) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.image_url = image_url;
        this.date = date;
        this.postName = postName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getDate() {
        DateConverter dateConverter = new DateConverter(date);
        return dateConverter.convert();
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }
}

