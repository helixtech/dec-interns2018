package co.helixtech.schoolapp.parent;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;

import java.util.List;

import co.helixtech.schoolapp.R;

//Adapter to display Notices in Recycler View
public class NoticeChildAdapter extends RecyclerView.Adapter<NoticeChildAdapter.MyViewHolder> {
    private Context mcontext;
    private List<Notice> notices;

    public NoticeChildAdapter(Context mcontext1, List<Notice> notices) {
        this.mcontext = mcontext1;
        this.notices = notices;
    }

    @NonNull
    @Override
    public NoticeChildAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parentX, int viewType) {
        View itemView = LayoutInflater.from(parentX.getContext()).inflate(R.layout.notice_card, parentX, false);
        return new NoticeChildAdapter.MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return notices.size();
    }

    @Override
    public void onBindViewHolder(@NonNull NoticeChildAdapter.MyViewHolder myViewHolder, final int position) {

        final Notice notice = notices.get(position);
        myViewHolder.title.setText(notice.getTitle());
        myViewHolder.description.setText(notice.getDescription());
        myViewHolder.date.setText(notice.getDate());
        final String image_url = notice.getImage();
        myViewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(view.getContext(), ImageViewer.class);
                intent.putExtra("image_url", image_url);
                view.getContext().startActivity(intent);
            }
        });


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView title, description, date;
        public CheckBox checkBox;
        public View view;

        public MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            view = itemView;

            title = (TextView) itemView.findViewById(R.id.titleNotice);
            description = (TextView) itemView.findViewById(R.id.description_notice);
            date = (TextView) itemView.findViewById(R.id.date_notice);

        }
    }
}
