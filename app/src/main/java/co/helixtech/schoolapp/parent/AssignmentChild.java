package co.helixtech.schoolapp.parent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.helixtech.schoolapp.R;
//redundant
//!!!! Use AssignmentChildFragment !!!!!

public class AssignmentChild extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private AssignmentChildAdapter assignmentChildAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_child);

        RequestQueue queue = Volley.newRequestQueue(this);

        final ArrayList<Assignment> assignments = new ArrayList<>();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://192.168.0.178:8000/api/get_all_assignments_of_class/3",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        Toast.makeText(AssignmentChild.this, response, Toast.LENGTH_SHORT).show();
                        String jsonStrX = response;
                        try {
                            JSONArray jsonArray = new JSONArray(jsonStrX);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                int id = jsonObject.getInt("id");
                                String title = jsonObject.getString("title");
                                String description = jsonObject.getString("description");
                                String date = jsonObject.getString("date");
                                String deadline = jsonObject.getString("deadline");
                            }
                        } catch (JSONException e) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getString("code").equals("token_not_valid")) {
                                    Toast.makeText(getApplicationContext(), "Token Invalid Or Expired \n Please Login Again", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error.Response", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "");
                return params;
            }
        };
        queue.add(stringRequest);
        mRecyclerView = findViewById(R.id.assignment_list);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        assignmentChildAdapter = new AssignmentChildAdapter(this, assignments);
        mRecyclerView.setAdapter(assignmentChildAdapter);
    }
}
