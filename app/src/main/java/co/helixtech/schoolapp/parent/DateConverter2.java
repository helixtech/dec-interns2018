package co.helixtech.schoolapp.parent;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

//Used to convert date to time ago format
//Takes date in yyyy-MM-dd'T'HH:mm:ss'Z' as input
public class DateConverter2 {
    String dateX;

    public DateConverter2(String date) {
        dateX = date;
    }

    public String getDate() {
        return dateX;
    }

    public void setDate(String date) {
        dateX = date;
    }

    public String convert() {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(dateX);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            Log.e("error", "parse error" + dateX);
            e.printStackTrace();
        }
        if (date == null) {
            return dateX;
        } else {
            DateFormat dateFormat = new SimpleDateFormat("EEEE,d MMMM yyyy HH:mm:ss");
            String strDate = dateFormat.format(date);
            return strDate;
        }
    }
}
