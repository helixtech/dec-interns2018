package co.helixtech.schoolapp.parent;

import android.util.Log;

//Notice Class
public class Notice {
    int id;
    String image;
    String title;
    String description;
    int teacher;
    String date;

    public Notice(int id, String image, String title, String description, int teacher, String Date) {
        this.image = image;
        this.id = id;
        this.title = title;
        this.description = description;
        this.teacher = teacher;
        date = Date;
        Log.d("Date", Date);
    }

    public String getImage() {
        Log.d("NoticeUrl", image);
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        DateConverter dateConverter = new DateConverter(date);
        return dateConverter.convert();
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTeacher() {
        return teacher;
    }

    public void setTeacher(int teacher) {
        this.teacher = teacher;
    }

}
