package co.helixtech.schoolapp.parent;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.utility.JsonWebToken;
import co.helixtech.schoolapp.utility.TokenSharedPreference;

public class AssignmentChildFragment extends Fragment {


    private RecyclerView mRecyclerView;
    private ProgressBar spinner;
    private AssignmentChildAdapter assignmentChildAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_assignment_child, container, false);
        RequestQueue queue = Volley.newRequestQueue(getContext());
        final ArrayList<Assignment> assignments = new ArrayList<>();
        //Progress Bar
        spinner = (ProgressBar) view.findViewById(R.id.progressBarAssignment);
        //Access token from shared preference
        TokenSharedPreference tokenSharedPreference = new TokenSharedPreference(getContext());
        final JsonWebToken jsonWebToken = tokenSharedPreference.getCurrentAccessToken();
        Log.d("token", jsonWebToken.getAuthorizationHeaderValue());
        //Get ID of current Child
        String childID = " ";
        if (getArguments() != null) {
            childID = getArguments().getString("childID");
            Log.d("ChildIdSuccess", childID);
        } else {
            Log.e("error", "error in getting child id");
        }
        //Request Server to give list of assignment for that student using volley library
        StringRequest stringRequest = new StringRequest(Request.Method.GET, getResources().getString(R.string.url) + getResources().getString(R.string.getAssignment) + childID,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        //Parsing the json Response (response)
                        String jsonStrX = response.toString();
                        try {
                            JSONArray jsonArray = new JSONArray(jsonStrX);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("assignment");
                                int id = jsonObject1.getInt("id");
                                String title = jsonObject1.getString("title");
                                String description = jsonObject1.getString("description");
                                String date = jsonObject1.getString("date");
                                String deadline = jsonObject1.getString("deadline");
                                String image_url = jsonObject1.getString("image");
                                JSONObject jsonObject2 = jsonObject1.getJSONObject("teacher_class");
                                JSONObject jsonObject3 = jsonObject2.getJSONObject("teacher");
                                String teacher_name = jsonObject3.getString("first_name") + " " + jsonObject3.getString("surname");
                                assignments.add(new Assignment(title, description, id, deadline, date, image_url, teacher_name));
                            }
                            //Displaying the assignment in list
                            mRecyclerView = (RecyclerView) view.findViewById(R.id.assignment_list);
                            LinearLayoutManager layoutManager
                                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            mRecyclerView.setLayoutManager(layoutManager);
                            mRecyclerView.setHasFixedSize(true);
                            assignmentChildAdapter = new AssignmentChildAdapter(getActivity(), assignments);
                            mRecyclerView.setAdapter(assignmentChildAdapter);
                            spinner.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            Log.e("JSON ERROR", "parsing error");
                            Toast.makeText(getContext(), "ERROR JOSN parsing", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                            spinner.setVisibility(View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error.Response", error.toString());
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_SHORT).show();
                spinner.setVisibility(View.GONE);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //Authorisation Token
                params.put("Authorization", jsonWebToken.getAuthorizationHeaderValue());
                return params;
            }
        };
        queue.add(stringRequest);
        return view;
    }
}