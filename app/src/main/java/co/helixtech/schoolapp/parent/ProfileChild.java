package co.helixtech.schoolapp.parent;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import co.helixtech.schoolapp.R;

//Redundant Do NOT USE THIS ACTIVITY ,THIS ACTIVITY IS IMPLEMENTED IN MAIN FEED
public class ProfileChild extends AppCompatActivity {

    TextView Name, Rollno, Class, DOB, Gender, BloodGrp, AddLine1, AddLine2, City, District, State, Country, Pincode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_child);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_profile);

        final Child child = (Child) getIntent().getParcelableExtra("child_data_x");
        Toast.makeText(ProfileChild.this, "recieved name" + child.getFirstName(), Toast.LENGTH_LONG).show();

      /*  Name = (TextView) findViewById(R.id.child_name);
        Rollno = (TextView) findViewById(R.id.child_roll_no);
        Class = (TextView) findViewById(R.id.child_class);
        DOB = (TextView) findViewById(R.id.child_dob);
        Gender = (TextView) findViewById(R.id.child_gender);
        BloodGrp = (TextView) findViewById(R.id.child_blood_group);
        AddLine1 = (TextView) findViewById(R.id.pAddressLine1);
        AddLine2 = (TextView) findViewById(R.id.pAddressLine2);
        City = (TextView) findViewById(R.id.pCity);
        District = (TextView) findViewById(R.id.pDistrict);
        State = (TextView) findViewById(R.id.pState);
        Country = (TextView) findViewById(R.id.pCountry);
        Pincode = (TextView) findViewById(R.id.pPincode);

        String name = child.getFirstName() + " " + child.getMiddleName() + " " + child.getSurName();

        Name.setText(name);
        Rollno.setText(child.getRoll_no());
        Class.setText(child.getClass_id());
        DOB.setText(child.getDob());
        Gender.setText(child.getGender());
        BloodGrp.setText(child.getBloodGroup());
        AddLine1.setText(child.getAddress_line_1());
        AddLine2.setText(child.getAddress_line_2());
        City.setText(child.getCity());
        District.setText(child.getDistrict());
        Country.setText(child.getCountry());
        Pincode.setText(child.getPin_code());*/


        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent update = new Intent(ProfileChild.this, UpdateChildProfile.class);
                startActivity(update);
            }
        });
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_profile, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //no inspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
