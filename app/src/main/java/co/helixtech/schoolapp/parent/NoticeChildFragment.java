package co.helixtech.schoolapp.parent;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.utility.JsonWebToken;
import co.helixtech.schoolapp.utility.TokenSharedPreference;

//Fragment used to display Notices
public class NoticeChildFragment extends Fragment {

    private static final String TAG = "NoticeChildFragment";
    private RecyclerView mRecyclerView;
    ProgressBar spinner2;
    private NoticeChildAdapter NoticechildAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        final View view = inflater.inflate(R.layout.activity_notice_child, container, false);
        spinner2 = view.findViewById(R.id.progressBarNotice);
        final ArrayList<Notice> notices = new ArrayList<>();
        RequestQueue queue = Volley.newRequestQueue(getContext());

        TokenSharedPreference tokenSharedPreference = new TokenSharedPreference(getContext());
        final JsonWebToken jsonWebToken = tokenSharedPreference.getCurrentAccessToken();
        Log.d("token", jsonWebToken.getAuthorizationHeaderValue());

        String childID = " ";
        if (getArguments() != null) {
            childID = getArguments().getString("childID");
            Log.d("ChildIdSuccess", childID);
        } else {
            Log.e("error", "error in getting child id");
        }
        StringRequest stringRequest = new StringRequest(Request.Method.GET, getResources().getString(R.string.url) + getResources().getString(R.string.getNotices) + childID,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        //Toast.makeText(getContext(), response, Toast.LENGTH_SHORT).show();
                        String jsonStrX = response;
                        try {
                            JSONArray jsonArray = new JSONArray(jsonStrX);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("notice");
                                int id = jsonObject1.getInt("id");
                                String image = jsonObject1.getString("image");
                                String title = jsonObject1.getString("title");
                                String description = jsonObject1.getString("description");
                                //int teacher = jsonObject1.getInt("teacher_class");
                                int teacher = 0;
                                String date = jsonObject1.getString("date");
                                notices.add(new Notice(id, image, title, description, teacher, date));
                            }
                            mRecyclerView = view.findViewById(R.id.notice_list);
                            LinearLayoutManager layoutManager
                                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            mRecyclerView.setLayoutManager(layoutManager);
                            mRecyclerView.setHasFixedSize(true);
                            NoticechildAdapter = new NoticeChildAdapter(getActivity(), notices);
                            mRecyclerView.setAdapter(NoticechildAdapter);
                            spinner2.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error.Response", error.toString());
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_SHORT).show();
                spinner2.setVisibility(View.GONE);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Authorization", getString(R.string.authorisation));
                params.put("Authorization", jsonWebToken.getAuthorizationHeaderValue());
                return params;
            }
        };
        queue.add(stringRequest);
        return view;
    }
}