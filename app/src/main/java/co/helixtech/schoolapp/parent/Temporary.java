package co.helixtech.schoolapp.parent;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import co.helixtech.schoolapp.R;

//Redundant
public class Temporary extends AppCompatActivity {

    TextView Name, Rollno, Class, DOB, Gender, BloodGrp;
    TextView AddLine1, AddLine2, City, District, State, Country, Pincode, child_class;
    TextView momnumber, dadnumber, momemail, dademail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temporary);
        //getSupportActionBar().hide();


        /*BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavViewBar);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);*/


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Toolbar toolbar1 = (Toolbar) findViewById(R.id.toolbar_layout);

        /*final Child child = (Child) getIntent().getParcelableExtra("child_data");
        Toast.makeText(Temporary.this, "recieved name" + child.getFirstName(), Toast.LENGTH_LONG).show();*/

        //setSupportActionBar(toolbar);
        //toolbar.setTitle(child.getFirstName()+" "+child.getSurName());
        //toolbar.setTitle("aaaaaaaaaa");

        ImageView profile_pic = (ImageView) findViewById(R.id.profile_pic);

        Glide.with(this)
                .load(R.drawable.dp)
                .into(profile_pic);


        /*Name = (TextView) findViewById(R.id.child_name);
        Rollno = (TextView) findViewById(R.id.child_roll_no);
        Class = (TextView) findViewById(R.id.child_class);
        DOB = (TextView) findViewById(R.id.child_dob);
        Gender = (TextView) findViewById(R.id.child_gender);
        BloodGrp = (TextView) findViewById(R.id.child_blood_group);*/
        child_class = (TextView) findViewById(R.id.child_class);
        AddLine1 = (TextView) findViewById(R.id.pAddressLine1);
        AddLine2 = (TextView) findViewById(R.id.pAddressLine2);
        City = (TextView) findViewById(R.id.pCity);
        District = (TextView) findViewById(R.id.pDistrict);
        State = (TextView) findViewById(R.id.pState);
        Country = (TextView) findViewById(R.id.pCountry);
        Pincode = (TextView) findViewById(R.id.pPincode);

        momnumber = (TextView) findViewById(R.id.tvNumber1);
        //dadnumber = (TextView) findViewById(R.id.tvNumber2);
        momemail = (TextView) findViewById(R.id.tvNumber3);
        //dademail = (TextView) findViewById(R.id.tvNumber4);

        /*String name = child.getFirstName() + " " + child.getMiddleName() + " " + child.getSurName();

        Name.setText(name);
        Rollno.setText(child.getRoll_no());
        Class.setText(child.getClass_id());
        DOB.setText(child.getDob());
        Gender.setText(child.getGender());
        BloodGrp.setText(child.getBloodGroup());*/

        /*child_class.setText(child.getClass_name());
        AddLine1.setText(child.getAddress_line_1());
        AddLine2.setText(child.getAddress_line_2());
        City.setText(child.getCity());
        District.setText(child.getDistrict());
        Country.setText(child.getCountry());
        Pincode.setText(child.getPin_code());*/


        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent update = new Intent(Temporary.this, UpdateChildProfile.class);
                startActivity(update);
            }
        });

        /*bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id._profile:
                        //Intent intent1 = new Intent(MainFeed.this,ProfileChild.class);
                        //startActivity(intent1);
                        break;
                    case R.id._dashboard:
                        Intent intent2 = new Intent(Temporary.this, DashboardParent.class);
                        //intent2.putExtra("child_data", child);
                        startActivity(intent2);
                        overridePendingTransition(0, 0);
                        break;
                    case R.id._activities:
                        Intent
                                intent3 = new Intent(Temporary.this, ActivitiesChild.class);
                        //intent3.putExtra("child_data", child);
                        startActivity(intent3);
                        overridePendingTransition(0, 0);
                        break;
                    case R.id._chat:
                        Intent intent4 = new Intent(Temporary.this,ChatParent.class);
                        //intent4.putExtra("child_data", child);
                        startActivity(intent4);
                        overridePendingTransition(0, 0);
                        break;
                }
                return false;
            }
        });*/

    }


    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //no inspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    /*@Override
    public void onBackPressed() {
        //your code when back button pressed
        Intent intentt = new Intent(Temporary.this,children.class);
        //intent4.putExtra("child_data", child);
        startActivity(intentt);
    }*/
}


