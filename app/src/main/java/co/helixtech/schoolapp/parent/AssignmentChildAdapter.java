package co.helixtech.schoolapp.parent;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import co.helixtech.schoolapp.R;

public class AssignmentChildAdapter extends RecyclerView.Adapter<AssignmentChildAdapter.MyViewHolder> {
    private Context mcontext;
    private List<Assignment> assignments;

    public AssignmentChildAdapter(Context mcontext1, List<Assignment> assignments) {
        this.mcontext = mcontext1;
        this.assignments = assignments;
    }

    @NonNull
    @Override
    public AssignmentChildAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parentX, int viewType) {
        View itemView = LayoutInflater.from(parentX.getContext()).inflate(R.layout.assignment_card, parentX, false);

        return new AssignmentChildAdapter.MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return assignments.size();
    }

    @Override
    public void onBindViewHolder(@NonNull AssignmentChildAdapter.MyViewHolder myViewHolder, final int position) {
        final Assignment assignment = assignments.get(position);
        myViewHolder.title.setText(assignment.getTitle());
        myViewHolder.description.setText(assignment.getDescription());
        myViewHolder.subject.setText("EVS");
        myViewHolder.date.setText(assignment.getDate());
        myViewHolder.teacher_name.setText(assignment.getTeacher_name());
        final String image_url = assignment.getImage();

        /*DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        String strDate = dateFormat.format(assignment.getDeadline());*/
        myViewHolder.deadline.setText("Deadline:" + assignment.getDeadline());
        myViewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(view.getContext(), ImageViewer.class);
                intent.putExtra("image_url", image_url);
                view.getContext().startActivity(intent);
            }
        });
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView title, description, date, subject, deadline, teacher_name;
        public CheckBox checkBox;
        public View view;

        public MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            view = itemView;
            title = (TextView) itemView.findViewById(R.id.title_assignment);
            description = (TextView) itemView.findViewById(R.id.description_assignment);
            subject = (TextView) itemView.findViewById(R.id.subject_assignment);
            deadline = (TextView) itemView.findViewById(R.id.deadline_assignment);
            date = (TextView) itemView.findViewById(R.id.date_assignment);
            teacher_name = (TextView) itemView.findViewById(R.id.teacher_assignment);
        }
    }

}
