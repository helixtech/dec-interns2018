package co.helixtech.schoolapp.parent;

//Class Declaration for Report
public class ReportParent {
    int id;
    String feedback;
    String date;
    String teacher_name;
    String subject;

    public ReportParent(int id, String feedback, String date, String teacher_name, String subject) {
        this.id = id;
        this.feedback = feedback;
        this.date = date;
        this.teacher_name = teacher_name;
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getDate() {
        DateConverter dateConverter = new DateConverter(date);
        return dateConverter.convert();
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }
}
