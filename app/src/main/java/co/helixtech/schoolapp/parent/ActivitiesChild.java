package co.helixtech.schoolapp.parent;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.utility.JsonWebToken;
import co.helixtech.schoolapp.utility.TokenSharedPreference;


public class ActivitiesChild extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private ActivityParentAdapter activityParentAdapter;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getParentActivityIntent() == null) {
                    onBackPressed();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activities_child);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Child child = getIntent().getParcelableExtra("child_data");

        final ArrayList<ActivityParent> activities = new ArrayList<>();

        final ProgressBar spinner;

        spinner = findViewById(R.id.progressBarActivities);

        RequestQueue queue = Volley.newRequestQueue(this);

        //Get Token From Shared Preference
        TokenSharedPreference tokenSharedPreference = new TokenSharedPreference(this);
        final JsonWebToken jsonWebToken = tokenSharedPreference.getCurrentAccessToken();
        Log.d("token", jsonWebToken.getAuthorizationHeaderValue());

        //Volley Request
        StringRequest stringRequest = new StringRequest(Request.Method.GET, getResources().getString(R.string.url) + getResources().getString(R.string.getActivitiesParent) + child.getId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        //Toast.makeText(ActivitiesChild.this, response, Toast.LENGTH_SHORT).show();
                        String jsonStrX = response;
                        try {
                            JSONArray jsonArray = new JSONArray(jsonStrX);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("activity");
                                int id = jsonObject1.getInt("id");
                                String title = jsonObject1.getString("title");
                                String description = jsonObject1.getString("description");
                                String date = jsonObject1.getString("date");
                                String image = jsonObject1.getString("image");
                                JSONObject jsonObject2 = jsonObject1.getJSONObject("teacher_class");
                                JSONObject jsonObject3 = jsonObject2.getJSONObject("teacher");
                                String name = jsonObject3.getString("first_name") + " " + jsonObject3.getString("surname");
                                activities.add(new ActivityParent(id, title, description, image, date, name));
                            }
                            mRecyclerView = findViewById(R.id.activities_list);

                            LinearLayoutManager layoutManager
                                    = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                            mRecyclerView.setLayoutManager(layoutManager);
                            mRecyclerView.setHasFixedSize(true);
                            activityParentAdapter = new ActivityParentAdapter(getApplicationContext(), activities);
                            mRecyclerView.setAdapter(activityParentAdapter);
                            spinner.setVisibility(View.GONE);

                        } catch (JSONException e) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getString("code").equals("token_not_valid")) {
                                    Toast.makeText(getApplicationContext(), "Token Invalid Or Expired \n Please Login Again", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                            spinner.setVisibility(View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error.Response", error.toString());
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                spinner.setVisibility(View.GONE);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", jsonWebToken.getAuthorizationHeaderValue());
                return params;
            }
        };
        queue.add(stringRequest);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavViewBar);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id._profile:
                        Intent intent1 = new Intent(ActivitiesChild.this, MainFeed.class);
                        intent1.putExtra("child_data", child);
                        finish();
                        startActivity(intent1);
                        overridePendingTransition(0, 0);
                        break;
                    case R.id._dashboard:
                        Intent intent2 = new Intent(ActivitiesChild.this, DashboardParent.class);
                        intent2.putExtra("child_data", child);
                        finish();
                        startActivity(intent2);
                        overridePendingTransition(0, 0);
                        break;
                    case R.id._activities:
                        //Intent intent3 = new Intent(ActivitiesChild.this, Notification.class);
                        //startActivity(intent3);
                        break;
                    case R.id._chat:
                        Intent intent4 = new Intent(ActivitiesChild.this, ChatParent.class);
                        intent4.putExtra("child_data", child);
                        finish();
                        startActivity(intent4);
                        overridePendingTransition(0, 0);
                        break;
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        //your code when back button pressed
        Intent intentt = new Intent(ActivitiesChild.this, children.class);
        //intent4.putExtra("child_data", child);
        finish();
        startActivity(intentt);
        overridePendingTransition(0, 0);
    }
}
