package co.helixtech.schoolapp.parent;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import co.helixtech.schoolapp.R;


public class DashboardParent extends AppCompatActivity {

    private static final String TAG = "DashboardParent";

    private SectionsPageAdapter mSectionsPageAdapter;


    String childID;
    private ViewPager mViewPager;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getParentActivityIntent() == null) {
                    onBackPressed();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_parent);
        Log.d(TAG, "onCreate: Starting.");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Child child = (Child) getIntent().getParcelableExtra("child_data");
        childID = child.getId();
        mSectionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager, childID);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        BottomNavigationView bottomNavigationView =(BottomNavigationView) findViewById(R.id.bottomNavViewBar);
        Menu menu= bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id._profile:
                        Intent intent1 = new Intent(DashboardParent.this,MainFeed.class);
                        intent1.putExtra("child_data", child);
                        finish();
                        startActivity(intent1);
                        overridePendingTransition(0, 0);
                        break;
                    case R.id._dashboard:
                        break;
                    case R.id._activities:
                        Intent intent3 = new Intent(DashboardParent.this,ActivitiesChild.class);
                        intent3.putExtra("child_data", child);
                        finish();
                        startActivity(intent3);
                        overridePendingTransition(0, 0);
                        break;
                    case R.id._chat:
                        Intent intent4 = new Intent(DashboardParent.this,ChatParent.class);
                        intent4.putExtra("child_data", child);
                        finish();
                        startActivity(intent4);
                        overridePendingTransition(0, 0);
                        break;
                }
                return false;
            }
        });
    }

    private void setupViewPager(ViewPager viewPager, String childID) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        Bundle bundle = new Bundle();
        bundle.putString("childID", childID);
        NoticeChildFragment noticeChildFragment = new NoticeChildFragment();
        noticeChildFragment.setArguments(bundle);
        AssignmentChildFragment assignmentChildFragment = new AssignmentChildFragment();
        assignmentChildFragment.setArguments(bundle);
        ReportFragment reportFragment = new ReportFragment();
        reportFragment.setArguments(bundle);
        adapter.addFragment(assignmentChildFragment, "ASSIGNMENT");
        adapter.addFragment(noticeChildFragment, "NOTICES");
        adapter.addFragment(reportFragment, "REPORT");
        viewPager.setAdapter(adapter);
    }
    @Override
    public void onBackPressed() {
        //your code when back button pressed
        Intent intentt = new Intent(DashboardParent.this,children.class);
        //intent4.putExtra("child_data", child);
        finish();
        startActivity(intentt);
        overridePendingTransition(0, 0);
    }
}
