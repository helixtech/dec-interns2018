package co.helixtech.schoolapp.parent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import co.helixtech.schoolapp.R;

//Redundant
//!!!!!!USE TAB2 Fragment !!!!!!!
public class NoticeChild extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private NoticeChildAdapter NoticechildAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_child);

        final ArrayList<Notice> notices = new ArrayList<>();
        RequestQueue queue = Volley.newRequestQueue(this);

        //comment
      /*  for (int i = 0; i < 10; i++) {
            notices.add(new Notice(4, "Payment Of school Fees", "You are kindly requested to pay the school fees for the Academic year 2018-2019. Last day of payment is 31st January", 3, "2018-12-17T11:15:51.473000Z"));
        }*/


        //uncomment
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://192.168.0.178:8000/api/get_all_notices",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        Toast.makeText(NoticeChild.this, response, Toast.LENGTH_SHORT).show();
                        String jsonStrX = response;
                        try {
                            JSONArray jsonArray = new JSONArray(jsonStrX);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                int id = jsonObject.getInt("id");
                                String title = jsonObject.getString("title");
                                String description = jsonObject.getString("description");
                                int class_id = jsonObject.getInt("class_id");
                                int teacher = jsonObject.getInt("teacher");
                                String date = jsonObject.getString("date");

                                /* notices.add(new Notice(id, title, description, teacher, date));*/
                            }
                        } catch (JSONException e) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getString("code").equals("token_not_valid")) {
                                    Toast.makeText(getApplicationContext(), "Token Invalid Or Expired \n Please Login Again", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error.Response", error.toString());

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "");
                return params;
            }
        };
        queue.add(stringRequest);


        mRecyclerView = findViewById(R.id.notice_list);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        NoticechildAdapter = new NoticeChildAdapter(this, notices);
        mRecyclerView.setAdapter(NoticechildAdapter);

    }
}
