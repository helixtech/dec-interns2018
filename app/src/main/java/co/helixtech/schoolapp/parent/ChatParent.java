package co.helixtech.schoolapp.parent;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import co.helixtech.schoolapp.Fragment.ChatFragment;
import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.teacher.teacher_main_fragment;


//Activity For Chat
public class ChatParent extends AppCompatActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getParentActivityIntent() == null) {
                    onBackPressed();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_parent);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        getSupportFragmentManager().beginTransaction().add(R.id.relLayoutMiddle, new ChatFragment(),"001").addToBackStack("tag").commit();



        final Child child = (Child) getIntent().getParcelableExtra("child_data");
        TextView title = (TextView) findViewById(R.id.ActivityTitleChat);
        title.setText("Chat");

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavViewBar);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(3);
        menuItem.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id._profile:
                        Intent intent1 = new Intent(ChatParent.this, MainFeed.class);
                        intent1.putExtra("child_data", child);
                        finish();
                        startActivity(intent1);
                        overridePendingTransition(0, 0);
                        break;
                    case R.id._dashboard:
                        Intent intent2 = new Intent(ChatParent.this, DashboardParent.class);
                        intent2.putExtra("child_data", child);
                        finish();
                        startActivity(intent2);
                        overridePendingTransition(0, 0);
                        break;
                    case R.id._activities:
                        Intent intent3 = new Intent(ChatParent.this, ActivitiesChild.class);
                        intent3.putExtra("child_data", child);
                        finish();
                        startActivity(intent3);
                        overridePendingTransition(0, 0);
                        break;
                    case R.id._chat:
                        //Intent intent4 = new Intent(MainFeed.this,Chat.class);
                        //startActivity(intent4);
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        Fragment currentFragment = fragmentManager.findFragmentById(R.id.relLayoutMiddle);

                        Log.d("fragment ",currentFragment.getTag());
                        break;
                }
                return false;
            }
        });


    }

    @Override
    public void onBackPressed() {

        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.relLayoutMiddle);
        if(currentFragment.getTag().equals("001")){
            Intent intent = new Intent(ChatParent.this, children.class);
            finish();
            startActivity(intent);
            overridePendingTransition(0, 0);
        }
        else {
            super.onBackPressed();
        }
        //your code when back button pressed

    }

}

