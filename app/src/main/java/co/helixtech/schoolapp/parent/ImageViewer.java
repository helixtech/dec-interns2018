package co.helixtech.schoolapp.parent;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import co.helixtech.schoolapp.R;

//Used to Display Image
public class ImageViewer extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewer);
        Bundle extras = getIntent().getExtras();
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        if (extras != null) {
            String image_url = extras.getString("image_url");
            Log.d("image_url", image_url);
            GlideApp.with(this)
                    .load(image_url)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            // log exception
                            progressBar.setVisibility(View.GONE);
                            Log.e("TAG", "Error loading image", e);
                            Toast.makeText(getApplicationContext(), "Error loading image", Toast.LENGTH_SHORT).show();
                            ;
                            return false; // important to return false so the error placeholder can be placed
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                    })
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);


            //The key argument here must match that used in the other activity
        } else {
            Log.e("image_url_error", "ERROR in image url in ImageViewer");
            Toast.makeText(getApplicationContext(), "ERROR in image url in ImageViewer", Toast.LENGTH_SHORT).show();
            ;
        }


    }
}
