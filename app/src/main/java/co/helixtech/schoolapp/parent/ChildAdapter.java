package co.helixtech.schoolapp.parent;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import co.helixtech.schoolapp.R;

public class ChildAdapter extends RecyclerView.Adapter<ChildAdapter.MyViewHolder> {

    private Context mcontext;
    private List<Child> children;

    public ChildAdapter(Context mcontext1, List<Child> children1) {
        this.mcontext = mcontext1;
        this.children = children1;
    }

    @NonNull
    @Override
    public ChildAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parentX, int viewType) {
        View itemView = LayoutInflater.from(parentX.getContext()).inflate(R.layout.child_card, parentX, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ChildAdapter.MyViewHolder myViewHolder, final int position) {
        final Child child = children.get(position);
        final String image_url = child.getImage_url();
        myViewHolder.name.setText(child.getFirstName());
        /*Glide.with(mcontext).load(image_url)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(myViewHolder.face);*/

        GlideApp.with(mcontext)
                .load(image_url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(myViewHolder.face);

        Log.d("View", child.getFirstName());
        Log.d("image_url", child.getImage_url());
        //myViewHolder.rollno.setText(child.getRollno());
        //myViewHolder.classX.setText(child.getClassX());
        //myViewHolder.division.setText(child.getDivision());
        myViewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(view.getContext(), MainFeed.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("child_data",child);
                mcontext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return children.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView name, rollno, classX, division;
        public ImageView face;
        public View view;

        public MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            view = itemView;

            name = (TextView) itemView.findViewById(R.id.name);
            face = (ImageView) itemView.findViewById(R.id.face);

            //rollno = (TextView) itemView.findViewById(R.id.rollNo);
            //classX = (TextView) itemView.findViewById(R.id.classX);
            //division = (TextView) itemView.findViewById(R.id.division);
            //final Employee employee = new Employee(code.getText().toString(),name.getText().toString(),email.getText().toString());
        }
    }

}

