package co.helixtech.schoolapp.parent;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import co.helixtech.schoolapp.R;

//Adapter To display Reports in Recycler View
public class ReportChildAdapter extends RecyclerView.Adapter<ReportChildAdapter.MyViewHolder> {
    private Context mcontext;
    private List<ReportParent> reports;

    public ReportChildAdapter(Context mcontext1, List<ReportParent> reports) {
        this.mcontext = mcontext1;
        this.reports = reports;
    }

    @NonNull
    @Override
    public ReportChildAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parentX, int viewType) {
        View itemView = LayoutInflater.from(parentX.getContext()).inflate(R.layout.report_card, parentX, false);
        return new ReportChildAdapter.MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return reports.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ReportChildAdapter.MyViewHolder myViewHolder, final int position) {

        final ReportParent report = reports.get(position);
        myViewHolder.teacher_name_report.setText(report.getTeacher_name());
        myViewHolder.feedback.setText(report.getFeedback());
        myViewHolder.date.setText(report.getDate());

        //To make card Clickable
        /*myViewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Code for what to do once particular card is clicked

            }
        });
*/

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView teacher_name_report, feedback, date;
        public View view;

        public MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            view = itemView;

            teacher_name_report = (TextView) itemView.findViewById(R.id.teacher_name_report);
            feedback = (TextView) itemView.findViewById(R.id.feedback_report);
            date = (TextView) itemView.findViewById(R.id.date_report);

        }
    }
}
