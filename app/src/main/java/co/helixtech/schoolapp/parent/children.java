package co.helixtech.schoolapp.parent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.common.Login;
import co.helixtech.schoolapp.utility.JsonWebToken;
import co.helixtech.schoolapp.utility.TokenSharedPreference;


public class children extends AppCompatActivity {

    private long backPressedTime;
    private Toast backtoast;

    @Override
    public void onBackPressed() {

        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            backtoast.cancel();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            backtoast = Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT);
            backtoast.show();
        }
        backPressedTime = System.currentTimeMillis();
    }

    private RecyclerView mRecyclerView;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.logout_btn_id) {
            Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show();
            TokenSharedPreference preference = new TokenSharedPreference(this);
            preference.clearCredentials();
            finish();
            Intent i = new Intent(children.this, Login.class);
            // set the new task and clear flags
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private ChildAdapter childAdapter;
    String class_name, id, enrollment_no, height, weight, image_url, roll_no, first_name, middle_name, surname, dob, gender, blood_group, address_line_1, address_line_2, city, district, state, country, pin_code, class_id;
    int id_int, pin_code_int, class_id_int;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_children);

        RequestQueue queue = Volley.newRequestQueue(this);

        final ArrayList<Child> children = new ArrayList<>();

        final ProgressBar spinner;
        spinner = (ProgressBar) findViewById(R.id.progressBar1);

        TokenSharedPreference tokenSharedPreference = new TokenSharedPreference(this);
        final JsonWebToken jsonWebToken = tokenSharedPreference.getCurrentAccessToken();
        Log.d("token", jsonWebToken.getAuthorizationHeaderValue());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://school-app-helix.herokuapp.com/api/get_children/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        //Toast.makeText(children.this, response, Toast.LENGTH_SHORT).show();
                        String jsonStrX = response.toString();
                        try {
                            JSONArray jsonArray = new JSONArray(jsonStrX);

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("child");
                                id_int = jsonObject1.getInt("id");
                                id = Integer.toString(id_int);
                                enrollment_no = jsonObject1.getString("enrollment_no");
                                roll_no = jsonObject1.getString("roll_no");
                                first_name = jsonObject1.getString("first_name");
                                middle_name = nullChecker(jsonObject1.getString("middle_name"));
                                surname = jsonObject1.getString("surname");
                                dob = jsonObject1.getString("dob");
                                gender = jsonObject1.getString("gender");
                                blood_group = jsonObject1.getString("blood_group");
                                height = nullChecker(jsonObject1.getString("height"));
                                weight = nullChecker(jsonObject1.getString("weight"));
                                address_line_1 = jsonObject1.getString("address_line_1");
                                address_line_2 = jsonObject1.getString("address_line_2");
                                city = nullChecker(jsonObject1.getString("city"));
                                district = nullChecker(jsonObject1.getString("district"));
                                state = nullChecker(jsonObject1.getString("state"));
                                country = nullChecker(jsonObject1.getString("country"));
                                pin_code_int = jsonObject1.getInt("pin_code");
                                pin_code = nullChecker(Integer.toString(pin_code_int));
                                image_url = jsonObject1.getString("image");
                                JSONObject jsonObject2 = jsonObject1.getJSONObject("class_id");
                                class_name = jsonObject2.getString("class_name");
                                class_id_int = jsonObject2.getInt("id");
                                class_id = Integer.toString(class_id_int);


                                children.add(new Child(id, enrollment_no, roll_no, first_name, middle_name, surname, dob, gender, blood_group, height, weight, address_line_1, address_line_2, city, district, state, country, pin_code, image_url, class_id, class_name));

                            }
                            mRecyclerView = (RecyclerView) findViewById(R.id.childlist);

                            LinearLayoutManager layoutManager
                                    = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                            mRecyclerView.setLayoutManager(layoutManager);
                            mRecyclerView.setHasFixedSize(true);
                            childAdapter = new ChildAdapter(getApplicationContext(), children);
                            mRecyclerView.setAdapter(childAdapter);
                            spinner.setVisibility(View.GONE);

                        } catch (JSONException e) {
                            Log.e("Error", "Json String Parcing Error");
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getString("code").equals("token_not_valid")) {
                                    Toast.makeText(getApplicationContext(), "Token Invalid Or Expired \n Please Login Again", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                            //Toast.makeText(getApplicationContext(), "Json String Parsing ERROR", Toast.LENGTH_SHORT).show();
                            spinner.setVisibility(View.GONE);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error.Response", error.toString());
                Toast.makeText(children.this, error.toString(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Authorization", getResources().getString(R.string.authorisation));
                params.put("Authorization", jsonWebToken.getAuthorizationHeaderValue());
                return params;
            }
        };
        queue.add(stringRequest);


    }

    public String nullChecker(String string) {
        if (string == "null") {
            return " ";
        } else {
            return string;
        }
    }

}
