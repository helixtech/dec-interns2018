package co.helixtech.schoolapp.parent;

import android.os.Parcel;
import android.os.Parcelable;

//Class declaration for Parent
public class Parent implements Parcelable {
    public static final Creator<Parent> CREATOR = new Creator<Parent>() {
        @Override
        public Parent createFromParcel(Parcel in) {
            return new Parent(in);
        }

        @Override
        public Parent[] newArray(int size) {
            return new Parent[size];
        }
    };
    int parentId;
    String name;
    String residential_address;
    String contact;
    String emergencyContact;
    String email_id;
    String occupation;
    String officeContact;

    public Parent(int parentId, String name, String residential_address, String contact, String emergencyContact, String email_id, String occupation, String officeContact) {
        this.parentId = parentId;
        this.name = name;
        this.residential_address = residential_address;
        this.contact = contact;
        this.emergencyContact = emergencyContact;
        this.email_id = email_id;
        this.occupation = occupation;
        this.officeContact = officeContact;
    }

    protected Parent(Parcel in) {
        parentId = in.readInt();
        name = in.readString();
        residential_address = in.readString();
        contact = in.readString();
        emergencyContact = in.readString();
        email_id = in.readString();
        occupation = in.readString();
        officeContact = in.readString();
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResidential_address() {
        return residential_address;
    }

    public void setResidential_address(String residential_address) {
        this.residential_address = residential_address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmergencyContact() {
        return emergencyContact;
    }

    public void setEmergencyContact(String emergencyContact) {
        this.emergencyContact = emergencyContact;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getOfficeContact() {
        return officeContact;
    }

    public void setOfficeContact(String officeContact) {
        this.officeContact = officeContact;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(parentId);
        dest.writeString(name);
        dest.writeString(residential_address);
        dest.writeString(contact);
        dest.writeString(emergencyContact);
        dest.writeString(email_id);
        dest.writeString(occupation);
        dest.writeString(officeContact);
    }
}


