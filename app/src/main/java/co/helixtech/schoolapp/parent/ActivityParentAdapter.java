package co.helixtech.schoolapp.parent;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import co.helixtech.schoolapp.R;

public class ActivityParentAdapter extends RecyclerView.Adapter<ActivityParentAdapter.MyViewHolder> {

    private Context mcontext;
    private List<ActivityParent> activities;

    public ActivityParentAdapter(Context mcontext, List<ActivityParent> activities) {
        this.mcontext = mcontext;
        this.activities = activities;
    }


    @NonNull
    @Override
    public ActivityParentAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activities_card, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {

        final ActivityParent activityParent = activities.get(position);

        final String image_url = activityParent.getImage_url();
        Log.d("Name", activityParent.getPostName());
        myViewHolder.name.setText(activityParent.getPostName());
        myViewHolder.date.setText(activityParent.getDate());
        myViewHolder.description.setText(activityParent.getDescription());
        myViewHolder.title.setText(activityParent.getTitle());
        /*Glide.with(mcontext).load(image_url)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(myViewHolder.face);*/
        GlideApp.with(mcontext)
                .load(image_url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(myViewHolder.imageView);
        //myViewHolder.rollno.setText(child.getRollno());
        //myViewHolder.classX.setText(child.getClassX());
        //myViewHolder.division.setText(child.getDivision());
        /*myViewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(view.getContext(), MainFeed.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("child_data",child);
                mcontext.startActivity(intent);
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return activities.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView title, description, date, name;
        public ImageView imageView;
        public View view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            date = (TextView) itemView.findViewById(R.id.date_activities);
            title = (TextView) itemView.findViewById(R.id.title_activities);
            description = (TextView) itemView.findViewById(R.id.description_activities);
            name = (TextView) itemView.findViewById(R.id.name_activities);
            imageView = (ImageView) itemView.findViewById(R.id.image_activities);

        }
    }
}
