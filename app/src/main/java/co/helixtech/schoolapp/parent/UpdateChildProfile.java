package co.helixtech.schoolapp.parent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.utility.JsonWebToken;
import co.helixtech.schoolapp.utility.TokenSharedPreference;

//Activity for Updating Child Profile
public class UpdateChildProfile extends AppCompatActivity {

    private TextView emailmom, emaildad, momcontact, dadcontact, emergencycontact;
    private TextView AddressLine1, AddressLine2, City, District, pincode;
    private Button submit;
    Child child;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getParentActivityIntent() == null) {
                    onBackPressed();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                ((ImageView) findViewById(R.id.displaypic)).setImageURI(result.getUri());

                Toast.makeText(
                        this, "Cropping successful, Sample: " + result.getSampleSize(), Toast.LENGTH_LONG)
                        .show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_child_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        child = (Child) getIntent().getParcelableExtra("child_data");

        RequestQueue queue = Volley.newRequestQueue(this);
        TokenSharedPreference tokenSharedPreference = new TokenSharedPreference(this);
        final JsonWebToken jsonWebToken = tokenSharedPreference.getCurrentAccessToken();
        final int id = jsonWebToken.getParentID();


        ImageView profile_pic = (ImageView) findViewById(R.id.displaypic);
        GlideApp.with(getApplicationContext())
                .load(child.getImage_url())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(profile_pic);


        /*upload = (Button) findViewById(R.id.upload);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity(null).setGuidelines(CropImageView.Guidelines.ON).setAspectRatio(2,1).start(UpdateChildProfile.this);

            }
        });*/


        emailmom = (TextView) findViewById(R.id.emailmom);
        emaildad = (TextView) findViewById(R.id.emaildad);
        momcontact = (TextView) findViewById(R.id.momcontact);
        dadcontact = (TextView) findViewById(R.id.dadcontact);
        emergencycontact = (TextView) findViewById(R.id.emergencynumber);
        AddressLine1 = (TextView) findViewById(R.id.AddressLine1);
        AddressLine2 = (TextView) findViewById(R.id.AddressLine2);
        City = (TextView) findViewById(R.id.City);
        District = (TextView) findViewById(R.id.District);
        pincode = (TextView) findViewById(R.id.pincode);

        AddressLine1.setText(child.getAddress_line_1());
        AddressLine2.setText(child.getAddress_line_2());
        City.setText(child.getCity());
        District.setText(child.getDistrict());
        pincode.setText(child.getPin_code());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://school-app-helix.herokuapp.com/api/get_parent_details/" + child.getId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        //Toast.makeText(getContext(), response, Toast.LENGTH_SHORT).show();
                        String jsonStrX = response.toString();
                        try {
                            //Log.d("Authorisation",jsonWebToken.getAuthorizationHeaderValue());
                            JSONArray jsonArray = new JSONArray(jsonStrX);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String relation = jsonObject.getString("relation");
                                if (relation.equals("M")) {
                                    momcontact.setVisibility(View.VISIBLE);
                                    emailmom.setVisibility(View.VISIBLE);
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("parent");
                                    emailmom.setText(jsonObject1.getString("email_id"));
                                    momcontact.setText(jsonObject1.getString("primary_contact"));
                                    if (jsonObject1.getInt("user_id") == id) {
                                        emergencycontact.setText(jsonObject1.getString("primary_contact"));
                                    }
                                } else if (relation.equals("F")) {
                                    dadcontact.setVisibility(View.VISIBLE);
                                    emaildad.setVisibility(View.VISIBLE);
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("parent");
                                    emaildad.setText(jsonObject1.getString("email_id"));
                                    dadcontact.setText(jsonObject1.getString("primary_contact"));
                                    if (jsonObject1.getInt("user_id") == id) {
                                        emergencycontact.setText(jsonObject1.getString("primary_contact"));
                                    }
                                }

                            }
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), "Json Parsing Error", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error.Response", error.toString());
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Authorization", getString(R.string.authorisation));
                params.put("Authorization", jsonWebToken.getAuthorizationHeaderValue());
                return params;
            }
        };
        queue.add(stringRequest);

        /*LinearLayout layout = (LinearLayout)findViewById(R.id.YOUD VIEW ID);
        ViewTreeObserver vto = layout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    this.layout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    this.layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                int width  = layout.getMeasuredWidth();
                int height = layout.getMeasuredHeight();

            }
        });*/


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity(null).setGuidelines(CropImageView.Guidelines.ON).setAspectRatio(1, 1).start(UpdateChildProfile.this);
            }
        });

        submit = (Button) findViewById(R.id.submitbutton1);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            /*String newfname = fname.getText().toString();
            String  newMname= Mname.getText().toString();
            String  newSname= Sname.getText().toString();
            String  newClassid= Classid.getText().toString();
            String  newDOB= DOB.getText().toString();
            String  newGender= Gender.getText().toString();
            String  newBloodgroup= Bloodgroup.getText().toString();
            String  newAddressLine1= AddressLine1.getText().toString();
            String  newAddressLine2= AddressLine2.getText().toString();
            String  newCity= City.getText().toString();
            String  newDistrict= District.getText().toString();
            String  newState= State.getText().toString();
            String  newCountry= Country.getText().toString();
            String  newpincode= pincode.getText().toString();*/

                UIUtil.hideKeyboard(UpdateChildProfile.this);
            }
        });

    }

    @Override
    public void onBackPressed() {
        //your code when back button pressed
        Intent intentt = new Intent(UpdateChildProfile.this, MainFeed.class);
        intentt.putExtra("child_data", child);
        finish();
        startActivity(intentt);
        overridePendingTransition(0, 0);
    }
}
