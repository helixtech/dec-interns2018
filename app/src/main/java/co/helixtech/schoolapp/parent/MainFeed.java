package co.helixtech.schoolapp.parent;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.utility.JsonWebToken;
import co.helixtech.schoolapp.utility.TokenSharedPreference;


public class MainFeed extends AppCompatActivity {


    String mother_name, father_name;
    TextView Name, Rollno, Class, DOB, Gender, BloodGrp, RegNo, Height, Weight;
    TextView AddLine1, AddLine2, City, District, State, Country, Pincode, child_class;
    TextView fatherContactno, motherContactno, EmergencyContactNo, emailname, email, FatherName, FatherOccupation, FatherOfficeContact, MotherName, MotherOccupation, MotherOfficeContactNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_feed);

        RequestQueue queue = Volley.newRequestQueue(this);
        TokenSharedPreference tokenSharedPreference = new TokenSharedPreference(this);
        final JsonWebToken jsonWebToken = tokenSharedPreference.getCurrentAccessToken();

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavViewBar);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.toolbar_layout);
        //Toolbar toolbar1 = (Toolbar) findViewById(R.id.toolbar_layout);

        final Child child = (Child) getIntent().getParcelableExtra("child_data");
        //Toast.makeText(MainFeed.this, "recieved name" + child.getFirstName(), Toast.LENGTH_LONG).show();

        setSupportActionBar(toolbar);
        toolbar.setTitle(child.getFirstName() + " " + child.getSurName());
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(R.color.titlecustom));
        Typeface font = Typer.set(MainFeed.this).getFont(Font.ROBOTO_MEDIUM);
        collapsingToolbarLayout.setExpandedTitleTypeface(font);

        ImageView profile_pic = (ImageView) findViewById(R.id.profile_pic);

        GlideApp.with(this)
                .load(child.getImage_url())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(profile_pic);

        Rollno = (TextView) findViewById(R.id.child_RollNo);
        child_class = (TextView) findViewById(R.id.child_class);
        BloodGrp = (TextView) findViewById(R.id.Child_BloodGrp);
        AddLine1 = (TextView) findViewById(R.id.pAddressLine1);
        AddLine2 = (TextView) findViewById(R.id.pAddressLine2);
        City = (TextView) findViewById(R.id.pCity);
        District = (TextView) findViewById(R.id.pDistrict);
        State = (TextView) findViewById(R.id.pState);
        Country = (TextView) findViewById(R.id.pCountry);
        Pincode = (TextView) findViewById(R.id.pPincode);
        RegNo = (TextView) findViewById(R.id.reg_no);
        Height = (TextView) findViewById(R.id.child_Height);
        Weight = (TextView) findViewById(R.id.child_Weight);
        email = (TextView) findViewById(R.id.tvNumber3);
        emailname = (TextView) findViewById(R.id.emailname);
        FatherName = (TextView) findViewById(R.id.father_name);
        MotherName = (TextView) findViewById(R.id.mother_name);
        FatherOccupation = (TextView) findViewById(R.id.father_Occupation);
        MotherOccupation = (TextView) findViewById(R.id.mother_Occupation);
        FatherOfficeContact = (TextView) findViewById(R.id.father_workNo);
        MotherOfficeContactNo = (TextView) findViewById(R.id.mother_workNo);
        final RelativeLayout MotherDetails = (RelativeLayout) findViewById(R.id.Mother);
        final RelativeLayout FatherDetails = (RelativeLayout) findViewById(R.id.fatherOccupationDetails);
        final RelativeLayout momContactLayout = (RelativeLayout) findViewById(R.id.momcontact_layout);
        final RelativeLayout dadContactLayout = (RelativeLayout) findViewById(R.id.dadcontact_layout);
        final View dividerbelowfather = (View) findViewById(R.id.belowfatheroccupation);
        final View dividerbelowmother = (View) findViewById(R.id.belowmotheroccupation);
        fatherContactno = (TextView) findViewById(R.id.tvNumber2);
        motherContactno = (TextView) findViewById(R.id.tvNumber1);
        EmergencyContactNo = (TextView) findViewById(R.id.tvNumber4);

        Rollno.setText(child.getRoll_no());
        RegNo.setText(child.getEnrollment_no());
        child_class.setText(child.getClass_name());
        BloodGrp.setText(child.getBloodGroup());
        AddLine1.setText(child.getAddress_line_1());
        AddLine2.setText(child.getAddress_line_2());
        City.setText(child.getCity());
        District.setText(child.getDistrict());
        Country.setText(child.getCountry());
        Pincode.setText(child.getPin_code());
        Height.setText(child.getHeight() + " cm");
        Weight.setText(child.getWeight() + " kg");

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://school-app-helix.herokuapp.com/api/get_parent_details/" + child.getId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        //Toast.makeText(getContext(), response, Toast.LENGTH_SHORT).show();
                        String jsonStrX = response.toString();
                        try {
                            //Log.d("Authorisation",jsonWebToken.getAuthorizationHeaderValue());
                            JSONArray jsonArray = new JSONArray(jsonStrX);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String relation = jsonObject.getString("relation");
                                if (relation.equals("M")) {
                                    MotherDetails.setVisibility(View.VISIBLE);
                                    dividerbelowmother.setVisibility(View.VISIBLE);
                                    momContactLayout.setVisibility(View.VISIBLE);
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("parent");
                                    mother_name = jsonObject1.getString("first_name") + " " + jsonObject1.getString("surname");
                                    MotherName.setText(mother_name);
                                    Log.d("Mother", mother_name);
                                    SetTextView2(MotherOfficeContactNo, jsonObject1.getString("office_no"));
                                    SetTextView2(MotherOccupation, "Occupation : " + jsonObject1.getString("occupation"));
                                    SetTextView2(email, jsonObject1.getString("email_id"));
                                    SetTextView2(emailname, mother_name);
                                    SetTextView2(motherContactno, jsonObject1.getString("primary_contact"));
                                    SetTextView2(EmergencyContactNo, jsonObject1.getString("primary_contact"));
                                } else if (relation.equals("F")) {
                                    FatherDetails.setVisibility(View.VISIBLE);
                                    dividerbelowfather.setVisibility(View.VISIBLE);
                                    dadContactLayout.setVisibility(View.VISIBLE);
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("parent");
                                    father_name = jsonObject1.getString("first_name") + " " + jsonObject1.getString("surname");
                                    FatherName.setText(father_name);
                                    Log.d("Father", father_name);
                                    SetTextView2(FatherOfficeContact, jsonObject1.getString("office_no"));
                                    SetTextView2(FatherOccupation, "Occupation : " + jsonObject1.getString("occupation"));
                                    SetTextView2(email, jsonObject1.getString("email_id"));
                                    SetTextView2(emailname, father_name);
                                    SetTextView2(fatherContactno, jsonObject1.getString("primary_contact"));
                                    Log.d("relation", "relation is not mother" + relation);
                                    SetTextView2(EmergencyContactNo, jsonObject1.getString("primary_contact"));

                                }

                            }
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), "Json Parsing Error", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error.Response", error.toString());
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Authorization", getString(R.string.authorisation));
                params.put("Authorization", jsonWebToken.getAuthorizationHeaderValue());
                return params;
            }
        };
        queue.add(stringRequest);


        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent5 = new Intent(MainFeed.this, UpdateChildProfile.class);
                intent5.putExtra("child_data", child);
                finish();
                startActivity(intent5);
            }
        });

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id._profile:
                        //Intent intent1 = new Intent(MainFeed.this,ProfileChild.class);
                        //startActivity(intent1);
                        break;
                    case R.id._dashboard:
                        Intent intent2 = new Intent(MainFeed.this, DashboardParent.class);
                        intent2.putExtra("child_data", child);
                        finish();
                        startActivity(intent2);
                        overridePendingTransition(0, 0);
                        break;
                    case R.id._activities:
                        Intent intent3 = new Intent(MainFeed.this, ActivitiesChild.class);
                        intent3.putExtra("child_data", child);
                        finish();
                        startActivity(intent3);
                        overridePendingTransition(0, 0);
                        break;
                    case R.id._chat:
                        Intent intent4 = new Intent(MainFeed.this, ChatParent.class);
                        intent4.putExtra("child_data", child);
                        finish();
                        startActivity(intent4);
                        overridePendingTransition(0, 0);
                        break;
                }
                return false;
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //no inspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //your code when back button pressed
        Intent intentt = new Intent(MainFeed.this, children.class);
        //intent4.putExtra("child_data", child);
        finish();
        startActivity(intentt);
        overridePendingTransition(0, 0);
    }

    public void SetTextView2(TextView textView, String string) {
        if (string != null && !string.isEmpty()) {
            // doSomething
            textView.setVisibility(View.VISIBLE);
            textView.setText(string);
        } else {
            textView.setVisibility(View.GONE);
        }

    }
}


