package co.helixtech.schoolapp.parent;

import android.text.format.DateUtils;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


//Used to convert date to time ago format
//Input Date format yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z' (Server generated Format)
public class DateConverter {

    String dateX;

    public DateConverter(String date) {
        dateX = date;
    }

    public String getDate() {
        return dateX;
    }

    public void setDate(String date) {
        dateX = date;
    }

    public String convert() {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(dateX);
            long time = date.getTime();
            if (date == null) {
                return dateX;
            } else {
                //DateFormat dateFormat = new SimpleDateFormat("EEEE\nd MMMM yyyy\nHH:mm:ss");
                //String strDate = dateFormat.format(date);
                long now = System.currentTimeMillis();

                CharSequence ago =
                        DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
                return ago.toString();

            }

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.e("error", "error" + dateX);
            return " ";
        }

    }
}
