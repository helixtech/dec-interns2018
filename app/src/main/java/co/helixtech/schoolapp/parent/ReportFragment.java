package co.helixtech.schoolapp.parent;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.helixtech.schoolapp.R;
import co.helixtech.schoolapp.utility.JsonWebToken;
import co.helixtech.schoolapp.utility.TokenSharedPreference;

//Fragment used in displaying Reports
public class ReportFragment extends Fragment {

    /*private static final String TAG = "ReportFragment";
    private Button btnTEST;*/
    private static final String TAG = "ReportChildFragment";
    ProgressBar spinner3;
    private RecyclerView mRecyclerView;
    private ReportChildAdapter ReportchildAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final View view = inflater.inflate(R.layout.report_fragment, container, false);
        spinner3 = view.findViewById(R.id.progressBarReport);
        final ArrayList<ReportParent> reports = new ArrayList<>();
        RequestQueue queue = Volley.newRequestQueue(getContext());

        TokenSharedPreference tokenSharedPreference = new TokenSharedPreference(getContext());
        final JsonWebToken jsonWebToken = tokenSharedPreference.getCurrentAccessToken();
        Log.d("token", jsonWebToken.getAuthorizationHeaderValue());

        String childID = " ";
        if (getArguments() != null) {
            childID = getArguments().getString("childID");
            Log.d("ChildIdSuccessReport", childID);
        } else {
            Log.e("error", "error in getting child id");
        }
        StringRequest stringRequest = new StringRequest(getResources().getString(R.string.url) + getResources().getString(R.string.getReports) + childID,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        //Toast.makeText(getContext(), response, Toast.LENGTH_SHORT).show();
                        String jsonStrX = response;
                        try {
                            JSONArray jsonArray = new JSONArray(jsonStrX);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                int id = jsonObject1.getInt("id");
                                String feedback = jsonObject1.getString("feedback");
                                JSONObject jsonObject = jsonObject1.getJSONObject("teacher");
                                String teacher_str = jsonObject.getString("first_name") + " " + jsonObject.getString("surname");
                                String date = jsonObject1.getString("date");
                                reports.add(new ReportParent(id, feedback, date, teacher_str, "Subject"));
                            }
                            mRecyclerView = view.findViewById(R.id.report_list);
                            LinearLayoutManager layoutManager
                                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            mRecyclerView.setLayoutManager(layoutManager);
                            mRecyclerView.setHasFixedSize(true);
                            ReportchildAdapter = new ReportChildAdapter(getActivity(), reports);
                            mRecyclerView.setAdapter(ReportchildAdapter);
                            spinner3.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("Error.Response", error.toString());
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_SHORT).show();
                spinner3.setVisibility(View.GONE);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Authorization", getString(R.string.authorisation));
                params.put("Authorization", jsonWebToken.getAuthorizationHeaderValue());
                return params;
            }
        };
        queue.add(stringRequest);

        return view;
    }
}