package co.helixtech.schoolapp.services;

import android.app.ActionBar;
import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import co.helixtech.schoolapp.backend_calls.BackendService;
import co.helixtech.schoolapp.backend_calls.DataApi;
import co.helixtech.schoolapp.backend_calls.ImageApi;
import co.helixtech.schoolapp.db.pojo.AppDatabase;
import co.helixtech.schoolapp.db.pojo.Assignment;
import co.helixtech.schoolapp.db.pojo.ClassActivity;
import co.helixtech.schoolapp.db.pojo.ReportCard;
import co.helixtech.schoolapp.utility.TokenSharedPreference;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

public class ImageSyncService extends IntentService {
    public final static String SERVICE_NAME = ImageSyncService.class.getCanonicalName();
    public final static int IMAGE_QUALITY = 75;
    private TokenSharedPreference preference = null;
    private AppDatabase appDatabase = null;
    private ImageApi imageApi = null;

    public ImageSyncService() {
        super(SERVICE_NAME);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        imageApi = new BackendService().getImageApi();
        preference = new TokenSharedPreference(this);
        appDatabase = AppDatabase.getInstance(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        int serviceCode = intent.getIntExtra(SERVICE_NAME, -1);
        switch (serviceCode) {
            case Constants.POST_ACTIVITY:
                postActivityImage(intent);
                break;
            case Constants.POST_ASSIGNMENT:
                postAssignmentImage(intent);
                break;
            case Constants.POST_NOTICE:
                //postNoticeImage(intent);
                break;
            case Constants.POST_REPORT_CARD:
                postReportCardImage(intent);
            default:
                break;
        }
    }

    private void postReportCardImage(Intent intent) {
        try {
            String fileName = intent.getStringExtra(Constants.PARAM_IMAGE_NAME);
            File file = new File(getCacheDir(), fileName);
            MultipartBody.Part body = getMultipartImageBody(file);
            int id = intent.getIntExtra(Constants.PARAM_REPORT_CARD_ID, -1);
            if (id == -1)
                return;
            Call<ReportCard> call = imageApi.postReportCardImage(
                    preference.getCurrentAccessToken().getAuthorizationHeaderValue(), id, body
            );
            Response<ReportCard> response = call.execute();
            Log.d(ImageSyncService.class.getName(), response.message());
            ReportCard reportCard = response.body();
            Log.d(ImageSyncService.class.getName(), reportCard.getId() + "");
            file.delete();

            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(Constants.BROADCAST_ACTION);
            broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE, Constants.REPORT_CARD_POSTED);
            broadcastIntent.putExtra(Constants.PARAM_REPORT_CARD_ID, reportCard.getId());
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void postNoticeImage(Intent intent) {

    }

    public void postAssignmentImage(Intent intent) {

        try {
            String fileName = intent.getStringExtra(Constants.PARAM_IMAGE_NAME);
            File file = new File(getCacheDir(), fileName);
            MultipartBody.Part body = getMultipartImageBody(file);
            int assignmentID = intent.getIntExtra(Constants.PARAM_ASSIGNMENT_ID, -1);
            if (assignmentID == -1)
                return;
            Call<Assignment> call = imageApi.postAssignmentImage(
                    preference.getCurrentAccessToken().getAuthorizationHeaderValue(), assignmentID, body);
            Response<Assignment> response = call.execute();
            Log.d(ImageSyncService.class.getName(), response.message());
            Assignment assignment = response.body();
            Log.d(ImageSyncService.class.getName(), assignment.getId() + "");
            file.delete();

            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(Constants.BROADCAST_ACTION);
            broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE, Constants.ASSIGNMENT_POSTED);
            broadcastIntent.putExtra(Constants.PARAM_ASSIGNMENT_ID, assignment.getId());
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void postActivityImage(Intent intent) {
        try {
            String fileName = intent.getStringExtra(Constants.PARAM_IMAGE_NAME);
            File file = new File(getCacheDir(), fileName);
            MultipartBody.Part body = getMultipartImageBody(file);
            int activityID = intent.getIntExtra(Constants.PARAM_ACTIVITY_ID, -1);
            if (activityID == -1)
                return;
            Call<ClassActivity> call = imageApi.postActivityImage(
                    preference.getCurrentAccessToken().getAuthorizationHeaderValue(), activityID, body);
            Response<ClassActivity> response = call.execute();
            Log.d(ImageSyncService.class.getName(), response.message());
            ClassActivity classActivity = response.body();
            Log.d(ImageSyncService.class.getName(), classActivity.getId() + "");
            file.delete();

            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(Constants.BROADCAST_ACTION);
            broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE, Constants.ACTIVITY_POSTED);
            broadcastIntent.putExtra(Constants.PARAM_ACTIVITY_ID, classActivity.getId());
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private MultipartBody.Part getMultipartImageBody(File file) {

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        return body;
    }

    public static final class Constants {
        public static final String BROADCAST_ACTION = SERVICE_NAME + ".BROADCAST_ACTION";
        public static final String BROADCAST_MESSAGE = SERVICE_NAME + ".BROADCAST_MESSAGE";
        /**********************************************************************************/
        public static final int POST_ACTIVITY = 100;
        public static final int ACTIVITY_POSTED = 101;

        public static final int POST_NOTICE = 200;
        public static final int NOTICE_POSTED = 201;

        public static final int POST_ASSIGNMENT = 300;
        public static final int ASSIGNMENT_POSTED = 301;

        public static final int POST_REPORT_CARD = 400;
        public static final int REPORT_CARD_POSTED = 401;

        /**********************************************************************************/
        public static final String PARAM_IMAGE_NAME = SERVICE_NAME + ".param_image";
        public static final String PARAM_ASSIGNMENT_ID = SERVICE_NAME + ".param_assignment_id";
        public static final String PARAM_NOTICE_ID = SERVICE_NAME + ".param_notice_id";
        public static final String PARAM_ACTIVITY_ID = SERVICE_NAME + ".param_activity_id";
        public static final String PARAM_REPORT_CARD_ID = SERVICE_NAME + ".param_report_card_id";

        public static final String ASSIGNMENT_FILE_NAME = "assignment";
        public static final String NOTICE_FILE_NAME = "notice";
        public static final String ACTIVITY_FILE_NAME = "activity";
        public static final String REPORT_CARD_FILE_NAME = "report_card";
    }
}
