package co.helixtech.schoolapp.services;

import android.app.IntentService;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import co.helixtech.schoolapp.backend_calls.BackendService;
import co.helixtech.schoolapp.backend_calls.DataApi;
import co.helixtech.schoolapp.db.pojo.AppDatabase;
import co.helixtech.schoolapp.db.pojo.Assignment;
import co.helixtech.schoolapp.db.pojo.Attendance;
import co.helixtech.schoolapp.db.pojo.ClassActivity;
import co.helixtech.schoolapp.db.pojo.Feedback;
import co.helixtech.schoolapp.db.pojo.Notice;
import co.helixtech.schoolapp.db.pojo.ParentChild;
import co.helixtech.schoolapp.db.pojo.PostActivity;
import co.helixtech.schoolapp.db.pojo.PostAssignment;
import co.helixtech.schoolapp.db.pojo.PostAttendance;
import co.helixtech.schoolapp.db.pojo.PostFeedback;
import co.helixtech.schoolapp.db.pojo.PostNotice;
import co.helixtech.schoolapp.db.pojo.PostReportCard;
import co.helixtech.schoolapp.db.pojo.ReportCard;
import co.helixtech.schoolapp.db.pojo.Student;
import co.helixtech.schoolapp.db.pojo.TeacherClass;
import co.helixtech.schoolapp.teacher.select_class_activity;
import co.helixtech.schoolapp.utility.DateConversionUtility;
import co.helixtech.schoolapp.utility.TokenSharedPreference;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class DataSyncService extends IntentService {
    public final static String SERVICE_NAME = DataSyncService.class.getCanonicalName();
    private DataApi dataApi = null;
    private TokenSharedPreference preference = null;
    private AppDatabase appDatabase = null;

    @Override
    public void onCreate() {
        super.onCreate();
        dataApi = new BackendService().getDataApi();
        preference = new TokenSharedPreference(this);
        appDatabase = AppDatabase.getInstance(this);
    }

    public DataSyncService() {
        super(SERVICE_NAME);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        switch (intent.getIntExtra(SERVICE_NAME, 0)) {
            case Constants.GET_CLASSES_OF_TEACHER:
                getClassesOfTeacher(intent);
                break;
            case Constants.GET_STUDENTS_OF_CLASS:
                getStudentsOfClass(intent);
                break;
            case Constants.GET_ASSIGNMENTS_OF_CLASS:
                getAssignmentsOfClass(intent);
                break;
            case Constants.POST_ASSIGNMENT:
                postAssignment(intent);
                break;
            case Constants.GET_NOTICES_OF_CLASS:
                getNoticesOfClass(intent);
                break;
            case Constants.GET_ACTIVITIES_OF_CLASS:
                getActivitiesOfClass(intent);
                break;
            case Constants.POST_NOTICE:
                postNotice(intent);
                break;
            case Constants.POST_ACTIVITY:
                postActivity(intent);
                break;
            case Constants.GET_PARENT_BY_STUDENT_ID:
                getParentbyStudentID(intent);
                break;
            case Constants.EDIT_ACTIVITY:
                editActivity(intent);
                break;
            case Constants.EDIT_ASSIGNMENT:
                editAssignment(intent);
                break;
            case Constants.EDIT_NOTICE:
                editNotice(intent);
                break;
            case Constants.DELETE_ACTIVITY:
                deleteActivity(intent);
                break;
            case Constants.DELETE_ASSIGNMENT:
                deleteAssignment(intent);
                break;
            case Constants.DELETE_NOTICE:
                deleteNotice(intent);
                break;

            case Constants.POST_ATTENDANCE:
                postAttendance(intent);
                break;
            case Constants.POST_FEEDBACK:
                postFeedback(intent);
                break;
            case Constants.POST_REPORT_CARD:
                postReportCard(intent);
                break;
            default:
                break;


        }
    }

    private void postReportCard(Intent intent) {
        PostReportCard postReportCard = (PostReportCard)
                intent.getSerializableExtra(Constants.PARAM_POST_REPORT_CARD);
        if(postReportCard == null)
            return;
        Call<ReportCard> call = dataApi.postReportCard(preference.getCurrentAccessToken().
                getAuthorizationHeaderValue(), postReportCard);
        try {
            Response<ReportCard> response = call.execute();
            Log.d(DataSyncService.class.getCanonicalName(), response.message());
            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:
                    ReportCard reportCard = response.body();
                    assert reportCard != null;
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE,
                            Constants.REPORT_CARD_POSTED);
                    broadcastIntent.putExtra(Constants.PARAM_REPORT_CARD_ID, reportCard.getId());
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                    break;
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void postFeedback(Intent intent) {
        PostFeedback post = (PostFeedback)
                intent.getSerializableExtra(Constants.PARAM_POST_FEEDBACK);
        if(post == null)
            return;
        Call<Feedback> call = dataApi.postFeedback(preference.getCurrentAccessToken().
                getAuthorizationHeaderValue(), post);
        try {
            Response<Feedback> response = call.execute();
            Log.d(DataSyncService.class.getCanonicalName(), response.message());
            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:
                    Feedback feedback = response.body();
                    assert feedback != null;
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE,
                            Constants.FEEDBACK_POSTED);
                    broadcastIntent.putExtra(Constants.PARAM_FEEDBACK_ID, feedback.getId());
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                    break;
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void postAttendance(Intent intent) {
        PostAttendance post = (PostAttendance)
                intent.getSerializableExtra(Constants.PARAM_POST_ATTENDANCE);
        if(post == null)
            return;
        Call<Attendance> call = dataApi.postAttendance(preference.getCurrentAccessToken().
                getAuthorizationHeaderValue(), post);
        try {
            Response<Attendance> response = call.execute();
            Log.d(DataSyncService.class.getCanonicalName(), response.message());
            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:
                    Attendance attendance = response.body();
                    assert attendance != null;
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE,
                            Constants.ATTENDANCE_POSTED);
                    broadcastIntent.putExtra(Constants.PARAM_ATTENDANCE_ID, attendance.getId());
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                    break;
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteNotice(Intent intent) {
        int id = intent.getIntExtra(Constants.PARAM_NOTICE_ID, -1);
        if (id == -1)
            return;

        Call<ResponseBody> call = dataApi.deleteNoticeByID(preference.getCurrentAccessToken().
                getAuthorizationHeaderValue(), id);
        try {
            Response<ResponseBody> response = call.execute();
            Log.d(DataSyncService.class.getCanonicalName(), response.message());
            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:
                    Notice notice = appDatabase.noticeDao().getNoticeByID(id);
                    appDatabase.noticeDao().deleteNotice(notice);
                    Log.d(DataSyncService.class.getCanonicalName(), "Notice deleted");
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE,
                            Constants.NOTICE_DELETED);
                    broadcastIntent.putExtra(Constants.PARAM_NOTICE_ID, id);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(DataSyncService.class.getCanonicalName(), "Unexpected Error", e);
        }
    }

    private void deleteAssignment(Intent intent) {
        int id = intent.getIntExtra(Constants.PARAM_ASSIGNMENT_ID, -1);
        if (id == -1)
            return;

        Call<ResponseBody> call = dataApi.deleteAssignmentByID(preference.getCurrentAccessToken().
                getAuthorizationHeaderValue(), id);
        try {
            Response<ResponseBody> response = call.execute();
            Log.d(DataSyncService.class.getCanonicalName(), response.message());
            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:
                    Assignment assignment = appDatabase.assignmentDao().getAssignmentsByIDs(id)
                            .get(0);
                    appDatabase.assignmentDao().deleteAssignment(assignment);
                    Log.d(DataSyncService.class.getCanonicalName(), "Assignment deleted");
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE,
                            Constants.ASSIGNMENT_DELETED);
                    broadcastIntent.putExtra(Constants.PARAM_ASSIGNMENT_ID, id);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(DataSyncService.class.getCanonicalName(), "Unexpected Error", e);
        }
    }

    private void deleteActivity(Intent intent) {
        int id = intent.getIntExtra(Constants.PARAM_ACTIVTIY_ID, -1);
        if (id == -1)
            return;
        Call<ResponseBody> call = dataApi.deleteActivityByID(preference.getCurrentAccessToken().
                getAuthorizationHeaderValue(), id);
        try {
            Response<ResponseBody> response = call.execute();
            Log.d(DataSyncService.class.getCanonicalName(), response.message());
            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:

                    ClassActivity activity = appDatabase.classActivityDao().getClassActivityByID(id);
                    appDatabase.classActivityDao().deleteClassActivity(activity);
                    Log.d(DataSyncService.class.getCanonicalName(), "Activity deleted");
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE,
                            Constants.ACTIVITY_DELETED);
                    broadcastIntent.putExtra(Constants.PARAM_ACTIVTIY_ID, id);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(DataSyncService.class.getCanonicalName(), "Unexpected Error", e);
        }
    }

    private void editNotice(Intent intent) {
        int id = intent.getIntExtra(Constants.PARAM_NOTICE_ID, -1);
        if (id == -1)
            return;

        PostNotice postNotice =
                (PostNotice) intent.getSerializableExtra(Constants.PARAM_POST_NOTICE);
        Call<ResponseBody> call = dataApi.editNoticeByID(preference.getCurrentAccessToken().
                getAuthorizationHeaderValue(), id, postNotice);
        try {
            Response<ResponseBody> response = call.execute();
            Log.d(DataSyncService.class.getCanonicalName(), response.message());
            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE,
                            Constants.NOTICE_EDITED);
                    broadcastIntent.putExtra(Constants.PARAM_NOTICE_ID, id);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(DataSyncService.class.getCanonicalName(), "Unexpected Error", e);
        }
    }

    private void editAssignment(Intent intent) {
        int id = intent.getIntExtra(Constants.PARAM_ASSIGNMENT_ID, -1);
        if (id == -1)
            return;

        PostAssignment postAssignment =
                (PostAssignment) intent.getSerializableExtra(Constants.PARAM_POST_ASSIGNMENT);
        Call<ResponseBody> call = dataApi.editAssignmentByID(preference.getCurrentAccessToken().
                getAuthorizationHeaderValue(), id, postAssignment);
        try {
            Response<ResponseBody> response = call.execute();
            Log.d(DataSyncService.class.getCanonicalName(), response.message());
            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE,
                            Constants.ASSIGNMENT_EDITED);
                    broadcastIntent.putExtra(Constants.PARAM_ASSIGNMENT_ID, id);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(DataSyncService.class.getCanonicalName(), "Unexpected Error", e);
        }
    }

    private void editActivity(Intent intent) {
        int id = intent.getIntExtra(Constants.PARAM_ACTIVTIY_ID, -1);
        if (id == -1)
            return;

        PostActivity postActivity =
                (PostActivity) intent.getSerializableExtra(Constants.PARAM_POST_ACTIVITY);
        Call<ResponseBody> call = dataApi.editActivityByID(preference.getCurrentAccessToken().
                getAuthorizationHeaderValue(), id, postActivity);
        try {
            Response<ResponseBody> response = call.execute();
            Log.d(DataSyncService.class.getCanonicalName(), response.message());
            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE,
                            Constants.ACTIVITY_EDITED);
                    broadcastIntent.putExtra(Constants.PARAM_ACTIVTIY_ID, id);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(DataSyncService.class.getCanonicalName(), "Unexpected Error", e);
        }
    }

    private void getParentbyStudentID(Intent intent) {

        int studentID = intent.getIntExtra(Constants.PARAM_STUDENT_ID, -1);

        if (studentID == -1)
            return;

        Call<List<ParentChild>> call = dataApi.getParentbyStudentID(preference.getCurrentAccessToken().getAuthorizationHeaderValue(), studentID);
        try {
            Response<List<ParentChild>> response = call.execute();
            Log.d("tag", response.message());

            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:
                    assert response.body() != null;
                    ArrayList<ParentChild> parentChildren = (ArrayList<ParentChild>) response.body();
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE,
                            Constants.PARENT_FETCHED);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.PARAM_PARENT_LIST, parentChildren);
                    broadcastIntent.putExtra(Constants.PARAM_PARENT_LIST, bundle);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                    break;
                case HttpURLConnection.HTTP_BAD_REQUEST:

                    break;
                default:
                    break;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void getClassesOfTeacher(Intent intent) {
        int teacherID = intent.getIntExtra(Constants.PARAM_TEACHER_ID, -1);
        if (teacherID == -1)
            return;

        Call<List<TeacherClass>> call = dataApi.getClassesOfTeacher(preference.getCurrentAccessToken()
                .getAuthorizationHeaderValue(), teacherID);
        try {
            Response<List<TeacherClass>> response = call.execute();
            Log.d(DataSyncService.class.getCanonicalName(), response.message());

            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:
                    assert response.body() != null;
                    List<TeacherClass> teacherClasses = response.body();


                    for (TeacherClass teacherClass : teacherClasses) {
                        teacherClass.setUpRedundantFields();
                        appDatabase.teacherDao().insertTeachers(teacherClass.getTeacher());
                        appDatabase.subjectDao().insertSubjects(teacherClass.getSubject());
                        appDatabase.klassDao().insertClasses(teacherClass.getClassId());
                    }
                    appDatabase.teacherClassDao().insertTeacherClasses(teacherClasses);

                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE,
                            Constants.CLASSES_OF_TEACHER_SYNCED);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                    break;
                case HttpURLConnection.HTTP_BAD_REQUEST:

                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(DataSyncService.class.getCanonicalName(), "Unexpected Error", e);
        }
    }

    private void getStudentsOfClass(Intent intent) {
        int classID = intent.getIntExtra(Constants.PARAM_CLASS_ID, -1);
        if (classID == -1)
            return;
        Call<List<Student>> call = dataApi.getStudentsOfClass(preference.getCurrentAccessToken()
                .getAuthorizationHeaderValue(), classID);
        try {
            Response<List<Student>> response = call.execute();
            Log.d(DataSyncService.class.getCanonicalName(), response.message());
            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:
                    List<Student> students = response.body();
                    appDatabase.studentDao().insertStudents(students);
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE,
                            Constants.STUDENTS_OF_CLASS_SYNCED);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                    break;
                case HttpURLConnection.HTTP_BAD_REQUEST:

                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(DataSyncService.class.getCanonicalName(), "Unexpected Error", e);
        }
    }

    private void getAssignmentsOfClass(Intent intent) {
        int classID = intent.getIntExtra(Constants.PARAM_CLASS_ID, -1);
        int teacherClassID = intent.getIntExtra(Constants.PARAM_TEACHER_CLASS_ID, -1);
        if (classID == -1 || teacherClassID == -1)
            return;
        Call<List<Assignment>> call = dataApi.getAssignmentsOfClass(new TokenSharedPreference(this).
                getCurrentAccessToken().getAuthorizationHeaderValue(), classID);
        try {
            Response<List<Assignment>> response = call.execute();
            Log.d(DataSyncService.class.getCanonicalName(), response.message());
            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:
                    List<Assignment> assignments = response.body();
                    assert assignments != null;
                    assignments = filterAssignmentsByTeacherClass(assignments, teacherClassID);
                    Assignment[] assignmentsArray = new Assignment[assignments.size()];
                    assignments.toArray(assignmentsArray);
//                    int[] ids = new int[assignmentsArray.length];
                    for (int i = 0; i < assignmentsArray.length; i++) {
                        try {
                            assignmentsArray[i].setPostDate(DateConversionUtility.
                                    stringToDate(assignmentsArray[i].getDate()));
                            assignmentsArray[i].setDeadlineDate(DateConversionUtility.
                                    stringToDate(assignmentsArray[i].getDeadline()));
//                            ids[i] = assignmentsArray[i].getId();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

                    appDatabase.assignmentDao().insertAssignments(assignmentsArray);
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE,
                            Constants.ASSIGNMENTS_OF_CLASS_FETCHED);
//                    broadcastIntent.putExtra(Constants.PARAM_ASSIGNMENT_ID,ids);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(DataSyncService.class.getCanonicalName(), "Unexpected Error", e);
        }
    }

    /*    private List<Assignment> filterByTeacherID(List<Assignment> assignments, int teacherID){
            List<Assignment> filteredList = new ArrayList<>();
            for(int i = 0 ;i < assignments.size();i++)
            {
                TeacherClass teacherClass  = appDatabase.teacherClassDao().getRecordByID(
                        assignments.get(i).getTeacherClass());
                if(teacherClass == null)
                    continue;
                if (teacherID == teacherClass.getTeacherID() ){
                    filteredList.add(assignments.get(i));
                }
            }
            return filteredList;
        }
        */
    private List<Assignment> filterAssignmentsByTeacherClass(List<Assignment> assignments, int teacherClassID) {
        List<Assignment> filteredList = new ArrayList<>();
        for (int i = 0; i < assignments.size(); i++) {
            Assignment assignment = assignments.get(i);
            if (assignment.getTeacherClass() == teacherClassID)
                filteredList.add(assignment);
        }
        return filteredList;
    }

    private void getNoticesOfClass(Intent intent) {
        int classID = intent.getIntExtra(Constants.PARAM_CLASS_ID, -1);
        int teacherClassID = intent.getIntExtra(Constants.PARAM_TEACHER_CLASS_ID, -1);
        if (classID == -1 || teacherClassID == -1)
            return;
        Call<List<Notice>> call = dataApi.getNoticesOfClass(preference.getCurrentAccessToken()
                .getAuthorizationHeaderValue(), classID);
        try {
            Response<List<Notice>> response = call.execute();
            Log.d(DataSyncService.class.getCanonicalName(), response.message());
            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:
                    List<Notice> notices = response.body();
                    notices = filterNoticesByTeachClassID(notices, teacherClassID);
                    Notice[] noticeArray = new Notice[notices.size()];
                    notices.toArray(noticeArray);
                    for (int i = 0; i < noticeArray.length; i++) {
                        noticeArray[i].setPostDate(
                                DateConversionUtility.stringToDate(noticeArray[i].getDate())
                        );
                    }

                    appDatabase.noticeDao().insertAll(noticeArray);

                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE, Constants.NOTICES_OF_CLASS_FETCHED);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                    break;
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    break;
                default:
                    break;

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private List<Notice> filterNoticesByTeachClassID(List<Notice> notices, int teacherClassID) {
        List<Notice> filteredList = new ArrayList<>();
        for (int i = 0; i < notices.size(); i++) {
            Notice notice = notices.get(i);
            if (notice.getTeacherClass() == teacherClassID)
                filteredList.add(notice);
        }
        return filteredList;
    }

    private void getActivitiesOfClass(Intent intent) {
        int classID = intent.getIntExtra(Constants.PARAM_CLASS_ID, -1);
        int teacherClassID = intent.getIntExtra(Constants.PARAM_TEACHER_CLASS_ID, -1);
        if (classID == -1 || teacherClassID == -1)
            return;
        Call<List<ClassActivity>> call = dataApi.getClassActivities(preference.getCurrentAccessToken()
                .getAuthorizationHeaderValue(), classID);
        try {
            Response<List<ClassActivity>> response = call.execute();
            Log.d(DataSyncService.class.getCanonicalName(), response.message());
            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:
                    List<ClassActivity> activities = response.body();
                    assert activities != null;
                    activities = filterActivitiesByTeachClassID(activities, teacherClassID);
                    ClassActivity[] classActivityArray = new ClassActivity[activities.size()];
                    activities.toArray(classActivityArray);
                    for (int i = 0; i < classActivityArray.length; i++) {
                        classActivityArray[i].setPostDate(
                                DateConversionUtility.stringToDate(classActivityArray[i].getDate())
                        );
                    }
                    appDatabase.classActivityDao().insertAll(classActivityArray);
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE,
                            Constants.ACTIVITIES_OF_CLASS_FETCHED);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                    break;
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    break;
                default:
                    break;

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private List<ClassActivity> filterActivitiesByTeachClassID(List<ClassActivity> activities,
                                                               int teacherClassID) {
        List<ClassActivity> filteredList = new ArrayList<>();
        for (int i = 0; i < activities.size(); i++) {
            ClassActivity activity = activities.get(i);
            if (activity.getTeacherClass() == teacherClassID)
                filteredList.add(activity);
        }
        return filteredList;
    }

    private void postAssignment(Intent intent) {
        PostAssignment postAssignment =
                (PostAssignment) intent.getSerializableExtra(Constants.PARAM_POST_ASSIGNMENT);
        Call<Assignment> call = dataApi.postAssignment(preference.getCurrentAccessToken().
                getAuthorizationHeaderValue(), postAssignment);
        try {
            Response<Assignment> response = call.execute();
            Log.d(DataSyncService.class.getCanonicalName(), response.message());
            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:
                    Assignment assignment = response.body();
                    assert assignment != null;
                    try {
                        assignment.setPostDate(DateConversionUtility.stringToDate(assignment.getDate()));
                        assignment.setDeadlineDate(DateConversionUtility.stringToDate(assignment.getDeadline()));
                        appDatabase.assignmentDao().insertAssignments(assignment);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE,
                            Constants.ASSIGNMENT_POSTED);
                    broadcastIntent.putExtra(Constants.PARAM_ASSIGNMENT_ID, assignment.getId());
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(DataSyncService.class.getCanonicalName(), "Unexpected Error", e);
        }

    }

    private void postNotice(Intent intent) {
        PostNotice postNotice = (PostNotice) intent.getSerializableExtra(Constants.PARAM_POST_NOTICE);
        Call<Notice> call = dataApi.postNotice(preference.getCurrentAccessToken().
                getAuthorizationHeaderValue(), postNotice);
        try {
            Response<Notice> response = call.execute();
            Log.d(DataSyncService.class.getName(), response.message());
            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:
                    Notice notice = response.body();
                    notice.setPostDate(DateConversionUtility.stringToDate(notice.getDate()));
                    appDatabase.noticeDao().insertAll(notice);
                    Log.d(DataSyncService.class.getName(), "Notice inserted in db");
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE, Constants.NOTICE_POSTED);
                    broadcastIntent.putExtra(Constants.PARAM_NOTICE_ID, notice.getId());
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                    break;
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void postActivity(Intent intent) {
        PostActivity postActivity = (PostActivity) intent.getSerializableExtra(Constants.PARAM_POST_ACTIVITY);
        Call<ClassActivity> call = dataApi.postActivity(preference.getCurrentAccessToken().
                getAuthorizationHeaderValue(), postActivity);
        try {
            Response<ClassActivity> response = call.execute();
            Log.d(DataSyncService.class.getName(), response.message());
            switch (response.code()) {
                case HttpURLConnection.HTTP_OK:
                    ClassActivity classActivity = response.body();
                    classActivity.setPostDate(DateConversionUtility.stringToDate(classActivity.getDate()));
                    appDatabase.classActivityDao().insertAll(classActivity);
                    Log.d(DataSyncService.class.getName(), "Activity inserted in db");
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(Constants.BROADCAST_ACTION);
                    broadcastIntent.putExtra(Constants.BROADCAST_MESSAGE, Constants.ACTIVITY_POSTED);
                    broadcastIntent.putExtra(Constants.PARAM_ACTIVTIY_ID, classActivity.getId());
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                    break;
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static final class Constants {
        public static final String BROADCAST_ACTION = SERVICE_NAME + ".BROADCAST_ACTION";
        public static final String BROADCAST_MESSAGE = SERVICE_NAME + ".BROADCAST_MESSAGE";
        /**********************************************************************************/
        public static final int GET_CLASSES_OF_TEACHER = 100;
        public static final int CLASSES_OF_TEACHER_SYNCED = 101;

        public static final int GET_STUDENTS_OF_CLASS = 110;
        public static final int STUDENTS_OF_CLASS_SYNCED = 111;

        public static final int GET_ASSIGNMENTS_OF_CLASS = 130;
        public static final int ASSIGNMENTS_OF_CLASS_FETCHED = 131;

        public static final int GET_NOTICES_OF_CLASS = 140;
        public static final int NOTICES_OF_CLASS_FETCHED = 141;

        public static final int GET_ACTIVITIES_OF_CLASS = 150;
        public static final int ACTIVITIES_OF_CLASS_FETCHED = 151;

        public static final int POST_ASSIGNMENT = 120;
        public static final int ASSIGNMENT_POSTED = 121;

        public static final int POST_NOTICE = 160;
        public static final int NOTICE_POSTED = 161;

        public static final int POST_ACTIVITY = 170;
        public static final int ACTIVITY_POSTED = 171;

        public static final int GET_PARENT_BY_STUDENT_ID = 201;
        public static final int PARENT_FETCHED = 202;

        public static final int EDIT_ASSIGNMENT = 210;
        public static final int ASSIGNMENT_EDITED = 211;

        public static final int EDIT_ACTIVITY = 220;
        public static final int ACTIVITY_EDITED = 221;

        public static final int EDIT_NOTICE = 230;
        public static final int NOTICE_EDITED = 231;

        public static final int DELETE_ASSIGNMENT = 300;
        public static final int ASSIGNMENT_DELETED = 301;

        public static final int DELETE_ACTIVITY = 310;
        public static final int ACTIVITY_DELETED = 311;

        public static final int DELETE_NOTICE = 320;
        public static final int NOTICE_DELETED = 321;

        public static final int POST_REPORT_CARD = 400;
        public static final int REPORT_CARD_POSTED = 401;

        public static final int POST_ATTENDANCE = 410;
        public static final int ATTENDANCE_POSTED = 411;

        public static final int POST_FEEDBACK = 420;
        public static final int FEEDBACK_POSTED = 421;

        /**********************************************************************************/
        public static final String PARAM_TEACHER_ID = SERVICE_NAME + ".teacher_id";
        public static final String PARAM_CLASS_ID = SERVICE_NAME + ".class_id";
        public static final String PARAM_POST_ASSIGNMENT = SERVICE_NAME + ".post_assignment";
        public static final String PARAM_ASSIGNMENT_ID = SERVICE_NAME + ".assignment_id";
        public static final String PARAM_TEACHER_CLASS_ID = SERVICE_NAME + ".teacher_class_id";
        public static final String PARAM_POST_NOTICE = SERVICE_NAME + ".post_notice";
        public static final String PARAM_POST_ACTIVITY = SERVICE_NAME + ".post_activity";
        public static final String PARAM_NOTICE_ID = SERVICE_NAME + ".notice_id";
        public static final String PARAM_ACTIVTIY_ID = SERVICE_NAME + ".activity_id";
        public static final String PARAM_STUDENT_ID = SERVICE_NAME + ".student_id";
        public static final String PARAM_PARENT_LIST = SERVICE_NAME + ".parent_list";

        public static final String PARAM_POST_REPORT_CARD = SERVICE_NAME + ".report_card";
        public static final String PARAM_POST_FEEDBACK = SERVICE_NAME + ".feedback";
        public static final String PARAM_POST_ATTENDANCE = SERVICE_NAME + ".attendance";
        public static final String PARAM_REPORT_CARD_ID = SERVICE_NAME + ".report_card_id";
        public static final String PARAM_FEEDBACK_ID = SERVICE_NAME + ".report_card_id";
        public static final String PARAM_ATTENDANCE_ID = SERVICE_NAME + ".attendance_id";
    }

}
