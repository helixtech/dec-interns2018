package co.helixtech.schoolapp.Model;

public class Chatlist {

    private int id;

    public Chatlist(int id) {
        this.id = id;
    }

    public Chatlist() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
