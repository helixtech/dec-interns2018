package co.helixtech.schoolapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import co.helixtech.schoolapp.Adapter.MessageAdapter;
import co.helixtech.schoolapp.Model.Chat;
import co.helixtech.schoolapp.Model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jaeger.library.StatusBarUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import co.helixtech.schoolapp.Adapter.MessageAdapter;
import co.helixtech.schoolapp.Model.Chat;
import co.helixtech.schoolapp.Model.User;
import co.helixtech.schoolapp.utility.TokenSharedPreference;
import de.hdodenhof.circleimageview.CircleImageView;

public class MessageActivity extends AppCompatActivity {

    CircleImageView profilepic;
    TextView username;
    ImageView backpress;

    //FirebaseUser firebaseUser;
    DatabaseReference reference;

    ImageButton btn_send;
    EditText txt_send;

    MessageAdapter md;
    List<Chat> mchat;

    RecyclerView recyclerView;

    Intent intent;

    ValueEventListener seenListener;

    int id=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        StatusBarUtil.setColor(this,getResources().getColor(R.color.colorPrimaryLight));



        /*Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MessageActivity.this,MainChatActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });*/


        recyclerView=findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);


        profilepic=findViewById(R.id.profilepic);
        username=findViewById(R.id.username);
        btn_send=findViewById(R.id.btn_send);
        txt_send=findViewById(R.id.text_send);
        backpress=findViewById(R.id.chat_back_btn);

        backpress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        intent=getIntent();
        final int userId=intent.getIntExtra("userId",-1);
        TokenSharedPreference preference = new TokenSharedPreference(this);
        id = preference.getCurrentAccessToken().getUserID();
        //firebaseUser=FirebaseAuth.getInstance().getCurrentUser();

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg=txt_send.getText().toString();
                if(!msg.equals("")){
                    sendMessage(id,userId,msg);
                }
                else {
                    Toast.makeText(MessageActivity.this,"You can't send an empty message",Toast.LENGTH_SHORT).show();
                }

                txt_send.setText("");
            }
        });


        reference=FirebaseDatabase.getInstance().getReference("Users").child(userId+"");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                User user = dataSnapshot.getValue(User.class);
                username.setText(user.getUsername());
                if(user.getImageURL().equals("default"))
                {
                    profilepic.setImageResource(R.drawable.profile_pic);
                }
                else
                {
                    Glide.with(getApplicationContext()).load(user.getImageURL()).into(profilepic);
                }

                readMessage(id,userId,user.getImageURL());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        seenMessage(userId);

    }

    private void seenMessage(final int userid){
        reference=FirebaseDatabase.getInstance().getReference("Chats");
        seenListener=reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Chat chat = snapshot.getValue(Chat.class);
                    if(chat.getReceiver()==id && chat.getSender()==userid){
                        HashMap<String,Object> hashMap=new HashMap<>();
                        hashMap.put("isseen",true);
                        snapshot.getRef().updateChildren(hashMap);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void sendMessage(final int sender, final int receiver, String message){

        DatabaseReference reference=FirebaseDatabase.getInstance().getReference();

        TokenSharedPreference preference = new TokenSharedPreference(this);
        int id = preference.getCurrentAccessToken().getUserID();

        String currentTime = "00";

        Date time = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
         currentTime = sdf.format(time);



        HashMap<String,Object> hashMap=new HashMap<>();
        hashMap.put("sender",sender);
        hashMap.put("receiver",receiver);
        hashMap.put("message",message);
        hashMap.put("isseen",false);
        hashMap.put("timestamp",currentTime);

        reference.child("Chats").push().setValue(hashMap);



        final DatabaseReference chatRef = FirebaseDatabase.getInstance().getReference("Chatlist")
                .child(id+"")
                .child(receiver+"");

        chatRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    chatRef.child("id").setValue(receiver);

                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        final DatabaseReference chatRef2 = FirebaseDatabase.getInstance().getReference("Chatlist")
                .child(receiver+"")
                .child(id+"");

        chatRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    chatRef2.child("id").setValue(sender);

                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




    }

    private void readMessage(final int myid, final int userid, final String imageurl)
    {
        mchat=new ArrayList<>();

        reference=FirebaseDatabase.getInstance().getReference("Chats");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                mchat.clear();
                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Chat chat= snapshot.getValue(Chat.class);
                    if((chat.getReceiver()==myid && chat.getSender()==userid)|| (chat.getReceiver()==userid && chat.getSender()==myid)){
                        mchat.add(chat);
                    }

                    md=new MessageAdapter(MessageActivity.this,mchat,imageurl);
                    recyclerView.setAdapter(md);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
    private void status(String status)
    {
        TokenSharedPreference preference = new TokenSharedPreference(this);
        int id = preference.getCurrentAccessToken().getUserID();
        reference=FirebaseDatabase.getInstance().getReference("Users").child(id+"");



        HashMap<String,Object> hashMap=new HashMap<>();
        hashMap.put("status",status);

        reference.updateChildren(hashMap);
    }

    @Override
    protected void onResume() {
        super.onResume();
        status("online");
    }

    @Override
    protected void onPause() {
        super.onPause();
        reference.removeEventListener(seenListener);
        status("offline");
    }
}
